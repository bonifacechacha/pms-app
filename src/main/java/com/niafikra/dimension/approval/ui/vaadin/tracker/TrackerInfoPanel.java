package com.niafikra.dimension.approval.ui.vaadin.tracker;

import com.niafikra.dimension.approval.domain.Tracker;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.TemplatePanel;
import com.vaadin.spring.annotation.SpringComponent;
import de.steinwedel.messagebox.MessageBox;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/25/17 12:02 AM
 */

@PrototypeScope
@SpringComponent
public class TrackerInfoPanel extends MVerticalLayout {

    @Inject
    private TemplatePanel templatePanel;

    @PostConstruct
    private void build() {
        setSizeFull();
        addComponentsAndExpand(templatePanel);
        templatePanel
                .getTemplateView()
                .setTemplatePath(Templates.APPROVAL_TRACKER);
    }

    public TrackerInfoPanel setTracker(Tracker tracker) {
        templatePanel
                .getTemplateView()
                .putBinding("tracker", tracker)
                .render();
        return this;
    }

    public void show(Tracker currentTracker) {
        setTracker(currentTracker);
        MessageBox messageBox = MessageBox.create()
                .withCaption(currentTracker.getType() + " by " + currentTracker.getCreator())
                .withMessage(this)
                .withCloseButton()
                .withWidth("50%")
                .withHeight("80%");

        messageBox.open();
    }
}
