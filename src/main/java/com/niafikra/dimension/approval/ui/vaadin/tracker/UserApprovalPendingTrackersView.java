package com.niafikra.dimension.approval.ui.vaadin.tracker;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;
import org.vaadin.viritin.layouts.MVerticalLayout;

import static com.niafikra.dimension.Permission.VIEW_USER_PENDING_APPROVAL_REQUESTS;
import static com.niafikra.dimension.approval.ui.vaadin.tracker.UserApprovalPendingTrackersView.VIEW_NAME;


/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/21/17 3:34 PM
 */
@Secured(VIEW_USER_PENDING_APPROVAL_REQUESTS)
@ViewComponent(value = MainDisplay.class, caption = "Pending requests")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.APPROVAL, caption = "Pending Requests")
@VaadinFontIcon(VaadinIcons.HANDS_UP)
public class UserApprovalPendingTrackersView extends MVerticalLayout implements View, TableExportable {
    public static final String VIEW_NAME = "pendingApprovals";

    private TrackersTable trackersTable;

    public UserApprovalPendingTrackersView(TrackersTable trackersTable, User currentUser) {
        this.trackersTable = trackersTable;
        trackersTable.getFilter().setApprover(currentUser);
        trackersTable.getFilter().setPendingFilter(true);
        trackersTable.getApprovalStatusFilter().setVisible(false);
        setSizeFull();
        setMargin(false);
        trackersTable.setSizeFull();
        withComponent(trackersTable).withExpandRatio(trackersTable, 1);
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(trackersTable);
    }
}
