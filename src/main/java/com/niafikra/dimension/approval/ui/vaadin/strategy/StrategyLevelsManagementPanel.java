package com.niafikra.dimension.approval.ui.vaadin.strategy;

import com.niafikra.dimension.approval.domain.Strategy;
import com.niafikra.dimension.approval.service.StrategyService;
import com.niafikra.dimension.core.ui.vaadin.user.group.GroupProvider;
import com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils;
import com.niafikra.dimension.core.ui.vaadin.util.grid.GridAddRemovePanel;
import com.niafikra.dimension.core.util.HasLogger;
import com.niafikra.dimension.group.domain.Group;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.spring.annotation.PrototypeScope;

import java.util.List;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/7/17 6:03 PM
 */
@SpringComponent
@PrototypeScope
public class StrategyLevelsManagementPanel extends VerticalLayout implements HasLogger {

    private Strategy strategy;
    private StrategyService strategyService;
    private Button saveButton = new Button("Save strategy groups", VaadinIcons.CHECK_CIRCLE);
    private GridAddRemovePanel<Group> addRemovePanel;
    private StrategyLevelsGrid groupsGrid;
    private StrategyCreateViewPanel createViewPanel;
    private GroupProvider groupProvider;

    public StrategyLevelsManagementPanel(StrategyService strategyService, GroupProvider groupProvider) {
        this.strategyService = strategyService;
        this.groupProvider = groupProvider;
        groupsGrid = new StrategyLevelsGrid();
        addRemovePanel = new GridAddRemovePanel<>(groupsGrid);

        build();
    }

    private void build() {
        setSizeFull();
        saveButton.setWidth("100%");
        saveButton.addClickListener((Button.ClickListener) event -> {
            upadateStrategyLevels();
        });

        addRemovePanel.setSizeFull();
        addRemovePanel.setMargin(false);
        addComponents(addRemovePanel, saveButton);
        setExpandRatio(addRemovePanel, 1);

        addRemovePanel.getSelector().setDataProvider(groupProvider);
        addRemovePanel.getSelector().setItemCaptionGenerator(Group::toString);
    }

    private void upadateStrategyLevels() {
        List<Group> groups = groupsGrid.getLevels();

        try {
            strategyService.updateLevels(strategy, groups);
            createViewPanel.getEntriesGrid().getDataProvider().refreshAll();

            String msg = "Saved strategy groups successful";
            getLogger().info(msg);
            NotificationUtils.showSuccess(msg);
        } catch (Exception e) {
            String msg = "Failed to save strategy groups";
            getLogger().error(msg, e);
            NotificationUtils.showError(msg);
        }
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
        groupsGrid.setStrategy(strategy);
    }

    public void setCreateViewPanel(StrategyCreateViewPanel createViewPanel) {
        this.createViewPanel = createViewPanel;
    }
}
