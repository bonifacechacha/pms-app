package com.niafikra.dimension.approval.ui.vaadin.tracker;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.approval.domain.Tracker;
import com.niafikra.dimension.approval.service.ApprovalTrackerService;
import com.niafikra.dimension.approval.ui.vaadin.tracker.issue.TrackerIssueForm;
import com.niafikra.dimension.approval.ui.vaadin.tracker.issue.TrackerIssueUtil;
import com.niafikra.dimension.approval.ui.vaadin.tracker.issue.TrackerIssuesView;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.util.SubWindow;
import com.niafikra.dimension.core.ui.vaadin.view.AbstractMainContentDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.DisplayComponent;
import com.niafikra.dimension.issue.service.IssueService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Component;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;
import de.steinwedel.messagebox.MessageBox;
import org.vaadin.viritin.MSize;
import org.vaadin.viritin.button.ConfirmButton;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Optional;

import static com.niafikra.dimension.core.security.SecurityUtils.hasAuthority;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/24/17 5:54 PM
 */
@DisplayComponent
public class ApprovalDisplay extends AbstractMainContentDisplay implements Notifier {
    private MButton approveButton, declineButton,
            infoButton, issuesButton,
            overrideApproveButton,
            overrideDeclineButton,
            previousButton, nextButton;
    private ConfirmButton cancelButton;
    private VerticalLayout viewComponentHolder;

    @Inject
    private ApprovalTrackerService trackerService;
    @Inject
    private IssueService issueService;
    private Tracker currentTracker;
    @Inject
    private User currentUser;

    private TextArea notesArea = new TextArea("Notes");

    @Inject
    private TrackerInfoPanel trackerInfoPanel;
    @Inject
    private TrackerIssuesView trackerIssuesView;
    @Inject
    private TrackerIssueForm trackerIssueForm;
    @Inject
    private OverrideForm overrideForm;

    private boolean doPrevious() {
        Optional<Tracker> tracker = trackerService.findPendingEarlyTracker(currentTracker, currentUser);
        if (tracker.isPresent()) {
            navigator.navigateTo(Tracker.createNavigationPath(tracker.get()));
            return true;
        } else {
            showWarning("There is no any previous pending approval request");
            return false;
        }
    }

    private boolean doNext() {
        Optional<Tracker> tracker = trackerService.findPendingLateTracker(currentTracker, currentUser);
        if (tracker.isPresent()) {
            navigator.navigateTo(Tracker.createNavigationPath(tracker.get()));
            return true;
        } else {
            showWarning("There is no any next pending approval request");
            return false;
        }
    }

    private void doOverrideApprove() {
        getCurrentView().onOverrideApprove();

        overrideForm.init(currentTracker);
        overrideForm.openInModalPopup("Are you sure you want to override and approve this request?");
        overrideForm.setSavedHandler(override -> {
            try {
                trackerService.overrideApproval(currentTracker, currentUser, override.getUser(), true, override.getComment());
                navigator.navigateTo(Tracker.createNavigationPath(currentTracker));
                showSuccess("Successfully overrode approval of " + currentTracker.getType(), currentTracker.toString());
                overrideForm.closePopup();
            } catch (Exception e) {
                showError("Failed to overrode approval of " + currentTracker.getType(), e);
            }
        });
    }


    private void doOverrideDecline() {
        getCurrentView().onOverrideDecline();

        overrideForm.init(currentTracker);
        overrideForm.openInModalPopup("Are you sure you want to override and decline this request?");
        overrideForm.setSavedHandler(override -> {
            try {
                trackerService.overrideApproval(currentTracker, currentUser, override.getUser(), false, override.getComment());
                navigator.navigateTo(Tracker.createNavigationPath(currentTracker));
                showSuccess("Successfully overrode decline approval of " + currentTracker.getType(), currentTracker.toString());
            } catch (Exception e) {
                showError("Failed to overrode decline approval for " + currentTracker.getType(), e);
            }
        });
    }

    private void showIssues() {
        //if there is no issues in the allocation then just show the create form
        if (issueService.countIssues(TrackerIssueUtil.getReference(currentTracker)) == 0) {
            createIssue();
        } else {
            trackerIssuesView.setTracker(currentTracker);
            new SubWindow(trackerIssuesView)
                    .show()
                    .withCaption(currentTracker + " issues")
                    .withCloseListener(e -> {
                        navigator.navigateTo(Tracker.createNavigationPath(currentTracker));
                    })
                    .withModal(true)
                    .withCenter()
                    .withSize(MSize.size("80%", "90%"));
        }
    }

    private void createIssue() {
        trackerIssueForm.setTracker(currentTracker);
        trackerIssueForm.prepareCreateIssue();
        trackerIssueForm
                .openInModalPopup()
                .addCloseListener(e -> {
                    //if the issue was successful created then probably there will
                    if (!issueService.isResolved(TrackerIssueUtil.getReference(currentTracker))) {
                        showIssues();
                    }
                });
    }

    private void doCancel() {
        getCurrentView().onCancel();

        try {
            currentTracker = trackerService.cancelApproval(currentTracker);
            navigator.navigateBack();
            showSuccess(String.format("Successfully cancelled approval for %s", currentTracker.getType()), currentTracker.toString());
        } catch (Exception e) {
            showError(String.format("Failed to cancel approval for %s", currentTracker.getType()), e);
        }
    }

    private void showInfo() {
        trackerInfoPanel.show(currentTracker);
    }

    @PostConstruct
    private void build() {
        approveButton = new MButton(
                FontAwesome.CHECK_CIRCLE_O,
                "Approve",
                (event) -> doApprove()
        );

        declineButton = new MButton(
                FontAwesome.BAN,
                "Decline",
                (event) -> doDecline()
        );

        overrideApproveButton = new MButton(
                FontAwesome.CHECK_CIRCLE_O,
                "Override Approve",
                (event) -> doOverrideApprove()
        );

        overrideDeclineButton = new MButton(
                FontAwesome.BAN,
                "Override Decline",
                (event) -> doOverrideDecline()
        );

        previousButton = new MButton(
                VaadinIcons.BACKWARDS,
                "Previous",
                (event) -> doPrevious()
        );

        nextButton = new MButton(
                VaadinIcons.FORWARD,
                "Next",
                (event) -> doNext()
        );

        infoButton = new MButton(
                FontAwesome.INFO,
                "Info",
                (event) -> showInfo()
        ).withVisible(hasAuthority(Permission.VIEW_APPROVAL_REQUEST_INFO));

        issuesButton = new MButton(
                VaadinIcons.COMMENT_ELLIPSIS,
                "Issues",
                (event) -> showIssues()
        ).withVisible(hasAuthority(Permission.VIEW_APPROVAL_REQUEST_ISSUES));

        cancelButton = new ConfirmButton(
                FontAwesome.CLOSE,
                "Cancel",
                "Are you sure you want to cancel the approval?",
                () -> doCancel()
        ).withVisible(hasAuthority(Permission.CANCEL_APPROVAL_REQUEST));

        viewComponentHolder = new MVerticalLayout().withFullSize().withMargin(false);
        overrideForm.getSaveButton().setCaption("Override");
        overrideForm.setResetHandler(entity -> overrideForm.closePopup());
        overrideForm.getResetButton().setCaption("Cancel");

        setSizeFull();
        setMargin(false);
        approveButton.withWidth("100px").withStyleName(Theme.BUTTON_PRIMARY, Theme.BUTTON_SMALL).withVisible(false);
        overrideApproveButton.withWidth("150px").withStyleName(Theme.BUTTON_PRIMARY, Theme.BUTTON_SMALL).withVisible(false);
        declineButton.withWidth("100px").withStyleName(Theme.BUTTON_DANGER, Theme.BUTTON_SMALL).withVisible(false);
        overrideDeclineButton.withWidth("150px").withStyleName(Theme.BUTTON_DANGER, Theme.BUTTON_SMALL).withVisible(false);
        previousButton.withStyleName(Theme.BUTTON_SMALL).setWidth("100px");
        nextButton.withStyleName(Theme.BUTTON_SMALL).setWidth("100px");
        cancelButton.withStyleName(Theme.BUTTON_SMALL).setWidth("100px");
        infoButton.withStyleName(Theme.BUTTON_SMALL).withWidth("100px");
        issuesButton.withStyleName(Theme.BUTTON_SMALL).withWidth("100px");
        addComponent(
                new MHorizontalLayout().withComponents(
                        approveButton,
                        overrideApproveButton,
                        declineButton,
                        overrideDeclineButton,
                        previousButton,
                        nextButton,
                        cancelButton,
                        infoButton,
                        issuesButton)
        );
        addComponent(viewComponentHolder);
        setExpandRatio(viewComponentHolder, 1);

        notesArea.setWidth("300px");
        notesArea.setRows(4);
    }

    private void doDecline() {
        getCurrentView().onDecline();

        notesArea.clear();
        MessageBox.createQuestion()
                .withCaption("Are you sure that you decline this?")
                .withMessage(notesArea)
                .withOkButton(() -> {
                    try {
                        trackerService.processApproval(currentTracker, currentUser, false, notesArea.getValue());
                        navigator.navigateTo(Tracker.createNavigationPath(currentTracker));
                        showSuccess("Successfully declined approval of " + currentTracker.getType(), currentTracker.toString());
                        if (!doPrevious()) doNext();
                    } catch (Exception e) {
                        showError("Failed to decline approval for " + currentTracker.getType(), e);
                    }
                })
                .withCancelButton()
                .open();
    }

    private void doApprove() {
        getCurrentView().onApprove();

        notesArea.clear();
        MessageBox.createQuestion()
                .withCaption("Are you sure that you approve this?")
                .withMessage(notesArea)
                .withOkButton(() -> {
                    try {
                        trackerService.processApproval(currentTracker, currentUser, true, notesArea.getValue());
                        navigator.navigateTo(Tracker.createNavigationPath(currentTracker));
                        showSuccess("Successfully approved " + currentTracker.getType(), currentTracker.toString());
                        if (!doPrevious()) doNext();
                    } catch (Exception e) {
                        showError("Failed to approved " + currentTracker.getType(), e);
                    }
                })
                .withCancelButton()
                .open();
    }

    @Override
    public void show(View view) {
        viewComponentHolder.removeAllComponents();
        Component viewComponent = view.getViewComponent();
        viewComponentHolder.addComponent(viewComponent);
        viewComponentHolder.setExpandRatio(viewComponent, 1);
    }

    @Override
    public void afterViewChange(ViewChangeEvent event) {
        if (isAttached()) {
            Long trackerId = Long.parseLong(event.getParameters());
            currentTracker = trackerService.getTracker(trackerId);

            approveButton.setVisible(trackerService.hasApprovalAuthority(currentTracker, currentUser));
            overrideApproveButton.setVisible(trackerService.hasApprovalOverrideAuthority(currentTracker, currentUser));
            declineButton.setVisible(trackerService.hasDeclineAuthority(currentTracker, currentUser));
            overrideDeclineButton.setVisible(trackerService.hasDeclineOverrideAuthority(currentTracker, currentUser));

            if (issueService.isResolved(TrackerIssueUtil.getReference(currentTracker)))
                issuesButton.removeStyleName(Theme.BUTTON_DANGER);
            else issuesButton.addStyleName(Theme.BUTTON_DANGER);
        }
    }

    public ApprovalView getCurrentView() {
        if (isAttached()) return (ApprovalView) navigator.getRecentViewChangeEvent().getNewView();
        else throw new IllegalStateException("The approval display is not active at the moment");
    }
}
