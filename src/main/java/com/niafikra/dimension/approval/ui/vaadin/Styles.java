package com.niafikra.dimension.approval.ui.vaadin;

import com.niafikra.dimension.approval.domain.Event;

public class Styles {

    public static final String determineApprovalActionColor(Event event) {
        if (event.isOverrode()) return "blue";
        if (event.isApproved()) return "green";
        if (event.isDeclined()) return "red";
        return "";
    }
}
