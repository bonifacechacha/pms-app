package com.niafikra.dimension.approval.ui.vaadin.strategy;

import com.niafikra.dimension.approval.domain.Strategy;
import com.niafikra.dimension.approval.service.StrategyService;
import com.niafikra.dimension.core.ui.vaadin.util.provider.AbstractNameFilterablePageableDataProvider;
import com.vaadin.data.provider.Query;
import com.vaadin.spring.annotation.SpringComponent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/29/17 8:40 PM
 */
@SpringComponent
@PrototypeScope
public class StrategyProvider extends AbstractNameFilterablePageableDataProvider<Strategy> {

    private StrategyService strategyService;

    public StrategyProvider(StrategyService strategyService) {
        this.strategyService = strategyService;
    }

    @Override
    protected Page<Strategy> fetchFromBackEnd(Query<Strategy, String> query, Pageable pageable) {
        return strategyService.findStrategies(getFilter(query), pageable);
    }


    @Override
    protected int sizeInBackEnd(Query<Strategy, String> query) {
        return strategyService.countStrategies(getFilter(query)).intValue();
    }
}
