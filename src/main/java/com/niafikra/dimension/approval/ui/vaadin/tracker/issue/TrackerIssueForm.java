package com.niafikra.dimension.approval.ui.vaadin.tracker.issue;

import com.niafikra.dimension.approval.domain.Tracker;
import com.niafikra.dimension.approval.service.ApprovalTrackerService;
import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.attachment.ui.vaadin.util.AttachmentsButton;
import com.niafikra.dimension.issue.domain.Issue;
import com.niafikra.dimension.issue.service.IssueService;
import com.niafikra.dimension.issue.ui.vaadin.IssueForm;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

import java.util.Set;

@PrototypeScope
@SpringComponent
public class TrackerIssueForm extends IssueForm {


    private Tracker tracker;
    private ApprovalTrackerService trackerService;
    private IssueService issueService;

    public TrackerIssueForm(ApprovalTrackerService trackerService,
                            IssueService issueService,
                            AttachmentsButton<Set<Attachment>> attachmentsButton) {
        super(attachmentsButton);
        this.trackerService = trackerService;
        this.issueService = issueService;

        addSavedHandler(issue -> doSave(issue));
    }

    public Tracker getTracker() {
        return tracker;
    }

    public void setTracker(Tracker tracker) {
        this.tracker = tracker;
    }

    private void doSave(Issue issue) {
        issue.setReference(TrackerIssueUtil.getReference(tracker));
        try {
            issueService.createIssue(
                    issue
            );
            closePopup();
            showSuccess("Created new issue successful");
        } catch (Exception e) {
            showError("Failed to create issue", e);
        }
    }

}
