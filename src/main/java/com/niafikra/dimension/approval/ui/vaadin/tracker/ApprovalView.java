package com.niafikra.dimension.approval.ui.vaadin.tracker;

import com.vaadin.navigator.View;

public interface ApprovalView extends View {
    default void onApprove() {
    }

    default void onDecline() {
    }

    default void onOverrideDecline() {
    }

    default void onOverrideApprove() {
    }

    default void onCancel() {
    }
}
