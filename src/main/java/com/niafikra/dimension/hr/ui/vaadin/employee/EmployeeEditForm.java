package com.niafikra.dimension.hr.ui.vaadin.employee;

import com.niafikra.dimension.attachment.ui.vaadin.util.AttachmentPictureField;
import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.user.UserComboBox;
import com.niafikra.dimension.core.ui.vaadin.util.AbstractForm;
import com.niafikra.dimension.hr.domain.Contact;
import com.niafikra.dimension.hr.domain.Employee;
import com.vaadin.annotations.PropertyId;
import com.vaadin.data.validator.BeanValidator;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import org.vaadin.viritin.fields.EmailField;
import org.vaadin.viritin.fields.EnumSelect;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.layouts.MFormLayout;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MPanel;

import javax.inject.Inject;

public class EmployeeEditForm extends AbstractForm<Employee> {

    public static final String FIELD_WIDTH = "200px";

    private MTextField code = new MTextField("Code").withWidth(FIELD_WIDTH);
    private MTextField firstName = new MTextField().withWidth(FIELD_WIDTH).withPlaceholder("First Name");
    private MTextField middleName = new MTextField().withWidth(FIELD_WIDTH).withPlaceholder("Middle Name");
    private MTextField lastName = new MTextField().withWidth(FIELD_WIDTH).withPlaceholder("Last Name");
    private DateField birthDate = new DateField("Date of birth");
    private EnumSelect<Employee.Gender> gender = new EnumSelect<>(Employee.Gender.class).withWidth(FIELD_WIDTH).withCaption("Gender");
    private EnumSelect<Employee.MaritalStatus> maritalStatus = new EnumSelect<>(Employee.MaritalStatus.class)
            .withWidth(FIELD_WIDTH)
            .withCaption("Marital Status");

    @PropertyId("contact.email")
    private EmailField contactEmail = new EmailField().withWidth(FIELD_WIDTH).withPlaceholder("Email");

    @PropertyId("contact.phone")
    private TextField contactPhone = new MTextField().withWidth(FIELD_WIDTH).withPlaceholder("Phone");

    @PropertyId("contact.address")
    private TextArea contactAddress = new TextArea("Address");

    @Inject
    private AttachmentPictureField picture;

    @Inject
    private UserComboBox associatedUser;

    public EmployeeEditForm() {
        super(Employee.class);
    }

    @Override
    protected void bind() {
        super.bind();

        getBinder().forField(contactEmail)
                .asRequired()
                .withValidator(new BeanValidator(Contact.class, "email"))
                .bind(Employee::getEmail, (employee, email) -> employee.getContact().setEmail(email));

        getBinder().forField(contactPhone)
                .asRequired()
                .withValidator(new BeanValidator(Contact.class, "phone"))
                .bind(Employee::getPhone, (employee, phone) -> employee.getContact().setPhone(phone));

        getBinder().forField(contactAddress)
                .withValidator(new BeanValidator(Contact.class, "address"))
                .bind(Employee::getAddress, (employee, address) -> employee.getContact().setAddress(address));
    }

    @Override
    protected Component createContent() {

        picture.setWidth("160px");
        birthDate.setWidth(FIELD_WIDTH);
        contactAddress.setWidth("400px");
        associatedUser.setWidth(FIELD_WIDTH);
        associatedUser.setCaption("Authentication");
        associatedUser.setItemCaptionGenerator(item -> item.getName());
        getSaveButton().setWidth(FIELD_WIDTH);

        return new MPanel(
                new MFormLayout(
                        picture,
                        code,
                        new MHorizontalLayout(firstName, middleName, lastName).withCaption("Name"),
                        birthDate,
                        gender,
                        maritalStatus,
                        new MHorizontalLayout(contactEmail, contactPhone).withCaption("Contact"),
                        associatedUser,
                        contactAddress,
                        getToolbar()
                )
        ).withStyleName(Theme.PANEL_BORDERLESS).withFullSize();
    }
}
