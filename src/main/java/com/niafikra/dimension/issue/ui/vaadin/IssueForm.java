package com.niafikra.dimension.issue.ui.vaadin;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.attachment.ui.vaadin.util.AttachmentsButton;
import com.niafikra.dimension.issue.domain.Issue;
import com.vaadin.ui.Window;
import org.vaadin.viritin.layouts.MHorizontalLayout;

import java.util.Set;

public class IssueForm extends NoteForm<Issue> {

    public IssueForm(AttachmentsButton<Set<Attachment>> attachmentsButton) {
        super(Issue.class, attachmentsButton);
    }

    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setCaption("Create issue");
        return window;
    }

    @Override
    protected MHorizontalLayout createContent() {
        return super.createContent().withMargin(true);
    }

    public void prepareCreateIssue() {
        setEntity(new Issue());
    }
}
