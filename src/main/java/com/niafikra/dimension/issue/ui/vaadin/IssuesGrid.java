package com.niafikra.dimension.issue.ui.vaadin;

import com.google.common.collect.ImmutableMap;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.issue.domain.Issue;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.HtmlRenderer;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.vaadin.spring.annotation.PrototypeScope;

import java.util.Locale;

@PrototypeScope
@SpringComponent
public class IssuesGrid extends Grid<Issue> {

    public IssuesGrid(SpringTemplateEngine templateEngine) {

        addColumn(issue ->
                        templateEngine.process(
                                Templates.ISSUE_ROW,
                                new Context(Locale.getDefault(), ImmutableMap.of("issue", issue))),
                new HtmlRenderer()
        );

        setFooterVisible(false);
        setHeaderVisible(false);
        setBodyRowHeight(90);
    }

}
