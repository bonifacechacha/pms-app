package com.niafikra.dimension.plan.ui.vaadin.budget.execution.retirement;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.security.SecurityUtils;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.plan.domain.Expense;
import com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense.ExpensesGrid;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.Permission.VIEW_USER_PENDING_RETIREMENT_EXPENSES;
import static com.niafikra.dimension.plan.ui.vaadin.budget.execution.retirement.PendingReconciliationExpensesView.VIEW_NAME;


@Secured(VIEW_USER_PENDING_RETIREMENT_EXPENSES)
@ViewComponent(value = MainDisplay.class, caption = "Pending reconciliation")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.FINANCE, caption = "Pending Reconciliation")
@VaadinFontIcon(VaadinIcons.MONEY)
public class PendingReconciliationExpensesView extends VerticalLayout implements View, TableExportable {
    public static final String VIEW_NAME = "pending-reconciliation-expenses";

    @Inject
    private ExpensesGrid expensesGrid;
    @Inject
    private Navigator navigator;
    @Inject
    private User currentUser;

    @PostConstruct
    private void build() {

        setSizeFull();
        setMargin(false);
        expensesGrid.setSizeFull();
        expensesGrid.getExpenseFilter().setAssociatedUser(currentUser);
        expensesGrid.getExpenseFilter().setReconciled(false);
        expensesGrid.getExpenseFilter().setSettled(false);
        expensesGrid.getDataProvider().refreshAll();

        addComponentsAndExpand(expensesGrid);
        setExpandRatio(expensesGrid, 1);

        if (SecurityUtils.hasAuthority(Permission.RETIRE_EXPENSE)) {
            expensesGrid.addContextClickListener(event -> {
                Grid.GridContextClickEvent<Expense> contextEvent = (Grid.GridContextClickEvent<Expense>) event;
                showRetirementWindow(contextEvent.getItem());
            });
        }
    }

    private void showRetirementWindow(Expense expense) {
        navigator.navigateTo(RetirementCreateView.class, expense.getId());
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(expensesGrid);
    }
}