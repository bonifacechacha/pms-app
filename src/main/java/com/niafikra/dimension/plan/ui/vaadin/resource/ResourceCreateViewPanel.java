package com.niafikra.dimension.plan.ui.vaadin.resource;

import com.niafikra.dimension.category.domain.Category;
import com.niafikra.dimension.category.ui.vaadin.HasCategoryFormPopupCreatePanel;
import com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils;
import com.niafikra.dimension.plan.domain.Resource;
import com.niafikra.dimension.plan.service.ResourceService;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/29/17 11:40 PM
 */
@SpringComponent
@PrototypeScope
public class ResourceCreateViewPanel extends HasCategoryFormPopupCreatePanel<Resource> {

    private ResourceService resourceService;
    private ResourcesBackendGrid resourcesGrid;

    public ResourceCreateViewPanel(ResourcesBackendGrid resourcesGrid,
                                   ResourceForm resourceForm,
                                   ResourceService resourceService) {
        super(resourcesGrid, resourceForm);
        this.resourcesGrid = resourcesGrid;
        this.resourceService = resourceService;
    }

    @PostConstruct
    protected void build() {
        setMargin(new MarginInfo(false, true, false, true));
    }

    @Override
    protected Resource createCategorisedEntity(Category category) {
        return new Resource(category);
    }

    @Override
    protected void onDelete(Resource resource) {
        try {
            resourceService.delete(resource);

            String msg = "Deleted resource successful";
            getLogger().info(msg);
            NotificationUtils.showSuccess(msg);
        } catch (Exception e) {
            String msg = "Failed to delete resource";
            getLogger().error(msg, e);
            NotificationUtils.showError(msg);
        }
    }

    @Override
    public void filterCategory(Category category) {
        resourcesGrid.getFilter().setCategory(category);
        resourcesGrid.getDataProvider().refreshAll();
    }
}
//    public static final String VIEW_NAME = "resources";
//
//    private ResourceService resourceService;
//
//    public ResourceCreateViewPanel(ResourcesBackendGrid resourceGrid,
//                                   ResourceForm resourceForm,
//                                   ResourceService resourceService) {
//        super(resourceGrid,resourceForm);
//        this.resourceService = resourceService;
//    }
//
//    @PostConstruct
//    @Override
//    protected void build() {
//        super.build();
//    }
//
//
//    @Override
//    protected Resource createNewEntity() {
//        return new Resource();
//    }
//
//    @Override
//    protected void onDelete(Resource resource) {
//        resourceService.delete(resource);
//    }
//}
