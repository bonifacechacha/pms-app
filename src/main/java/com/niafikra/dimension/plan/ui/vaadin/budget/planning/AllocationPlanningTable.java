package com.niafikra.dimension.plan.ui.vaadin.budget.planning;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.MoneyField;
import com.niafikra.dimension.core.ui.vaadin.util.MoneyRangeValidator;
import com.niafikra.dimension.money.Money;
import com.niafikra.dimension.plan.domain.AllocationProposal;
import com.niafikra.dimension.plan.domain.Budget;
import com.niafikra.dimension.plan.service.AllocationService;
import com.vaadin.data.Binder;
import com.vaadin.data.ValueProvider;
import com.vaadin.server.Setter;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.components.grid.FooterRow;
import org.vaadin.viritin.label.MLabel;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/19/17 4:49 PM
 */
@SpringComponent
@ViewScope
public class AllocationPlanningTable extends Grid<AllocationProposal> {

    private AllocationService allocationService;
    private User currentUser;
    private List<AllocationProposal> allocations;

    private FooterRow footerRow;

    private Budget budget;

    public AllocationPlanningTable(AllocationService allocationService, User currentUser) {
        this.allocationService = allocationService;
        this.currentUser = currentUser;

        addComponentColumn(proposal -> createResourceInfoView(proposal))
                .setId("resource")
                .setCaption("Resource");

        addComponentColumn(allocation -> createNumberField(allocation))
                .setId("amount")
                .setCaption("Propose amount");

        addComponentColumn(allocation -> createDescriptionField(allocation))
                .setId("description")
                .setCaption("Description");

        addComponentColumn(allocation -> createReasonField(allocation))
                .setId("reason")
                .setCaption("Reason");

        setBodyRowHeight(70);
        setSelectionMode(SelectionMode.NONE);
        setFooterVisible(true);
        footerRow = appendFooterRow();
    }

    private Component createResourceInfoView(AllocationProposal proposal) {
        return new MVerticalLayout(
                new MLabel(proposal.getRole().toString()).withStyleName(Theme.LABEL_LIGHT, Theme.BUTTON_QUIET),
                new MLabel(proposal.getResource().toString()).withStyleName(Theme.LABEL_BOLD, Theme.LABEL_LARGE)
        ).withMargin(false).withSpacing(false);
    }

    private TextArea createReasonField(AllocationProposal allocation) {
        TextArea reasonField = bindTextAreaField(
                allocation,
                alloc -> alloc.getReason(),
                (alloc, value) -> {
                    alloc.setReason(value);
                    //after changing the allocation recalculate summary
                    calculateSummary();
                }
        );
        return reasonField;
    }


    private TextArea createDescriptionField(AllocationProposal allocation) {
        TextArea descriptionField = bindTextAreaField(
                allocation,
                alloc -> alloc.getDescription(),
                (alloc, value) -> {
                    alloc.setDescription(value);
                    //after changing the allocation recalculate summary
                    calculateSummary();
                }
        );
        return descriptionField;
    }


    private TextArea bindTextAreaField(AllocationProposal allocation,
                                       ValueProvider<AllocationProposal, String> getter,
                                       Setter<AllocationProposal, String> setter) {
        TextArea reasonField = new TextArea();
        reasonField.setWidth("100%");
        reasonField.setValue(getter.apply(allocation));

        Binder<AllocationProposal> binder = new Binder<>();
        binder.forField(reasonField)
                .bind(
                        alloc -> {
                            return getter.apply(allocation);
                        },
                        (alloc, value) -> {
                            setter.accept(alloc, value);
                            //after changing the allocation recalculate summary
                            calculateSummary();
                        }
                );
        binder.setBean(allocation);
        return reasonField;
    }

    private Component createNumberField(AllocationProposal allocation) {

        MoneyField amountField = new MoneyField();
        amountField.setWidth("100%");
        amountField.setValue(allocation.getProposedAmount());

        Binder<AllocationProposal> binder = new Binder<>();
        binder.forField(amountField)
                .withValidator(new MoneyRangeValidator("Value must be greater than zero", Money.getZERO(), null))
                .bind(
                        alloc -> {
                            return alloc.getProposedAmount();
                        },
                        (alloc, value) -> {
                            alloc.setProposedAmount(value);
                            //after changing the allocation recalculate summary
                            calculateSummary();
                        }
                );


        binder.setBean(allocation);
        return amountField;
    }

    private void calculateSummary() {
        footerRow.getCell("amount").setText(
                getTotalAllocations().toString()
        );
    }

    public void load() {
        allocations = allocationService.prepareAllocationProposals(budget, currentUser);

        setItems(allocations);
        calculateSummary();
    }

    public Money getTotalAllocations() {
        return allocations.stream()
                .map(alloc -> alloc.getProposedAmount())
                .reduce(Money.getZERO(), (total, amt) -> total.plus(amt));
    }

    public List<AllocationProposal> getAllocations() {
        return allocations;
    }

    public Budget getBudget() {
        return budget;
    }

    public void setBudget(@NotNull Budget budget) {
        Objects.requireNonNull(budget, "Budget must not be null");
        this.budget = budget;
        load();
    }
}
