package com.niafikra.dimension.plan.ui.vaadin.budget.execution.periodAdjustment;

import com.niafikra.dimension.plan.domain.Budget;
import com.niafikra.dimension.plan.domain.PeriodAdjustment;
import com.niafikra.dimension.plan.service.PeriodAdjustmentService;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.spring.annotation.SpringComponent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;
import org.vaadin.spring.annotation.PrototypeScope;

import java.util.ArrayList;
import java.util.List;

@PrototypeScope
@SpringComponent
public class PeriodAdjustmentProvider extends PageableDataProvider<PeriodAdjustment, String> {

    private PeriodAdjustmentService adjustmentService;

    private Budget budget;

    public PeriodAdjustmentProvider(PeriodAdjustmentService adjustmentService) {
        this.adjustmentService = adjustmentService;
    }

    @Override
    protected Page<PeriodAdjustment> fetchFromBackEnd(Query<PeriodAdjustment, String> query, Pageable pageable) {
        return adjustmentService.findAll(budget, pageable);
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        List<QuerySortOrder> sortOrders = new ArrayList<>();
        sortOrders.add(new QuerySortOrder("timeCreated", SortDirection.DESCENDING));
        return sortOrders;
    }

    @Override
    protected int sizeInBackEnd(Query<PeriodAdjustment, String> query) {
        return adjustmentService.countAll(budget).intValue();
    }

    public void filterBudget(Budget budget) {
        this.budget = budget;
        refreshAll();
    }
}
