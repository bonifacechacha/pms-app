package com.niafikra.dimension.plan.ui.vaadin.budget.execution.allocationAdjustment;

import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.TemplatePanel;
import com.niafikra.dimension.plan.domain.AllocationAdjustment;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;

/**
 * @Author Juma mketto
 * @Date 1/2/19.
 */
@PrototypeScope
@SpringComponent
public class AllocationAdjustmentPanel extends VerticalLayout {
    private TemplatePanel templatePanel;

    public AllocationAdjustmentPanel(TemplatePanel templatePanel) {
        this.templatePanel = templatePanel;
        this.templatePanel.getTemplateView().setTemplatePath(Templates.ALLOCATION_ADJUSTMENT);
    }
    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(true);
        addComponent(templatePanel);
        addComponent(new Label(""));
    }

    public void setAllocationAdjustment(AllocationAdjustment adjustment){
        templatePanel.getTemplateView().putBinding("allocationAdjustment", adjustment);
        templatePanel.getTemplateView().render();
    }
}
