package com.niafikra.dimension.plan.ui.vaadin.consumer;

import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.plan.domain.Consumer;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

@PrototypeScope
@SpringComponent
public class ConsumerComboBox extends MComboBox<Consumer> {
    public ConsumerComboBox(ConsumerProvider provider) {
        setDataProvider(provider);
        setCaption("Consumer");
    }
}
