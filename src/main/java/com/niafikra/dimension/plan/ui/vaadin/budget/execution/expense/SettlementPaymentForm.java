package com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense;

import com.niafikra.dimension.core.ui.vaadin.util.PrintWindow;
import com.niafikra.dimension.plan.domain.Expense;
import com.niafikra.dimension.plan.domain.Payment;
import com.niafikra.dimension.plan.service.ExpenseService;
import com.niafikra.dimension.plan.ui.vaadin.budget.execution.PaymentForm;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class SettlementPaymentForm extends PaymentForm {
    private Expense expense;

    @Inject
    private ExpenseService expenseService;

    @PostConstruct
    private void build() {
        addSavedHandler(payment -> pay(payment));
    }

    private void pay(Payment payment) {
        try {
            payment = expenseService.settle(expense, payment);
            PrintWindow.create(PaymentPrintable.create(payment), "Do you want to print " + (payment.isPaid() ? "payment" : "receipt") + " voucher?");
            closePopup();
            showSuccess("Successfully settled expense retirements with " + payment.getAmount(),
                    "Amount pending is: " + expense.getPendingSettlement());
        } catch (Exception e) {
            showError("Failed to settle expense retirements", e);
        }
    }

    public void setExpense(Expense expense) {
        this.expense = expense;

        Payment payment = new Payment(
                expense.getAssociatedUser().toString(),
                expense.getPendingSettlement(),
                expense.requiresPayment());
        setEntity(payment);
    }
}
