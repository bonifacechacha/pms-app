package com.niafikra.dimension.plan.ui.vaadin.budget.execution.retirement;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.plan.domain.*;
import com.niafikra.dimension.plan.service.RetirementService;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.spring.annotation.SpringComponent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.List;

@PrototypeScope
@SpringComponent
public class RetirementsProvider extends PageableDataProvider<Retirement, Void> {
    @Inject
    private RetirementService retirementService;

    private LocalDateTime startDate, endDate;
    private CostCenter costCenter;
    private Budget budget;
    private Role role;
    private Resource resource;
    private User creator;
    private Consumer consumer;
    private String description;


    @Override
    protected Page<Retirement> fetchFromBackEnd(Query<Retirement, Void> query, Pageable pageable) {
        return retirementService.findRetirementRequests(
                costCenter,
                budget,
                role,
                resource,
                startDate,
                endDate,
                creator,
                consumer,
                description,
                pageable
        );
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        return List.of(new QuerySortOrder("timeCreated", SortDirection.DESCENDING));
    }

    @Override
    protected int sizeInBackEnd(Query<Retirement, Void> query) {
        Long count = retirementService.countRetirementRequests(
                costCenter,
                budget,
                role,
                resource,
                startDate,
                endDate,
                creator,
                consumer,
                description
        );

        return count.intValue();
    }

    public void setCostCenter(CostCenter costCenter) {
        this.costCenter = costCenter;
        refreshAll();
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
        refreshAll();
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
        refreshAll();
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
        refreshAll();
    }


    public void setRole(Role role) {
        this.role = role;
        refreshAll();
    }

    public void setResource(Resource resource) {
        this.resource = resource;
        refreshAll();
    }

    public void setCreator(User creator) {
        this.creator = creator;
        refreshAll();
    }

    public void setConsumer(Consumer consumer) {
        this.consumer = consumer;
        refreshAll();
    }

    public void setDescription(String description) {
        this.description = description;
        refreshAll();
    }

    public void setDateRange(LocalDateTime startDateTime, LocalDateTime endDateTime) {
        this.startDate = startDateTime;
        this.endDate = endDateTime;
        refreshAll();
    }
}
