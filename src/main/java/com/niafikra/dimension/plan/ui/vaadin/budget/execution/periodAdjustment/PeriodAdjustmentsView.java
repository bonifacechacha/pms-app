package com.niafikra.dimension.plan.ui.vaadin.budget.execution.periodAdjustment;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.ui.vaadin.MainHeader;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.plan.domain.Budget;
import com.niafikra.dimension.plan.domain.PeriodAdjustment;
import com.niafikra.dimension.plan.service.BudgetService;
import com.niafikra.dimension.plan.ui.vaadin.budget.AbstractBudgetView;
import com.niafikra.dimension.plan.ui.vaadin.budget.BudgetDisplay;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.MenuBar;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;

import static com.niafikra.dimension.Permission.ADJUST_BUDGET_PERIOD;
import static com.niafikra.dimension.core.security.SecurityUtils.hasAuthority;
import static com.niafikra.dimension.plan.ui.vaadin.budget.execution.periodAdjustment.PeriodAdjustmentsView.VIEW_NAME;


@Secured(Permission.VIEW_BUDGET_PERIOD_ADJUSTMENTS)
@ViewComponent(value = BudgetDisplay.class, caption = "Budget period adjustments")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class PeriodAdjustmentsView extends AbstractBudgetView implements TableExportable {

    public static final String VIEW_NAME = "budget-period-adjustments";

    private MainHeader mainHeader;
    private PeriodAdjustmentForm adjustmentForm;
    private PeriodAdjustmentsGrid adjustmentsGrid;
    private Budget budget;

    private MenuBar.MenuItem requestMenuItem;

    public PeriodAdjustmentsView(MainHeader mainHeader,
                                 PeriodAdjustmentForm adjustmentForm,
                                 PeriodAdjustmentsGrid adjustmentsGrid,
                                 BudgetService budgetService) {
        super(budgetService);
        this.adjustmentForm = adjustmentForm;
        this.mainHeader = mainHeader;
        this.adjustmentsGrid = adjustmentsGrid;
    }

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        adjustmentsGrid.setSizeFull();
        addComponent(adjustmentsGrid);
    }

    private void doCreateAdjustment() {
        adjustmentForm.setEntity(new PeriodAdjustment(budget));
        adjustmentForm
                .openInModalPopup()
                .addCloseListener(e -> adjustmentsGrid.getDataProvider().refreshAll());
    }


    @Override
    public void setBudget(Budget budget) {
        this.budget = budget;
        this.adjustmentsGrid.getDataProvider().filterBudget(budget);
    }

    @Override
    public void attach() {
        super.attach();

        if (hasAuthority(ADJUST_BUDGET_PERIOD)) {
            requestMenuItem = mainHeader.getMenuBar().addItemAsFirst(
                    "Request Adjustment",
                    VaadinIcons.PLUS_CIRCLE,
                    sel -> doCreateAdjustment()
            );
        }
    }

    @Override
    public void detach() {
        super.detach();

        if (hasAuthority(ADJUST_BUDGET_PERIOD)) {
            mainHeader.getMenuBar().removeItem(requestMenuItem);
        }
    }


    @Override
    public TableExport getTableExport() {
        return new ExcelExport(adjustmentsGrid);
    }
}
