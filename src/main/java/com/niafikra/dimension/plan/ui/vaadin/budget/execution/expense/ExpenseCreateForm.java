package com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.attachment.ui.vaadin.util.AttachmentsField;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.*;
import com.niafikra.dimension.group.service.GroupService;
import com.niafikra.dimension.plan.domain.*;
import com.niafikra.dimension.plan.service.*;
import com.vaadin.annotations.PropertyId;
import com.vaadin.data.BeanValidationBinder;
import com.vaadin.data.validator.BeanValidator;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.*;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.fields.LabelField;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Collections;
import java.util.Set;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 10/1/17 7:55 PM
 */
@PrototypeScope
@SpringComponent
public class ExpenseCreateForm extends AbstractForm<Expense> implements Notifier {

    @PropertyId("budget")
    private LabelField<Budget> budgetField = new LabelField<>();
    @PropertyId("payment.time")
    private DateTimeField paymentTimeField = new DateTimeField("Time");

    @PropertyId("role")
    private ComboBox<Role> roleSelector = new ComboBox<>("Role");
    @PropertyId("resource")
    private ComboBox<Resource> resourceSelector = new ComboBox<>("Resource");

    @PropertyId("associatedUser")
    private ComboBox<User> userSelector = new ComboBox<>("User");
    @PropertyId("payment.associate")
    private TextField payeeField = new TextField("Payee");
    @PropertyId("consumer")
    private ComboBox<Consumer> consumerSelector = new ComboBox<>("Consumer");

    @PropertyId("payment.amount")
    private MoneyField amountField;
    @PropertyId("payment.method")
    private ComboBox<PaymentMethod> paymentMethodSelector = new ComboBox<>("Payment");
    @PropertyId("payment.reference")
    private TextField referenceField = new TextField("Reference");

    @PropertyId("payment.description")
    private TextArea notesField = new TextArea("Notes");

    @Inject
    @PropertyId("payment.attachment")
    private AttachmentsField<Set<Attachment>> attachments;

    @Inject
    private TemplateView balanceView;
    @Inject
    private ExpenseService expenseService;
    @Inject
    private ConsumerService consumerService;
    @Inject
    private PaymentMethodService methodService;
    @Inject
    private ResourceService resourceService;

    @Inject
    private CostCenterService costCenterService;

    @Inject
    private GroupService groupService;

    private Budget budget;

    public ExpenseCreateForm() {
        super(Expense.class);
    }

    @PostConstruct
    private void build() {
        this.balanceView.setTemplatePath(Templates.BALANCE);

        addSavedHandler(expense -> {
            doCreate(expense);
        });


        amountField = new MoneyField().withCaption("Amount");
        budgetField.setRequiredIndicatorVisible(false);

        paymentTimeField.setWidth("100%");
        notesField.setWidth("100%");
        notesField.setRows(7);

        roleSelector.setWidth("100%");
        roleSelector.addValueChangeListener(event -> {
            Role role = event.getValue();
            resourceSelector.clear();
            if (role == null) {
                resourceSelector.setItems(Collections.emptyList());
            } else {
                resourceSelector.setItems(resourceService.getResources(role, budget.getCostCenter()));
                userSelector.setItems(groupService.getUsers(role));
            }
        });
        resourceSelector.setWidth("100%");
        resourceSelector.addValueChangeListener(event -> {
            balanceView.putBinding("resource", event.getValue());
            balanceView.putBinding("role", roleSelector.getValue()).render();
        });

        userSelector.setWidth("100%");
        userSelector.addValueChangeListener(event -> {
            User user = event.getValue();
            if (user != null)
                payeeField.setValue(user.getName());
        });

        payeeField.setWidth("100%");
        consumerSelector.setWidth("100%");
        consumerSelector.setItems(consumerService.getAllActive(true));

        amountField.setRequiredIndicatorVisible(true);
        amountField.setWidth("100%");
        paymentMethodSelector.setWidth("100%");
        paymentMethodSelector.setItems(methodService.getAll());
        referenceField.setWidth("100%");

        attachments.setCaption("Attachments");
        attachments.setWidth("100%");

        getSaveButton().setWidth("200px");
        getSaveButton().setCaption("Create expense");
    }

    @Override
    protected void bind() {
        super.bind();
        getBinder()
                .forField(paymentTimeField)
                .asRequired("Must specify time of expense")
                .withValidator(new BeanValidator(Expense.class, "payment.time"))
                .bind(
                        expense -> expense.getPayment().getTime(),
                        (expense, time) -> expense.getPayment().setTime(time)
                );

        getBinder()
                .forField(payeeField)
                .withValidator(new BeanValidator(Expense.class, "payment.associate"))
                .bind(
                        expense -> expense.getPayment().getAssociate(),
                        (expense, associate) -> expense.getPayment().setAssociate(associate)
                );

        getBinder()
                .forField(amountField)
                .asRequired("Must specify expense amount")
                .withValidator(new BeanValidator(Expense.class, "payment.amount"))
                .bind(
                        expense -> expense.getPayment().getAmount(),
                        (expense, value) -> expense.getPayment().setAmount(value)
                );

        getBinder()
                .forField(paymentMethodSelector)
                .asRequired("Must specify method used during payment")
                .withValidator(new BeanValidator(Expense.class, "payment.method"))
                .bind(
                        expense -> expense.getPayment().getMethod(),
                        (expense, value) -> expense.getPayment().setMethod(value)
                );

        getBinder()
                .forField(referenceField)
                .withValidator(new BeanValidator(Expense.class, "payment.reference"))
                .bind(
                        expense -> expense.getPayment().getReference(),
                        (expense, value) -> expense.getPayment().setReference(value)
                );

        getBinder()
                .forField(notesField)
                .withValidator(new BeanValidator(Expense.class, "payment.description"))
                .bind(
                        expense -> expense.getPayment().getDescription(),
                        (expense, value) -> expense.getPayment().setDescription(value)
                );

        getBinder()
                .forField(attachments)
                .withValidator(new BeanValidator(Expense.class, "payment.attachments"))
                .bind(
                        expense -> expense.getPayment().getAttachments(),
                        (expense, attachments) -> expense.getPayment().setAttachments(attachments)
                );

        //prevent showing errors
//        getBinder().setRequiredConfigurator(null);
        getBinder().setValidationStatusHandler(statusChange -> {
        });
    }

    @Override
    public BeanValidationBinder<Expense> getBinder() {
        return (BeanValidationBinder<Expense>) super.getBinder();
    }

    private void doCreate(Expense expense) {
        try {
            expense = expenseService.create(expense);
            init();
            showPrintPaymentVoucher(expense);
            showSuccess("Expense posted successfully", expense.toString());
        } catch (Exception e) {
            showError("Failed to create expense", e);
        }
    }

    private void showPrintPaymentVoucher(Expense expense) {
        PrintWindow
                .create(PaymentPrintable.create(expense.getPayment()), "Do you want to print payment voucher?")
                .open();
    }

    public void init() {
        setEntity(new Expense(budget));
    }

    @Override
    protected Component createContent() {
        Component toolbar = getToolbar();
        return new MVerticalLayout(
                budgetField,
                new MHorizontalLayout(paymentTimeField, roleSelector, resourceSelector).withFullSize(),
                new MHorizontalLayout(userSelector, payeeField, consumerSelector).withFullSize(),
                new MHorizontalLayout(amountField, paymentMethodSelector, referenceField).withFullSize(),
                new MHorizontalLayout(notesField, attachments).withFullWidth(),
                balanceView.withFullWidth(),
                toolbar
        ).withAlign(toolbar, Alignment.MIDDLE_CENTER).withFullWidth().withMargin(true);
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
        init();

        balanceView.putBinding("budget", budget).setValue("");
        roleSelector.clear();
        roleSelector.setItems(costCenterService.getRoles(budget.getCostCenter()));
    }

    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setWidth("70%");
        window.setHeight("80%");
        window.setCaption("Create new expense");
        return window;
    }
}
