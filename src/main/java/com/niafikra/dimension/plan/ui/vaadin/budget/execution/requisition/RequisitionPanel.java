package com.niafikra.dimension.plan.ui.vaadin.budget.execution.requisition;

import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.util.TemplatePanel;
import com.niafikra.dimension.plan.domain.Requisition;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class RequisitionPanel extends MVerticalLayout implements Notifier {

    @Inject
    private TemplatePanel templatePanel;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        this.templatePanel.getTemplateView().setTemplatePath(Templates.REQUISITION);
        templatePanel.setSizeFull();
        withComponents(templatePanel).withExpand(templatePanel, 1);

    }

    public void setRequisition(Requisition requisition) {
        templatePanel.getTemplateView().putBinding("requisition", requisition).render();
    }
}
