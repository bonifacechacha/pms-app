package com.niafikra.dimension.plan.ui.vaadin.role;

import com.niafikra.dimension.core.ui.vaadin.util.create.NameFilterableBackendGrid;
import com.niafikra.dimension.group.service.GroupService;
import com.niafikra.dimension.plan.domain.Role;
import com.niafikra.dimension.plan.service.RoleService;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.inject.Inject;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/6/17 7:20 PM
 */
@SpringComponent
@PrototypeScope
public class RolesBackendGrid extends NameFilterableBackendGrid<Role> {

    @Inject
    private GroupService groupService;
    @Inject
    private RoleService roleService;

    public RolesBackendGrid(RoleProvider dataProvider) {
        super(dataProvider);
        addColumn(t -> groupService.countMembers(t))
                .setId("memberCount")
                .setCaption("Members");

        addColumn(t -> roleService.countResources(t))
                .setId("resourceCount")
                .setCaption("Resources");

    }
}
