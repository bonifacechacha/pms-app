package com.niafikra.dimension.plan.ui.vaadin.budget;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.security.SecurityUtils;
import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.util.SubWindow;
import com.niafikra.dimension.core.ui.vaadin.util.grid.MTreeGrid;
import com.niafikra.dimension.issue.service.IssueService;
import com.niafikra.dimension.plan.domain.Allocation;
import com.niafikra.dimension.plan.domain.Budget;
import com.niafikra.dimension.plan.domain.Resource;
import com.niafikra.dimension.plan.domain.Role;
import com.niafikra.dimension.plan.service.AllocationService;
import com.niafikra.dimension.plan.service.ExpenseService;
import com.niafikra.dimension.plan.service.RequisitionService;
import com.niafikra.dimension.plan.ui.vaadin.budget.issue.AllocationIssueForm;
import com.niafikra.dimension.plan.ui.vaadin.budget.issue.AllocationIssueUtil;
import com.niafikra.dimension.plan.ui.vaadin.budget.issue.AllocationIssuesView;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.MSize;
import org.vaadin.viritin.button.MButton;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.niafikra.dimension.core.ui.vaadin.Theme.TEXT_RIGHT;


@PrototypeScope
@SpringComponent
public class BudgetAllocationsTable extends MTreeGrid<Pair<Role, Resource>> implements Notifier {

    @Inject
    protected AllocationService allocationService;
    @Inject
    protected AllocationPanel allocationPanel;
    @Inject
    private AllocationIssueForm allocationIssueForm;
    @Inject
    private AllocationIssuesView allocationIssuesView;
    @Inject
    private ExpenseService expenseService;
    @Inject
    private RequisitionService requisitionService;
    @Inject
    private IssueService issueService;

    private Budget budget;

    public static boolean isResourceEntry(Pair<Role, Resource> item) {
        return item.getRight() != null;
    }

    public static boolean isRoleEntry(Pair<Role, Resource> item) {
        return item.getRight() == null && item.getLeft() != null;
    }

    @PostConstruct
    private void build() {
        addColumn(pair -> {
            if (isResourceEntry(pair))
                return pair.getRight();
            else return pair.getLeft();
        }).setCaption("Allocation")
                .setId("allocation")
                .setStyleGenerator(item -> {
                    if (isResourceEntry(item)) return Theme.GRID_ROW_RESOURCE;
                    if (isRoleEntry(item)) return Theme.GRID_ROW_ROLE;
                    else return null;
                });


        if (SecurityUtils.hasAuthority(Permission.VIEW_ALLOCATION_ISSUES)) {
            addComponentColumn(pair -> {
                        Role role = pair.getLeft();
                        Resource resource = pair.getRight();
                        if (!isResourceEntry(pair)) return null;

                        Allocation allocation = allocationService.getAllocation(budget, role, resource);

                        return new MButton(
                                VaadinIcons.COMMENT_ELLIPSIS,
                                createCommentCountString(issueService.countIssues(AllocationIssueUtil.getReference(allocation))),
                                event -> showIssues(allocation))
                                .withStyleName(Theme.BUTTON_TINY, Theme.BUTTON_BORDERLESS);
                    }
            ).setCaption("")
                    .setId("issues")
                    .setWidth(70)
                    .setStyleGenerator(pair -> {

                        Role role = pair.getLeft();
                        Resource resource = pair.getRight();
                        if (!isResourceEntry(pair)) return null;

                        Allocation allocation = allocationService.getAllocation(budget, role, resource);
                        return issueService.isResolved(AllocationIssueUtil.getReference(allocation)) ? null : Theme.FONT_COLOR_WARNING;
                    });
            ;
        }


        addStyleNames(Theme.GRID_NO_STRIPES, Theme.GRID_TEXT_SMALL);
        setSelectionMode(Grid.SelectionMode.NONE);


        setDescriptionGenerator(item -> {
            if (isResourceEntry(item)) {
                Allocation allocation = allocationService.getAllocation(budget, item.getLeft(), item.getRight());
                return allocation.getDescription();
            } else return null;
        });

        //after creating the issue then close the popup
        allocationIssueForm.addSavedHandler(issue -> allocationIssueForm.closePopup());

        addItemClickListener(event -> {
            //if clicked resource entry
            if (isResourceEntry(event.getItem()))
                showAllocationTemplate(event.getItem());
        });
    }

    private void showAllocationTemplate(Pair<Role, Resource> item) {
        Allocation allocation = allocationService.getAllocation(budget, item.getLeft(), item.getRight());
        new SubWindow(allocationPanel.setAllocation(allocation))
                .show()
                .withSize(MSize.size("80%","70%"));
    }

    private void showIssues(Allocation allocation) {
        //if there is no issues in the allocation then just show the create form
        if (!issueService.hasIssues(AllocationIssueUtil.getReference(allocation))) {
            if (SecurityUtils.hasAuthority(Permission.POST_ISSUE))
                createIssue(allocation);
            else showWarning("There is no issues on the allocation", allocation.toString());
        } else {
            allocationIssuesView.setAllocation(allocation);
            new SubWindow(allocationIssuesView)
                    .show()
                    .withCaption(allocation + " issues")
                    .withCloseListener(e -> load())
                    .withModal(true)
                    .withCenter()
                    .withSize(MSize.size("80%", "90%"));
        }
    }

    private void createIssue(Allocation allocation) {
        allocationIssueForm.setAllocation(allocation);
        allocationIssueForm.prepareCreateIssue();
        allocationIssueForm
                .openInModalPopup()
                .addCloseListener(e -> {
                    //if the issue was successful created then probably there will
                    if (!issueService.isResolved(AllocationIssueUtil.getReference(allocation))) {
                        showIssues(allocation);
                    }
                });
    }

    private String createCommentCountString(Integer commentCount) {
        return commentCount == 0 ? "" : commentCount > 99 ? "99+" : commentCount.toString();
    }

    public void setBudget(Budget budget) {
        this.budget = budget;

        if (budget.isApproved()) {
            createExecutionColumns();
        } else {
            createPlanningColumns();
        }

        load();
    }

    private void createPlanningColumns() {
        addColumn(pair ->
                allocationService.calculateTotalProposed(
                        this.budget,
                        pair.getLeft(),
                        pair.getRight()))
                .setCaption("Proposed")
                .setId("proposed")
                .setStyleGenerator(item -> TEXT_RIGHT);
    }

    private void createExecutionColumns() {

        addColumn(pair -> allocationService.calculateTotalAllocated(
                this.budget,
                pair.getLeft(),
                pair.getRight()
        )).setCaption("Allocated")
                .setId("allocated")
                .setStyleGenerator(item -> TEXT_RIGHT);

        addColumn(pair -> expenseService.calculateTotalExpenditure(
                this.budget,
                pair.getLeft(),
                pair.getRight()
        )).setCaption("Expenditure")
                .setId("expenditure")
                .setStyleGenerator(item -> TEXT_RIGHT);

        addColumn(pair -> expenseService.calculateTotalBalance(
                this.budget,
                pair.getLeft(),
                pair.getRight()
        )).setCaption("Balance")
                .setId("actualBalance")
                .setStyleGenerator(item -> TEXT_RIGHT);

        addColumn(pair -> requisitionService.calculateTotalGrossBalance(
                this.budget,
                pair.getLeft(),
                pair.getRight()
        )).setCaption("Gross")
                .setId("grossBalance")
                .setHidable(true)
                .setHidden(true)
                .setStyleGenerator(item -> TEXT_RIGHT);
    }

    public void load() {
        setItems(getRoots(), triple -> getChildren(triple));
        expandRoots();
    }

    private List<Pair<Role, Resource>> getChildren(Pair<Role, Resource> triple) {
        if (triple.getRight() != null) return Collections.emptyList();
        else if (triple.getLeft() != null) return createResourceEntries(triple.getLeft());
        else
            throw new IllegalArgumentException("A pair entry for the allocation table must have atlease one of its element not null");
    }

    private List<Pair<Role, Resource>> createResourceEntries(Role role) {
        return allocationService.getAllocatedResources(budget, role)
                .stream()
                .map(resource -> ImmutablePair.of(role, resource))
                .collect(Collectors.toList());

    }

    private List<Pair<Role, Resource>> getRoots() {
        if (budget == null) return Collections.emptyList();
        return allocationService.getAllocatedRoles(budget, null)
                .stream()
                .map(role -> ImmutablePair.<Role, Resource>of(role, null))
                .collect(Collectors.toList());
    }
}