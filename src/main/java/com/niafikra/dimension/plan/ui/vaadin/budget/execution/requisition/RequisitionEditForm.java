package com.niafikra.dimension.plan.ui.vaadin.budget.execution.requisition;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.attachment.ui.vaadin.util.AttachmentsField;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.AbstractForm;
import com.niafikra.dimension.core.ui.vaadin.util.MoneyField;
import com.niafikra.dimension.core.ui.vaadin.util.TemplateView;
import com.niafikra.dimension.money.Money;
import com.niafikra.dimension.plan.domain.*;
import com.niafikra.dimension.plan.service.*;
import com.vaadin.data.validator.BeanValidator;
import com.vaadin.ui.*;
import org.vaadin.viritin.fields.LabelField;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Collections;
import java.util.Set;

public class RequisitionEditForm extends AbstractForm<Requisition> {

    @Inject
    protected RequisitionService requisitionService;
    @Inject
    protected ConsumerService consumerService;
    @Inject
    protected AllocationService allocationService;
    @Inject
    protected BudgetService budgetService;
    @Inject
    protected ResourceService resourceService;
    @Inject
    protected RoleService roleService;

    protected LabelField<Budget> budget = new LabelField<>();
    protected ComboBox<Role> role = new ComboBox<>("Role");
    protected ComboBox<Resource> resource = new ComboBox<>("Resource");
    protected ComboBox<Consumer> consumer = new ComboBox<>("Consumer");
    protected MoneyField amountField = new MoneyField("Amount");
    protected TextArea description = new TextArea("Notes");


    @Inject
    protected AttachmentsField<Set<Attachment>> attachments;

    @Inject
    protected TemplateView balanceView;

    @Inject
    protected User currentUser;

    public RequisitionEditForm() {
        super(Requisition.class);
    }


    @PostConstruct
    private  void build(){

        this.balanceView.setTemplatePath(Templates.BALANCE);
        budget.setRequiredIndicatorVisible(false);

        resource.setWidth("100%");
        resource.addValueChangeListener(event -> {
            Resource resource = event.getValue();
            role.clear();
            if (resource == null) {
                role.setItems(Collections.emptyList());
            } else {
                Set<Role> roles = allocationService.findRoles(getBudget(),currentUser,resource);

                role.setItems(roles);
                if (roles.size() == 1)
                    role.setValue(roles.stream().findFirst().get());
            }

        });

        role.setWidth("100%");
        role.addValueChangeListener(event -> {
            balanceView
                    .putBinding("resource", resource.getValue())
                    .putBinding("role", event.getValue())
                    .render();
        });

        consumer.setWidth("100%");
        consumer.setItems(consumerService.getAllActive(true));

        amountField.setWidth("100%");

        description.setWidth("100%");
        description.setRows(7);
        description.setPlaceholder("Describe your request");
        attachments.setCaption("Attachments");
        attachments.setWidth("100%");
    }
    @Override
    protected void bind() {
        super.bind();
        getBinder()
                .forField(amountField)
                .asRequired("Must specify requested amountField")
                //the validator for amount requested will also work even in amount approved
                .withValidator(new BeanValidator(Requisition.class, "requestedAmount"))

                //once the requisition is submitted then every changes to the amount are as approved amount
                .bind(requisition -> getAmount(requisition), (requisition, amount) -> setAmount(requisition, amount));
    }

    private void setAmount(Requisition requisition, Money amount) {
        if (requisitionService.isSubmitted(requisition))
            requisition.setApprovedAmount(amount);
        else requisition.setRequestedAmount(amount);
    }

    private Money getAmount(Requisition requisition) {
        if (requisitionService.isSubmitted(requisition))
            return requisition.getApprovedAmount();
        else return requisition.getRequestedAmount();
    }

    @Override
    protected Component createContent() {
        Component toolbar = getToolbar();
        return new MVerticalLayout(
                budget,
                new MHorizontalLayout(resource, role).withFullWidth(),
                new MHorizontalLayout(amountField, consumer).withFullWidth(),
                new MHorizontalLayout(description, attachments).withFullWidth(),
                balanceView.withFullWidth(),
                toolbar
        ).withFullWidth().withAlign(toolbar, Alignment.MIDDLE_CENTER).withMargin(true);
    }

    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setWidth("70%");
        window.setHeight("100%");
        return window;
    }

    public void setEntity(Requisition requisition) {
        Budget budget = requisition.getBudget();
        if (budget == null)
            throw new IllegalArgumentException("Requisition must have budget specified to be editor by this component");

        balanceView.putBinding("budget", budget);

        resource.setItems(allocationService.findResources(budget,currentUser));

        super.setEntity(requisition);
    }


    public Window show(Requisition requisition) {
        setRequisition(requisition);
        return openInModalPopup();
    }

    public Requisition getRequisition() {
        return getEntity();
    }

    public void setRequisition(Requisition requisition) {
        setEntity(requisition);
    }

    private Budget getBudget() {
        return getRequisition().getBudget();
    }
}
