package com.niafikra.dimension.plan.ui.vaadin.budget;

import com.niafikra.dimension.core.ui.vaadin.util.date.LocalDateTimeRangeSelector;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.util.DateUtils;
import com.niafikra.dimension.plan.domain.Budget;
import com.niafikra.dimension.plan.service.BudgetService;
import com.niafikra.dimension.plan.service.ExpenseService;
import com.niafikra.dimension.plan.ui.vaadin.costCenter.CostCenterProvider;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.HeaderRow;
import org.vaadin.spring.annotation.PrototypeScope;

import java.time.format.DateTimeFormatter;

import static com.niafikra.dimension.core.ui.vaadin.Theme.TEXT_RIGHT;
import static com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils.*;

@SpringComponent
@PrototypeScope
public class BudgetsTable extends Grid<Budget> {

    private BudgetsProvider budgetsProvider;
    private BudgetService budgetService;
    private ExpenseService expenseService;
    private LocalDateTimeRangeSelector timeCreatedRangFilter;
    private CostCenterProvider costCenterProvider;

    private Navigator navigator;

    public BudgetsTable(BudgetsProvider budgetsProvider,
                        Navigator navigator,
                        CostCenterProvider costCenterProvider,
                        BudgetService budgetService,
                        ExpenseService expenseService) {
        this.budgetsProvider = budgetsProvider;
        this.navigator = navigator;
        this.budgetService = budgetService;
        this.costCenterProvider = costCenterProvider;
        this.expenseService = expenseService;

        setDataProvider(budgetsProvider);

        addColumn(budget -> budget.getTimeCreated().format(DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss")))
                .setId("timeCreated")
                .setCaption("Created On")
                .setHidable(true)
                .setSortable(true);

        addColumn(Budget::getCostCenter)
                .setId("costCenter")
                .setCaption("Cost center")
                .setHidable(true)
                .setSortable(true);

        addColumn(Budget::getTitle)
                .setId("title")
                .setCaption("Title")
                .setHidable(true)
                .setSortable(true);

        addColumn(budget -> budgetService.getStatus(budget))
                .setId("status")
                .setCaption("Status")
                .setHidable(true)
                .setSortable(false);

        addColumn(budget -> budgetService.calculateTotalProposed(budget))
                .setId("proposed")
                .setCaption("Proposed")
                .setHidable(true)
                .setSortable(false)
                .setStyleGenerator(item -> TEXT_RIGHT);

        addColumn(budget -> budgetService.calculateTotalAllocation(budget))
                .setId("allocated")
                .setCaption("Allocated")
                .setHidable(true)
                .setSortable(false)
                .setStyleGenerator(item -> TEXT_RIGHT);

        addColumn(budget -> expenseService.calculateTotalBalance(budget))
                .setId("balance")
                .setCaption("Balance")
                .setHidable(true)
                .setSortable(false)
                .setStyleGenerator(item -> TEXT_RIGHT);

        addColumn(budget -> DateUtils.formatDate(budget.getStartDate()))
                .setId("startDate")
                .setCaption("Start date")
                .setHidable(true)
                .setSortable(true);

        addColumn(budget -> DateUtils.formatDate(budget.getEndDate()))
                .setId("endDate")
                .setCaption("End date")
                .setHidable(true)
                .setSortable(true);

        HeaderRow headerRow = appendHeaderRow();
        timeCreatedRangFilter = new LocalDateTimeRangeSelector();
        timeCreatedRangFilter.setWidth("200px");
        headerRow.getCell("timeCreated").setComponent(timeCreatedRangFilter);
        timeCreatedRangFilter.addValueChangeListener(event -> {
            budgetsProvider.filterTimeCreatedRange(timeCreatedRangFilter.getStart(), timeCreatedRangFilter.getEnd());
        });

        createFilteringComboBox(
                headerRow,
                "costCenter",
                "Filter by cost center",
                costCenterProvider,
                event -> budgetsProvider.filterCostCenter(event.getValue())
        );

        createFilteringTextField(
                headerRow,
                "title",
                "Filter budget title",
                event -> budgetsProvider.filterTitle(event.getValue())
        );

        createFilteringCheckBox(
                headerRow,
                "status",
                "Show archived budgets",
                //set null to ignore archive flag or false to show not archived only
                event -> budgetsProvider.filterArchived(event.getValue() ? null : false)
        );

        setSelectionMode(Grid.SelectionMode.NONE);
        addItemClickListener(event -> showBudget(event.getItem()));
    }


    private void showBudget(Budget budget) {
        navigator.navigateTo(BudgetAllocationsView.class, budget.getId());
    }

    public BudgetsProvider getBudgetsProvider() {
        return budgetsProvider;
    }
}
