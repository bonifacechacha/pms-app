package com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.plan.domain.Budget;
import com.niafikra.dimension.plan.service.BudgetService;
import com.niafikra.dimension.plan.service.ExpenseService;
import com.niafikra.dimension.plan.service.RequisitionService;
import com.niafikra.dimension.plan.ui.vaadin.budget.UserBudgetsSelector;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

import java.util.Collection;

@PrototypeScope
@SpringComponent
public class UserRequisitionBudgetSelector extends UserBudgetsSelector {

    private BudgetService budgetService;

    public UserRequisitionBudgetSelector(BudgetService budgetService,
                                         ExpenseService expenseService,
                                         RequisitionService requisitionService,
                                         User currentUser) {
        super(expenseService, requisitionService, currentUser);
        this.budgetService = budgetService;
    }

    @Override
    protected Collection<Budget> findBudgets() {
        return budgetService.findActiveAssociatedBudgets(currentUser);
    }
}
