package com.niafikra.dimension.plan.ui.vaadin.budget.execution.retirement;

import com.niafikra.dimension.core.security.service.UserService;
import com.niafikra.dimension.core.ui.vaadin.user.UsersProvider;
import com.niafikra.dimension.core.ui.vaadin.util.date.LocalDateTimeRangeSelector;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.util.DateUtils;
import com.niafikra.dimension.plan.domain.Retirement;
import com.niafikra.dimension.plan.service.RetirementService;
import com.niafikra.dimension.plan.ui.vaadin.consumer.ConsumerProvider;
import com.niafikra.dimension.plan.ui.vaadin.costCenter.CostCenterProvider;
import com.niafikra.dimension.plan.ui.vaadin.resource.ResourceComboBox;
import com.niafikra.dimension.plan.ui.vaadin.role.RoleProvider;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.HeaderRow;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.core.ui.vaadin.Theme.TEXT_RIGHT;
import static com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils.createFilteringComboBox;

@PrototypeScope
@SpringComponent
public class RetirementsGrid extends Grid<Retirement> {

    @Inject
    private UsersProvider usersProvider;
    @Inject
    private CostCenterProvider costCenterProvider;
    @Inject
    private RoleProvider roleProvider;
    @Inject
    private ResourceComboBox resourceFilter;
    @Inject
    private ConsumerProvider consumerProvider;
    @Inject
    private Navigator navigator;
    @Inject
    private RetirementService retirementService;

    private RetirementsGrid(RetirementsProvider retirementsProvider) {
        super(retirementsProvider);
    }

    @Override
    public RetirementsProvider getDataProvider() {
        return (RetirementsProvider) super.getDataProvider();
    }

    @PostConstruct
    private void build() {

        addColumn(retirement -> DateUtils.formatDateTime(retirement.getTimeCreated()))
                .setId("timeCreated")
                .setCaption("Time")
                .setHidable(true)
                .setSortable(true);

        addColumn(Retirement::getCreator)
                .setId("creator")
                .setCaption("Person")
                .setHidable(true)
                .setSortable(true);

        addColumn(retirement -> retirement.getExpense().getPaidAmount())
                .setId("expense.payment.amount")
                .setCaption("Payment")
                .setSortable(false)
                .setStyleGenerator(item -> TEXT_RIGHT);

        addColumn(retirement -> retirement.getTotal())
                .setId("totalRetirement")
                .setCaption("Retired")
                .setSortable(false)
                .setHidable(true)
                .setStyleGenerator(item -> TEXT_RIGHT);

        addColumn(retirement -> retirement.getTotalAccepted())
                .setId("totalAccepted")
                .setCaption("Accepted")
                .setSortable(false)
                .setHidable(true)
                .setStyleGenerator(item -> TEXT_RIGHT);

        addColumn(requisition -> retirementService.getStatus(requisition))
                .setId("status")
                .setCaption("Status")
                .setHidable(true)
                .setSortable(false);

        addColumn(Retirement::getRole)
                .setId("expense.role")
                .setCaption("Role")
                .setHidable(true)
                .setSortable(true);

        addColumn(Retirement::getResource)
                .setId("expense.resource")
                .setCaption("Resource")
                .setHidable(true)
                .setSortable(true);

        addColumn(Retirement::getConsumer)
                .setId("expense.consumer")
                .setCaption("Consumer")
                .setHidable(true)
                .setSortable(true);

        addColumn(Retirement::getBudget)
                .setId("expense.budget")
                .setCaption("Budget")
                .setHidable(true)
                .setSortable(true);


        LocalDateTimeRangeSelector timeCreatedRangePanel = new LocalDateTimeRangeSelector();
        timeCreatedRangePanel.setWidth("200px");

        HeaderRow headerRow = appendHeaderRow();
        headerRow.getCell("timeCreated").setComponent(timeCreatedRangePanel);
        timeCreatedRangePanel.addValueChangeListener(event -> {
            getDataProvider().setDateRange(timeCreatedRangePanel.getStart(), timeCreatedRangePanel.getEnd());
        });

        createFilteringComboBox(
                headerRow,
                "creator",
                "Filter creator",
                usersProvider.withConvertedFilter(name -> UserService.UserFilter.builder().name(name).build()),
                event -> getDataProvider().setCreator(event.getValue())
        );
        createFilteringComboBox(
                headerRow,
                "expense.budget",
                "Filter cost center",
                costCenterProvider,
                event -> getDataProvider().setCostCenter(event.getValue())
        );

        createFilteringComboBox(
                headerRow,
                "expense.role",
                "Filter role",
                roleProvider,
                event -> getDataProvider().setRole(event.getValue())
        );

        resourceFilter.setPlaceholder("Filter resource");
        headerRow.getCell("expense.resource").setComponent(
                resourceFilter.withValueChangeListener(event -> {
                    getDataProvider().setResource(event.getValue());
                })
        );

        createFilteringComboBox(
                headerRow,
                "expense.consumer",
                "Filter consumer",
                consumerProvider,
                event -> getDataProvider().setConsumer(event.getValue())
        );

        addItemClickListener(event -> {
            showRetirement(event.getItem());
        });
    }

    private void showRetirement(Retirement retirement) {
        navigator.navigateTo(RetirementView.class, retirement.getId());
    }
}
