package com.niafikra.dimension.plan.ui.vaadin.budget.execution.periodAdjustment;

import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.TemplatePanel;
import com.niafikra.dimension.plan.domain.PeriodAdjustment;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;

@PrototypeScope
@SpringComponent
public class PeriodAdjustmentPanel extends VerticalLayout {

    private TemplatePanel templatePanel;

    public PeriodAdjustmentPanel(TemplatePanel templatePanel) {
        this.templatePanel = templatePanel;
        this.templatePanel.getTemplateView().setTemplatePath(Templates.PERIOD_ADJUSTMENT);
    }

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);
        addComponent(templatePanel);
    }


    public void setPeriodAdjustment(PeriodAdjustment adjustment) {
        templatePanel.getTemplateView().putBinding("periodAdjustment", adjustment);
        templatePanel.getTemplateView().render();
    }
}
