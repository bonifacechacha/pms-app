package com.niafikra.dimension.plan.ui.vaadin.budget;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.util.DateUtils;
import com.niafikra.dimension.plan.domain.Budget;
import com.niafikra.dimension.plan.service.ExpenseService;
import com.niafikra.dimension.plan.service.RequisitionService;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.ui.Grid;

import javax.annotation.PostConstruct;
import java.util.Collection;


public abstract class UserBudgetsSelector extends Grid<Budget> {

    protected ExpenseService expenseService;
    protected RequisitionService requisitionService;
    protected User currentUser;

    public UserBudgetsSelector(ExpenseService expenseService,
                               RequisitionService requisitionService,
                               User currentUser) {

        this.expenseService = expenseService;
        this.requisitionService = requisitionService;
        this.currentUser = currentUser;

        addColumn(budget -> DateUtils.formatDateTime(budget.getTimeCreated()))
                .setId("timeCreated")
                .setCaption("Created On")
                .setSortable(true);

        addColumn(Budget::getCostCenter)
                .setId("costCenter")
                .setCaption("Cost center")
                .setSortable(true);

        addColumn(Budget::getTitle)
                .setId("title")
                .setCaption("Title")
                .setSortable(true);

        addColumn(budget -> expenseService.calculateTotalRolesBalance(budget, currentUser, null))
                .setId("userBalance")
                .setCaption("My Balance")
                .setSortable(true);

        addColumn(budget -> requisitionService.calculateTotalGrossRolesBalance(budget, currentUser, null))
                .setId("userGrossBalance")
                .setCaption("My Gross")
                .setSortable(true);
    }

    @PostConstruct
    public void load() {
        Collection<Budget> budgets = findBudgets();
        setItems(budgets);
    }

    protected abstract Collection<Budget> findBudgets();

    public Collection<Budget> getBudgets() {
        ListDataProvider<Budget> provider = (ListDataProvider<Budget>) getDataProvider();
        return provider.getItems();
    }
}
