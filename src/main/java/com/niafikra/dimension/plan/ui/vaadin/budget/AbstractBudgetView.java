package com.niafikra.dimension.plan.ui.vaadin.budget;

import com.niafikra.dimension.plan.domain.Budget;
import com.niafikra.dimension.plan.service.BudgetService;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import org.vaadin.viritin.layouts.MVerticalLayout;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/26/17 12:53 AM
 */
public abstract class AbstractBudgetView extends MVerticalLayout implements View {
    protected BudgetService budgetService;

    public AbstractBudgetView(BudgetService budgetService) {
        this.budgetService = budgetService;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long budgetId = Long.parseLong(event.getParameters());
        Budget budget = budgetService.getBudget(budgetId);
        setBudget(budget);
    }

    public abstract void setBudget(Budget budget);
}
