package com.niafikra.dimension.plan.ui.vaadin.budget.execution.retirement;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.security.SecurityUtils;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.plan.domain.Expense;
import com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense.ExpensesGrid;
import com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense.SettlementPaymentForm;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid.GridContextClickEvent;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.Permission.VIEW_PENDING_SETTLEMENT_EXPENSES;
import static com.niafikra.dimension.plan.ui.vaadin.budget.execution.retirement.PendingSettlementExpensesView.VIEW_NAME;

@Secured(VIEW_PENDING_SETTLEMENT_EXPENSES)
@ViewComponent(value = MainDisplay.class, caption = "Pending settlement")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.FINANCE, caption = "Pending Settlement")
@VaadinFontIcon(VaadinIcons.MONEY_EXCHANGE)
public class PendingSettlementExpensesView extends VerticalLayout implements View, TableExportable {
    public static final String VIEW_NAME = "settle-retirements";

    @Inject
    private ExpensesGrid expensesGrid;
    @Inject
    private SettlementPaymentForm paymentForm;
    @Inject
    private Navigator navigator;

    @PostConstruct
    private void build() {

        setSizeFull();
        setMargin(false);
        expensesGrid.setSizeFull();
        expensesGrid.getExpenseFilter().setRetired(true);
        expensesGrid.getExpenseFilter().setReconciled(false);
        expensesGrid.getDataProvider().refreshAll();
        addComponent(expensesGrid);
        setExpandRatio(expensesGrid, 1);

        if (SecurityUtils.hasAuthority(Permission.SETTLE_RETIREMENT)) {
            expensesGrid.addContextClickListener(event -> {
                GridContextClickEvent<Expense> contextEvent = (GridContextClickEvent<Expense>) event;
                showPaymentWindow(contextEvent.getItem());
            });
        }
    }

    private void showPaymentWindow(Expense expense) {
        paymentForm.setExpense(expense);
        paymentForm.openInModalPopup().addCloseListener(e -> UI.getCurrent().access(() -> navigator.reload()));
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(expensesGrid);
    }
}