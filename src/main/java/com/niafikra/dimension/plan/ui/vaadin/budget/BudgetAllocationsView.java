package com.niafikra.dimension.plan.ui.vaadin.budget;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.plan.domain.*;
import com.niafikra.dimension.plan.service.AllocationService;
import com.niafikra.dimension.plan.service.BudgetService;
import com.niafikra.dimension.plan.ui.vaadin.budget.planning.ChangeAllocationForm;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.dialogs.ConfirmDialog;

import javax.annotation.PostConstruct;

import static com.niafikra.dimension.plan.ui.vaadin.budget.BudgetAllocationsView.VIEW_NAME;
import static com.vaadin.ui.themes.ValoTheme.MENUBAR_BORDERLESS;
import static com.vaadin.ui.themes.ValoTheme.MENUBAR_SMALL;

@Secured(Permission.VIEW_BUDGET_ALLOCATIONS)
@ViewComponent(value = BudgetDisplay.class, caption = "Budget allocations")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class BudgetAllocationsView extends VerticalLayout implements BudgetView {
    public static final String VIEW_NAME = "budget-allocations";

    private BudgetAllocationsTable allocationsGrid;
    private ChangeAllocationForm changeAllocationForm;
    private BudgetSummaryPanel budgetSummaryPanel;
    private Navigator navigator;
    private Budget budget;
    private AllocationService allocationService;
    private BudgetService budgetService;

    public BudgetAllocationsView(BudgetAllocationsTable allocationsGrid,
                                 ChangeAllocationForm changeAllocationForm,
                                 BudgetSummaryPanel budgetSummaryPanel,
                                 Navigator navigator,
                                 AllocationService allocationService,
                                 BudgetService budgetService) {
        this.allocationsGrid = allocationsGrid;
        this.changeAllocationForm = changeAllocationForm;
        this.budgetSummaryPanel = budgetSummaryPanel;
        this.navigator = navigator;
        this.budgetService = budgetService;
        this.allocationService = allocationService;
    }

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        HorizontalSplitPanel base = new HorizontalSplitPanel();
//        base.setFirstComponent(new MVerticalLayout(budgetSummaryPanel).withFullSize().withMargin(true));
        base.setFirstComponent(budgetSummaryPanel);
        allocationsGrid.setSizeFull();
//        base.setSecondComponent(new MVerticalLayout(allocationsGrid).withFullSize().withMargin(true));
        base.setSecondComponent(allocationsGrid);
        base.setSplitPosition(35);
        base.setSizeFull();
        addComponent(base);
        setExpandRatio(base, 1);
    }


    private void doChangeAllocation(Pair<Role, Resource> pair) {
        Allocation allocation = allocationService.getAllocation(budget, pair.getLeft(), pair.getRight());
        changeAllocationForm.setEntity(new AllocationProposal(allocation));
        Window subWindow = changeAllocationForm.openInModalPopup();
        subWindow.addCloseListener(e -> UI.getCurrent().access(() -> setBudget(budget)));
    }


    private void doRemove(Pair<Role, Resource> pair) {
        ConfirmDialog.show(
                getUI(), "Are you sure you?",
                dialog -> {
                    if (dialog.isConfirmed()) {
                        allocationService.deleteAllocations(budget, pair.getLeft(), pair.getRight());
                        UI.getCurrent().access(() -> navigator.reload());
                    }
                }
        );
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
        allocationsGrid.setBudget(budget);
        budgetSummaryPanel.setBudget(budget);

        if (!budget.isApproved()) {

            allocationsGrid.addComponentColumn(pair -> {

                MenuBar menu = new MenuBar();
                menu.addStyleNames(MENUBAR_BORDERLESS, MENUBAR_SMALL);

                MenuBar.MenuItem optionsMenu = menu.addItem("", null);

                if (budget.isWaiting()) {
                    if (pair.getRight() != null)//only allow change for the resource entries
                        optionsMenu.addItem("Change", selectedItem -> doChangeAllocation(pair));
                    optionsMenu.addItem("Remove", selectedItem -> doRemove(pair));
                }
                return menu;
            }).setId("change").setWidth(70);
        }
    }

    @Override
    public void show(Long budgetId) {
        setBudget(budgetService.getBudget(budgetId));
    }
}
