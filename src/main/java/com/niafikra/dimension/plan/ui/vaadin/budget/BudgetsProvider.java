package com.niafikra.dimension.plan.ui.vaadin.budget;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.util.ProviderUtils;
import com.niafikra.dimension.plan.domain.Budget;
import com.niafikra.dimension.plan.domain.CostCenter;
import com.niafikra.dimension.plan.domain.Role;
import com.niafikra.dimension.plan.service.BudgetService;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.spring.annotation.SpringComponent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;
import org.vaadin.spring.annotation.PrototypeScope;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/12/17 7:43 PM
 */
//TODO CHANGE THE IMPLEMENTATION TO USE PROVIDER FILTER GENERIC AND REMOVE THIS METHOD
@SpringComponent
@PrototypeScope
public class BudgetsProvider extends PageableDataProvider<Budget, String> {

    private BudgetService budgetService;

    private Boolean approved;
    private Boolean archived=false;
    private CostCenter costCenter;
    private Role role;
    private User userFilter;
    private LocalDateTime endDateTime;
    private LocalDateTime startDateTime;
    private String titleFilter;

    public BudgetsProvider(BudgetService budgetService) {
        this.budgetService = budgetService;
    }

    @Override
    protected Page<Budget> fetchFromBackEnd(Query<Budget, String> query, Pageable pageable) {
        return budgetService.findBudgets(
                getFilter(query),
                pageable
        );
    }

    //TODO CHANGE THE IMPLEMENTATION TO USE PROVIDER FILTER GENERIC AND REMOVE THIS METHOD
    private BudgetService.BudgetFilter getFilter(Query<Budget, String> query) {
       return BudgetService.BudgetFilter.builder()
                .titleFilter(ProviderUtils.getFilter(query, titleFilter))
                .costCenter(costCenter)
                .role(role)
                .planner(userFilter)
                .startTime(startDateTime)
                .endTime(endDateTime)
                .approved(approved)
                .archived(archived)
                .build();
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        List<QuerySortOrder> sortOrders = new ArrayList<>();
        sortOrders.add(new QuerySortOrder("timeCreated", SortDirection.DESCENDING));
        return sortOrders;
    }

    @Override
    protected int sizeInBackEnd(Query<Budget, String> query) {
        Long count = budgetService.countBudgets(getFilter(query));
        return count.intValue();
    }

    public void filterApproved(Boolean approved) {
        this.approved = approved;
        refreshAll();
    }

    public void filterCostCenter(CostCenter center) {
        this.costCenter = center;
        refreshAll();
    }

    public void filterTimeCreatedRange(LocalDateTime startDateTime, LocalDateTime endDateTime) {
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        refreshAll();
    }

    public void filterTitle(String titleFilter) {
        this.titleFilter = titleFilter;
        refreshAll();
    }

    public void filterUser(User currentUser) {
        this.userFilter = currentUser;
        refreshAll();
    }

    public void filterArchived(Boolean archived) {
        this.archived = archived;
        refreshAll();
    }
}
