package com.niafikra.dimension.plan.ui.vaadin.budget.issue;

import com.niafikra.dimension.plan.domain.Allocation;

public class AllocationIssueUtil {
    public static String getReference(Allocation allocation) {
        return String.format("%s:%d", Allocation.class.getCanonicalName(), allocation.getId());
    }
}
