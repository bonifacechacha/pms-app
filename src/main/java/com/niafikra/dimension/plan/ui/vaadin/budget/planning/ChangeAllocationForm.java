package com.niafikra.dimension.plan.ui.vaadin.budget.planning;

import com.niafikra.dimension.core.ui.vaadin.util.AbstractForm;
import com.niafikra.dimension.core.ui.vaadin.util.MoneyField;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.plan.domain.AllocationProposal;
import com.niafikra.dimension.plan.domain.Resource;
import com.niafikra.dimension.plan.domain.Role;
import com.niafikra.dimension.plan.service.AllocationService;
import com.vaadin.annotations.PropertyId;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.Window;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.fields.LabelField;
import org.vaadin.viritin.layouts.MFormLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/20/17 7:49 PM
 */
@SpringComponent
@PrototypeScope
public class ChangeAllocationForm extends AbstractForm<AllocationProposal> implements Notifier {

    public static final String FIELD_WIDTH = "400px";

    @PropertyId("role")
    private LabelField<Role> roleLabel = new LabelField<>("Role");

    @PropertyId("resource")
    private LabelField<Resource> resourceLabel = new LabelField<>("Resource");

    @PropertyId("proposedAmount")
    private MoneyField amountField = new MoneyField("Amount");

    @PropertyId("description")
    private TextArea descriptionField = new TextArea("Description");

    @PropertyId("reason")
    private TextArea reasonField = new TextArea("Reason");

    public ChangeAllocationForm(AllocationService allocationService) {
        super(AllocationProposal.class);

        roleLabel.setWidth(FIELD_WIDTH);
        resourceLabel.setWidth(FIELD_WIDTH);
        amountField.setWidth(FIELD_WIDTH);
        descriptionField.setWidth(FIELD_WIDTH);
        descriptionField.setRows(4);
        reasonField.setWidth(FIELD_WIDTH);
        reasonField.setRows(4);
        getSaveButton().setCaption("Submit");
        getSaveButton().setWidth("200px");
        getSaveButton().setIcon(VaadinIcons.CHECK_SQUARE);

        addSavedHandler(allocation -> {
            try {
                AllocationProposal proposal = getAllocation();
                allocationService.propose(
                        proposal.getBudget(),
                        proposal.getRole(),
                        proposal.getResource(),
                        proposal.getProposedAmount(),
                        proposal.getDescription(),
                        proposal.getReason()
                );
                closePopup();
            } catch (Exception e) {
                showError("Failed to allocate resource", e.getMessage());
            }
        });

    }

    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setCaption("Change allocation");
        window.setWidth("600px");
        return window;
    }


    private AllocationProposal getAllocation() {
        return getBinder().getBean();
    }

    @Override
    protected Component createContent() {
        return new MVerticalLayout(
                new MFormLayout(
                        roleLabel,
                        resourceLabel,
                        amountField,
                        descriptionField,
                        reasonField,
                        getToolbar()));
    }
}