package com.niafikra.dimension.plan.ui.vaadin.budget.report;

import com.byteowls.vaadin.chartjs.ChartJs;
import com.byteowls.vaadin.chartjs.config.BarChartConfig;
import com.byteowls.vaadin.chartjs.data.BarDataset;
import com.byteowls.vaadin.chartjs.options.Position;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.report.ReportDisplay;
import com.niafikra.dimension.core.ui.vaadin.report.ReportView;
import com.niafikra.dimension.core.ui.vaadin.util.Charts;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.niafikra.dimension.plan.domain.Role;
import com.niafikra.dimension.plan.service.AllocationService;
import com.niafikra.dimension.plan.service.ExpenseService;
import com.niafikra.dimension.plan.service.RoleService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.viritin.layouts.MHorizontalLayout;

import javax.annotation.PostConstruct;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.niafikra.dimension.Permission.VIEW_MONTHLY_ROLE_EXPENDITURE_CHART;
import static com.niafikra.dimension.plan.ui.vaadin.budget.report.RoleConsumptionReport.VIEW_NAME;

@Secured(VIEW_MONTHLY_ROLE_EXPENDITURE_CHART)
@ViewComponent(value = ReportDisplay.class, caption = "Role Consumption")
@ViewInfo(icon = VaadinIcons.MALE, section = "Expenditure", value = "Roles Consumption")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class RoleConsumptionReport extends VerticalLayout implements ReportView {
    public static final String VIEW_NAME = "role-consumption";

    private User currentUser;
    private AllocationService allocationService;
    private ExpenseService expenseService;
    private RoleService roleService;

    public RoleConsumptionReport(User currentUser,
                                 AllocationService allocationService,
                                 ExpenseService expenseService,
                                 RoleService roleService) {
        this.currentUser = currentUser;
        this.allocationService = allocationService;
        this.expenseService = expenseService;
        this.roleService = roleService;
    }


    @PostConstruct
    private void build() {
        setSizeFull();
        MHorizontalLayout base = new MHorizontalLayout().withFullSize();
        base.addComponent(createRoleConsumptionBarChart());
        addComponent(base);
    }


    private ChartJs createRoleConsumptionBarChart() {

        List<Role> roles = roleService.getAll();
        roles.sort(Comparator.comparing(Role::getName));

        List<String> roleNames = roles.stream()
                .map(role -> role.getName())
                .collect(Collectors.toList());

        BarChartConfig config = new BarChartConfig();
        config.horizontal()
                .data()
                .labelsAsList(roleNames)
                .addDataset(new BarDataset().type().label("Budget").backgroundColor(Charts.COLOR_1))
                .addDataset(new BarDataset().type().label("Expenditure").backgroundColor(Charts.COLOR_2))
                .and()
                .options()
                .responsive(true)
                .maintainAspectRatio(true)
                .title()
                .display(true)
                .position(Position.TOP)
                .text("Roles Monthly Budget Expenditure")
                .and()
                .animation()
                .and()
                .done();

        BarDataset budgetDataSet = (BarDataset) config.data().getDatasetAtIndex(0);
        List<Double> budgetData = roles.stream()
                .map(role -> allocationService.calculateTotalMonthlyAllocated(role).toBaseCurrency().getAmount().doubleValue())
                .collect(Collectors.toList());
        budgetDataSet.dataAsList(budgetData);

        BarDataset expenditureDataSet = (BarDataset) config.data().getDatasetAtIndex(1);
        List<Double> expenditureData = roles.stream()
                .map(role -> expenseService.calculateTotalMonthlyExpenses(role).toBaseCurrency().getAmount().doubleValue())
                .collect(Collectors.toList());
        expenditureDataSet.dataAsList(expenditureData);

        ChartJs chart = new ChartJs(config);
//        chart.setWidth(100, Unit.PERCENTAGE);
//        chart.setHeight(400,Unit.PIXELS);
        chart.setSizeFull();
        chart.setJsLoggingEnabled(true);

        return chart;

    }
}
