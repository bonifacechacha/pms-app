package com.niafikra.dimension.plan.ui.vaadin.budget;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.attachment.ui.vaadin.util.AttachmentsField;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.util.AbstractForm;
import com.niafikra.dimension.core.ui.vaadin.util.MoneyField;
import com.niafikra.dimension.plan.domain.Budget;
import com.niafikra.dimension.plan.domain.CostCenter;
import com.niafikra.dimension.plan.service.BudgetService;
import com.niafikra.dimension.plan.service.CostCenterService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.*;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.layouts.MFormLayout;
import org.vaadin.viritin.layouts.MHorizontalLayout;

import javax.annotation.PostConstruct;
import java.util.Set;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/13/17 1:20 PM
 */
@SpringComponent
@ViewScope
public class BudgetCreateForm extends AbstractForm<Budget> {

    public static final String FIELD_WIDTH = "400px";
    private MTextField title = new MTextField("Title");
    private MoneyField fund = new MoneyField("Allocated Funds");
    private MoneyField cost = new MoneyField("Associated Cost(eg. PO Value)");
    private TextArea description = new TextArea("Description");
    private DateField startDate = new DateField();
    private DateField endDate = new DateField();
    private ComboBox<CostCenter> costCenter = new ComboBox<>("Cost center");
    private CheckBox allowExpensesWithLessBalance = new CheckBox("Can allow expenses even with less balance");
    private CheckBox allowRequestWithLessBalance = new CheckBox("Can allow requests even with less balance");
    private CheckBox allowRequestWithLessGrossBalance = new CheckBox("Can allow request even with less gross balance");
    private CheckBox allowRequestWithSimilarPending = new CheckBox("Can allow new request while similar is pending approval");
    private AttachmentsField<Set<Attachment>> attachments;
    private BudgetService budgetService;
    private CostCenterService costCenterService;
    private User currentUser;

    public BudgetCreateForm(AttachmentsField<Set<Attachment>> attachments, BudgetService budgetService, CostCenterService costCenterService, User currentUser) {
        super(Budget.class);
        this.attachments = attachments;

        this.budgetService = budgetService;
        this.costCenterService = costCenterService;
        this.currentUser = currentUser;
    }

    @PostConstruct
    private void build() {
        title.setWidth(FIELD_WIDTH);
        fund.setWidth(FIELD_WIDTH);
        cost.setWidth(FIELD_WIDTH);
        startDate.setWidth("100%");
        endDate.setWidth("100%");
        attachments.setWidth(FIELD_WIDTH);
        attachments.setCaption("Attachments");
        costCenter.setItems(costCenterService.getPlannerCostCenters(currentUser));
        costCenter.setWidth(FIELD_WIDTH);
        description.setWidth(FIELD_WIDTH);

        addSavedHandler(budget -> doSave(budget));
        setResetHandler(budget -> doCancel());
    }

    private void doCancel() {
        setEntity(new Budget());
    }

    private void doSave(Budget budget) {
        try {
            budgetService.create(budget);
            showSuccess("Successful created budget", budget.toString());
            closePopup();
        } catch (Exception e) {
            showError("Failed to create budget", e);
        }
    }


    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setWidth("700px");
        return window;
    }

    @Override
    protected Component createContent() {
        HorizontalLayout toolbar = getToolbar();

        return new MFormLayout(
                title,
                new MHorizontalLayout(startDate, endDate).withWidth(FIELD_WIDTH).withCaption("Period"),
                costCenter,
                cost,
                fund,
                description,
                attachments,
                allowExpensesWithLessBalance,
                allowRequestWithLessBalance,
                allowRequestWithLessGrossBalance,
                allowRequestWithSimilarPending,
                toolbar)
                .withMargin(true);
    }
}
