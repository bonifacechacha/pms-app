package com.niafikra.dimension.plan.ui.vaadin.paymentMethod;

import com.niafikra.dimension.core.ui.vaadin.util.NameDescriptionForm;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.plan.domain.PaymentMethod;
import com.niafikra.dimension.plan.service.PaymentMethodService;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/5/17 3:25 PM
 */
@SpringComponent
@PrototypeScope
public class PaymentMethodForm extends NameDescriptionForm<PaymentMethod> implements Notifier {

    private PaymentMethodService paymentMethodService;

    public PaymentMethodForm(PaymentMethodService paymentMethodService) {
        super(PaymentMethod.class);
        this.paymentMethodService = paymentMethodService;

        addSavedHandler(paymentMethod -> doSave(paymentMethod));
    }

    protected void doSave(PaymentMethod paymentMethod) {
        try {
            paymentMethod = paymentMethodService.save(paymentMethod);
            setEntity(paymentMethod);

            String msg = "Saved payment method successful";
            showSuccess(msg, paymentMethod.toString());
        } catch (Exception e) {
            String msg = "Failed to save payment method";
            showError(msg, e);
        }
    }

}
