package com.niafikra.dimension.plan.ui.vaadin.budget;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.ui.vaadin.view.AbstractMainContentDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.DisplayComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.niafikra.dimension.plan.domain.AllocationAdjustment;
import com.niafikra.dimension.plan.domain.Budget;
import com.niafikra.dimension.plan.domain.PeriodAdjustment;
import com.niafikra.dimension.plan.service.BudgetService;
import com.niafikra.dimension.plan.ui.vaadin.budget.execution.allocationAdjustment.AllocationAdjustmentForm;
import com.niafikra.dimension.plan.ui.vaadin.budget.execution.allocationAdjustment.AllocationAdjustmentView;
import com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense.BudgetExpensesView;
import com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense.ExpenseCreateForm;
import com.niafikra.dimension.plan.ui.vaadin.budget.execution.periodAdjustment.PeriodAdjustmentForm;
import com.niafikra.dimension.plan.ui.vaadin.budget.execution.periodAdjustment.PeriodAdjustmentsView;
import com.niafikra.dimension.plan.ui.vaadin.budget.planning.AllocationsPlanView;
import com.niafikra.dimension.plan.ui.vaadin.budget.planning.CreateAllocationForm;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/30/17 3:21 PM
 */
@DisplayComponent
public class BudgetDisplay extends AbstractMainContentDisplay {

    @Inject
    PeriodAdjustmentForm periodAdjustmentForm;
    @Inject
    private BudgetService budgetService;
    private MVerticalLayout viewComponentHolder;
    @Inject
    private BudgetEditForm editForm;
    @Inject
    private ExpenseCreateForm expenseForm;
    @Inject
    private CreateAllocationForm createAllocationForm;
    @Inject
    private AllocationAdjustmentForm allocationAdjustmentForm;
    private Budget budget;


    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        viewComponentHolder = new MVerticalLayout().withFullSize().withMargin(false);
        with(viewComponentHolder).withExpandRatio(viewComponentHolder, 1);
    }

    @Secured(Permission.RECORD_EXPENSE)
    @ViewMenuOption(value = "New Expense", icon = VaadinIcons.PLUS_CIRCLE)
    public void showExpenseCreateForm() {
        expenseForm.setBudget(budget);
        Window window = expenseForm.openInModalPopup();
        window.addCloseListener(e -> {
            UI.getCurrent().access(() -> navigator.reload());
        });
    }


    @Secured(Permission.ALLOCATE_BUDGET_RESOURCES)
    @ViewMenuOption(value = "Multiple Entries", menu = "Propose", icon = VaadinIcons.FOLDER_ADD, menuIcon = VaadinIcons.MONEY)
    public void doCreateMultiAllocation() {
        navigator.navigateTo(AllocationsPlanView.class, budget.getId());
    }

    @Secured(Permission.ALLOCATE_BUDGET_RESOURCES)
    @ViewMenuOption(value = "Single Entries", menu = "Propose", icon = VaadinIcons.FILE_ADD, menuIcon = VaadinIcons.MONEY)
    public void doCreateSingleAllocation() {
        this.createAllocationForm.setBudget(budget);
        Window subWindow = this.createAllocationForm.openInModalPopup();
        subWindow.setCaption("Allocate resource");
        subWindow.addCloseListener(e -> UI.getCurrent().access(() -> navigator.reload()));
    }


    @Secured(Permission.EDIT_BUDGET)
    @ViewMenuOption(value = "Edit", menu = "Budget", icon = VaadinIcons.EDIT, menuIcon = VaadinIcons.BOOK_DOLLAR)
    public void doEditBudget() {
        editForm.setEntity(budget);
        editForm.openInModalPopup().addCloseListener(e -> UI.getCurrent().access(() -> navigator.reload()));
    }

    @Secured(Permission.SUBMIT_BUDGET)
    @ViewMenuOption(value = "Submit", menu = "Budget", icon = VaadinIcons.KEY, menuIcon = VaadinIcons.BOOK_DOLLAR)
    public void doSubmit() {
        ConfirmDialog.show(
                getUI(),
                "Are you sure?",
                dialog -> {
                    if (dialog.isConfirmed()) {
                        try {
                            budgetService.submitApproval(budget);
                            navigator.navigateTo(navigator.getState());
                            showSuccess("Budget submitted for approval successful", budget.toString());
                        } catch (Exception e) {
                            showError("Failed to submit budget for approval", e.getMessage());
                        }
                    }
                }
        );
    }


    @Secured(Permission.DELETE_BUDGET)
    @ViewMenuOption(value = "Delete", menu = "Budget", icon = VaadinIcons.TRASH, separator = true, menuIcon = VaadinIcons.BOOK_DOLLAR)
    public void doDelete() {
        ConfirmDialog.show(
                getUI(),
                "Are you sure you want to delete this budget: \n" + budget,
                dialog -> {
                    if (dialog.isConfirmed()) {
                        try {
                            budgetService.delete(budget);
                            navigator.navigateTo(UserBudgetsView.class);
                            showSuccess("Budget deleted successful", budget.toString());
                        } catch (Exception e) {
                            showError("Failed to delete budget", e.getMessage());
                        }
                    }
                });
    }

    @Secured(Permission.VIEW_BUDGET_ALLOCATIONS)
    @ViewMenuOption(value = "Allocations", menu = "Budget", icon = VaadinIcons.COINS, menuIcon = VaadinIcons.BOOK_DOLLAR)
    public void showAllocations() {
        navigator.navigateTo(BudgetAllocationsView.class, budget.getId());
    }

    @Secured(Permission.VIEW_BUDGET_EXPENSES)
    @ViewMenuOption(value = "Expenses", menu = "Budget", icon = VaadinIcons.MONEY, menuIcon = VaadinIcons.BOOK_DOLLAR)
    public void showExpenses() {
        navigator.navigateTo(BudgetExpensesView.class, budget.getId());
    }

    @Secured(Permission.VIEW_BUDGET_REQUISITIONS)
    @ViewMenuOption(value = "Requisitions", menu = "Budget", icon = VaadinIcons.ACCESSIBILITY, menuIcon = VaadinIcons.BOOK_DOLLAR)
    public void showRequisitions() {

    }

    @Secured(Permission.ADJUST_ALLOCATION)
    @ViewMenuOption(value = "View Allocation Adjustment", menu = "Budget", icon = VaadinIcons.ADJUST, menuIcon = VaadinIcons.BOOK_DOLLAR)
    public void showAllocationAdjustments() {
        navigator.navigateTo(AllocationAdjustmentView.class, budget.getId());

    }

    @Secured(Permission.ADJUST_ALLOCATION)
    @ViewMenuOption(value = "Add Allocation Adjustment", menu = "Budget", icon = VaadinIcons.PLUS_CIRCLE, menuIcon = VaadinIcons.BOOK_DOLLAR)
    public void addAllocationAdjustment() {
        allocationAdjustmentForm.setEntity(new AllocationAdjustment(budget));
        this.allocationAdjustmentForm.setBudget(budget);
        allocationAdjustmentForm
                .openInModalPopup();

    }

    @Secured(Permission.ADJUST_BUDGET_PERIOD)
    @ViewMenuOption(value = "View Period Adjustment", menu = "Budget", icon = VaadinIcons.CALENDAR_ENVELOPE, menuIcon = VaadinIcons.BOOK_DOLLAR)
    public void showPeriodAdjustments() {
        navigator.navigateTo(PeriodAdjustmentsView.class, budget.getId());
    }

    @Secured(Permission.ADJUST_BUDGET_PERIOD)
    @ViewMenuOption(value = "Add Period Adjustment", menu = "Budget", icon = VaadinIcons.PLUS, menuIcon = VaadinIcons.BOOK_DOLLAR)
    public void addPeriodAdjustments() {
        periodAdjustmentForm.setEntity(new PeriodAdjustment(budget));
        periodAdjustmentForm
                .openInModalPopup();
    }

    @Secured(Permission.TRANSFER_ALLOCATION)
    @ViewMenuOption(value = "Allocation Transfer", menu = "Budget", icon = VaadinIcons.EXCHANGE, menuIcon = VaadinIcons.BOOK_DOLLAR)
    public void showAllocationTransfer() {
    }


    protected void setBudget(Budget budget) {
        this.budget = budget;

        setMenuVisible("Lock", () -> !budget.isArchived());
        setMenuVisible("UnLock", () -> budget.isArchived());

        setMenuVisible("Delete", () -> budgetService.canDelete(budget));
        setMenuVisible("Submit", () -> budgetService.canSubmit(budget));
        setMenuVisible("Propose", () -> budget.isWaiting());
        setMenuVisible("New Expense", () -> budget.isApproved());

        setMenuVisible("Expenses", () -> budget.isApproved());
        setMenuVisible("Requisitions", () -> budget.isApproved());
        setMenuVisible("Allocation Adjustment", () -> budget.isApproved());
        setMenuVisible("Period Adjustment", () -> budget.isApproved());
        setMenuVisible("Allocation Transfer", () -> budget.isApproved());
    }

    @Override
    public void afterViewChange(ViewChangeEvent event) {
        Long budgetId = Long.parseLong(event.getParameters());
        Budget budget = budgetService.getBudget(budgetId);
        setBudget(budget);
    }

    @Override
    public void show(View view) {
        viewComponentHolder.removeAllComponents();
        Component viewComponent = view.getViewComponent();
        viewComponentHolder.addComponent(viewComponent);
        viewComponentHolder.setExpandRatio(viewComponent, 1);
    }

}
