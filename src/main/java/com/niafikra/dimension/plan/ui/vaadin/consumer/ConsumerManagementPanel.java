package com.niafikra.dimension.plan.ui.vaadin.consumer;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.category.ui.vaadin.CategorisedItemsManagementPanel;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingDisplay;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingView;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.niafikra.dimension.plan.domain.Consumer;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;


/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/8/17 12:36 AM
 */

@Secured({Permission.CONFIGURE_CONSUMERS})
@ViewComponent(value = SettingDisplay.class, caption = "Consumers")
@ViewInfo(value = "Consumers", section = "Planning", icon = VaadinIcons.TRUCK)
@SpringView(name = ConsumerManagementPanel.VIEW_NAME, ui = MainUI.class)
public class ConsumerManagementPanel extends CategorisedItemsManagementPanel<Consumer> implements SettingView, TableExportable {
    public static final String VIEW_NAME = "consumers";

    private ConsumerCategoryCRUDPanel categoryCRUDPanel;
    private ConsumerCreateViewPanel consumerCreateViewPanel;

    public ConsumerManagementPanel(ConsumerCategoryCRUDPanel categoryCRUDPanel, ConsumerCreateViewPanel consumerCreateViewPanel) {
        super(categoryCRUDPanel, consumerCreateViewPanel);
        this.categoryCRUDPanel = categoryCRUDPanel;
        this.consumerCreateViewPanel = consumerCreateViewPanel;
    }

    @Override
    public TableExport getTableExport() {
        return consumerCreateViewPanel.getTableExport();
    }
}
