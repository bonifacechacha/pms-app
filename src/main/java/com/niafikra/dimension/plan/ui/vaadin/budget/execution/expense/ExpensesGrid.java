package com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense;

import com.niafikra.dimension.core.security.service.UserService;
import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.user.UsersProvider;
import com.niafikra.dimension.core.ui.vaadin.util.date.LocalDateTimeRangeSelector;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.util.DateUtils;
import com.niafikra.dimension.plan.domain.Expense;
import com.niafikra.dimension.plan.domain.Requisition;
import com.niafikra.dimension.plan.service.ExpenseService;
import com.niafikra.dimension.plan.service.RequisitionService;
import com.niafikra.dimension.plan.service.RetirementService;
import com.niafikra.dimension.plan.ui.vaadin.budget.BudgetsProvider;
import com.niafikra.dimension.plan.ui.vaadin.consumer.ConsumerProvider;
import com.niafikra.dimension.plan.ui.vaadin.paymentMethod.PaymentMethodProvider;
import com.niafikra.dimension.plan.ui.vaadin.resource.ResourceComboBox;
import com.niafikra.dimension.plan.ui.vaadin.role.RoleProvider;
import com.vaadin.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.HeaderRow;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Optional;

import static com.niafikra.dimension.core.ui.vaadin.Theme.TEXT_RIGHT;
import static com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils.createFilteringComboBox;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 10/1/17 2:16 PM
 */
@ViewScope
@SpringComponent
public class ExpensesGrid extends Grid<Expense> {

    @Inject
    private RetirementService retirementService;
    @Inject
    private RequisitionService requisitionService;
    @Inject
    private Navigator navigator;
    @Inject
    private UsersProvider usersProvider;
    @Inject
    private RoleProvider roleProvider;
    @Inject
    private ResourceComboBox resourceFilter;
    @Inject
    private BudgetsProvider budgetsProvider;
    @Inject
    private ConsumerProvider consumerProvider;
    @Inject
    private PaymentMethodProvider paymentMethodProvider;

    private ExpenseService.ExpenseFilter expenseFilter = ExpenseService.ExpenseFilter.builder().build();

    public ExpensesGrid(ExpensesProvider expensesProvider) {
        super(expensesProvider.withConfigurableFilter());
    }

    @PostConstruct
    private void build() {

        setSelectionMode(SelectionMode.NONE);

        addColumn(expense -> expense.getPayment().getReference())
                .setId("reference")
                .setCaption("Reference")
                .setSortable(true);

        addColumn(expense -> DateUtils.formatDateTime(expense.getPaymentTime()))
                .setId("payment.time")
                .setCaption("Time")
                .setHidable(true)
                .setSortable(true);

        addColumn(Expense::getAssociatedUser)
                .setId("associatedUser")
                .setCaption("Person")
                .setHidable(true)
                .setSortable(true);

        addColumn(expense -> expense.getPayment().getAmount().toBaseCurrency().getFormattedAmount())
                .setId("payment.amount")
                .setCaption("Amount")
                .setSortable(true)
                .setStyleGenerator(item -> TEXT_RIGHT);


        addColumn(expense -> expense.getTotalRetirement().toBaseCurrency().getFormattedAmount())
                .setId("retiredAmount")
                .setCaption("Retired Amount")
                .setSortable(false)
                .setStyleGenerator(item -> TEXT_RIGHT);

        addColumn(expense -> retirementService.findRetiredAmountPendingApproval(expense))
                .setId("pendingRetirement")
                .setCaption("Pending Retirement")
                .setSortable(false)
                .setStyleGenerator(item -> TEXT_RIGHT);

        addColumn(expense -> expense.getTotalSettlement().toBaseCurrency().getFormattedAmount())
                .setId("totalSettlement")
                .setCaption("Settled")
                .setSortable(false)
                .setStyleGenerator(item -> TEXT_RIGHT);

        addColumn(expense -> expense.getRetiredDifference().toBaseCurrency().getFormattedAmount())
                .setId("retiredDifference")
                .setCaption("Difference")
                .setSortable(false)
                .setStyleGenerator(item -> TEXT_RIGHT);

        addColumn(expense -> expense.getPayment().getMethod())
                .setId("payment.method")
                .setCaption("Payment")
                .setHidable(true)
                .setSortable(true);


        addColumn(expense -> expense.isReconciled() ? "YES" : "NO")
                .setId("reconciled")
                .setCaption("Reconciled")
                .setHidable(true)
                .setSortable(true)
                .setStyleGenerator(expense -> generateReconciliationStyle(expense));

        addColumn(Expense::getRole)
                .setId("role")
                .setCaption("Role")
                .setSortable(true);

        addColumn(Expense::getResource)
                .setId("resource")
                .setCaption("Resource")
                .setHidable(true)
                .setSortable(true);


        addColumn(Expense::getConsumer)
                .setId("consumer")
                .setCaption("Consumer")
                .setHidable(true)
                .setSortable(true);

        addColumn(Expense::getBudget)
                .setId("budget")
                .setCaption("Budget")
                .setHidable(true)
                .setSortable(true);

        addColumn(expense -> expense.getPayment().getAssociate())
                .setId("payee")
                .setCaption("Payeee")
                .setSortable(true);


        addColumn(expense -> expense.getId())
                .setId("id")
                .setCaption("ID")
                .setSortable(true);

        addColumn(expense -> {
            Optional<Requisition> requisition = requisitionService.findRequisition(expense);
            if (requisition.isPresent()) return requisition.get().getDescription();
            else return expense.getNotes();
        }).setId("description").setHidable(true).setSortable(false);

        addItemClickListener(event -> {
            Expense expense = event.getItem();
            navigator.navigateTo(ExpenseView.class, expense.getId());
        });

        getDataProvider().setFilter(expenseFilter);

        HeaderRow headerRow = appendHeaderRow();
        LocalDateTimeRangeSelector timeFilter = new LocalDateTimeRangeSelector();
        timeFilter.setWidth("200px");
        headerRow.getCell("payment.time").setComponent(timeFilter);
        timeFilter.addValueChangeListener(event -> {
            expenseFilter.setStartTime(timeFilter.getStart());
            expenseFilter.setEndTime(timeFilter.getEnd());
            getDataProvider().refreshAll();
        });


        createFilteringComboBox(
                headerRow,
                "associatedUser",
                "Filter associated user",
                usersProvider.withConvertedFilter(name -> UserService.UserFilter.builder().name(name).build()),
                event -> {
                    expenseFilter.setAssociatedUser(event.getValue());
                    getDataProvider().refreshAll();
                }
        );

        createFilteringComboBox(
                headerRow,
                "role",
                "Filter role",
                roleProvider,
                event -> {
                    expenseFilter.setRole(event.getValue());
                    getDataProvider().refreshAll();
                }
        );

        //TODO REMOVE THE DEPRECATED LOGIC FOR CREATING COMBO BOX USING VAADIN UTILS CLASS AND IMPLEMENT LIKE I DID IN RESOURCE FILTER
        resourceFilter.setPlaceholder("Filter resources");
        headerRow.getCell("resource").setComponent(
                resourceFilter
                        .withStyleName(Theme.COMBOBOX_TINY)
                        .withValueChangeListener(event -> {
                            expenseFilter.setResource(event.getValue());
                            getDataProvider().refreshAll();
                        })
        );

        //TODO REMOVE THE DEPRECATED LOGIC FOR CREATING COMBO BOX USING VAADIN UTILS CLASS AND IMPLEMENT LIKE I DID IN RESOURCE FILTER
        createFilteringComboBox(
                headerRow,
                "consumer",
                "Filter consumer",
                consumerProvider,
                event -> {
                    expenseFilter.setConsumer(event.getValue());
                    getDataProvider().refreshAll();
                }
        );

        createFilteringComboBox(
                headerRow,
                "budget",
                "Filter budget",
                budgetsProvider,
                event -> {
                    expenseFilter.setBudget(event.getValue());
                    getDataProvider().refreshAll();
                }
        );

        createFilteringComboBox(
                headerRow,
                "payment.method",
                "Filter payment method",
                paymentMethodProvider,
                event -> {
                    expenseFilter.setPaymentMethod(event.getValue());
                    getDataProvider().refreshAll();
                }
        );
    }

    private String generateReconciliationStyle(Expense expense) {
        if (expense.isReconciled()) return Theme.TEXT_SUCCESS;

        if (retirementService.findPendingApprovalRetirement(expense).isPresent()) return Theme.TEXT_WARNING;
        else return Theme.TEXT_DANGER;
    }

    @Override
    public ConfigurableFilterDataProvider<Expense, Void, ExpenseService.ExpenseFilter> getDataProvider() {
        return (ConfigurableFilterDataProvider<Expense, Void, ExpenseService.ExpenseFilter>) super.getDataProvider();
    }

    public ExpenseService.ExpenseFilter getExpenseFilter() {
        return expenseFilter;
    }
}
