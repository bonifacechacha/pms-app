package com.niafikra.dimension.plan.ui.vaadin.budget.execution.allocationAdjustment;

import com.niafikra.dimension.plan.domain.AllocationAdjustment;
import com.niafikra.dimension.plan.domain.Budget;
import com.niafikra.dimension.plan.service.AllocationAdjustmentService;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.spring.annotation.SpringComponent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;
import org.vaadin.spring.annotation.PrototypeScope;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Juma mketto
 * @Date 1/2/19.
 */
@PrototypeScope
@SpringComponent
public class AllocationAdjustmentProvider extends PageableDataProvider<AllocationAdjustment, String> {
    private AllocationAdjustmentService service;
    private Budget budget;

    public AllocationAdjustmentProvider(AllocationAdjustmentService service) {
        this.service = service;
    }


    @Override
    protected Page<AllocationAdjustment> fetchFromBackEnd(Query<AllocationAdjustment, String> query, Pageable pageable) {
        return service.findAll(budget, pageable);
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        List<QuerySortOrder> sortOrders = new ArrayList<>();
        sortOrders.add(new QuerySortOrder("timeCreated", SortDirection.DESCENDING));
        return sortOrders;
    }

    @Override
    protected int sizeInBackEnd(Query<AllocationAdjustment, String> query) {
        return service.countAll(budget).intValue();
    }

    public void filterBudget(Budget budget) {
        this.budget = budget;
        refreshAll();
    }
}
