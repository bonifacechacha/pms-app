package com.niafikra.dimension.plan.ui.vaadin.budget.execution;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.attachment.ui.vaadin.util.AttachmentsField;
import com.niafikra.dimension.core.ui.vaadin.util.AbstractForm;
import com.niafikra.dimension.core.ui.vaadin.util.MoneyField;
import com.niafikra.dimension.plan.domain.Payment;
import com.niafikra.dimension.plan.domain.PaymentMethod;
import com.niafikra.dimension.plan.service.PaymentMethodService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.*;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Set;

@PrototypeScope
@SpringComponent
public class PaymentForm extends AbstractForm<Payment> {

    private DateTimeField time = new DateTimeField("Time");
    private TextField associate = new TextField();
    private ComboBox<PaymentMethod> method = new ComboBox<>("Method");
    private MoneyField amount = new MoneyField("Amount");
    private TextField reference = new TextField("Reference");
    private TextArea noteField = new TextArea("Notes");

    @Inject
    private AttachmentsField<Set<Attachment>> attachments;
    @Inject
    private PaymentMethodService methodService;

    public PaymentForm() {
        super(Payment.class);
    }

    @PostConstruct
    private void build() {
        time.setWidth("50%");
        method.setWidth("100%");
        method.setItems(methodService.getAll());
        amount.setWidth("100%");
        associate.setWidth("100%");
        reference.setWidth("100%");
        noteField.setWidth("100%");
        noteField.setRows(7);
        attachments.setCaption("Attachments");
        attachments.setWidth("100%");

        getSaveButton().setWidth("200px");
        getSaveButton().setIcon(VaadinIcons.MONEY);
    }

    @Override
    protected void bind() {
        super.bind();

        getBinder()
                .forField(noteField)
                .bind(
                        payment -> payment.getNote(Payment.ON_PAYMENT_EXTRA_NOTE),
                        (payment, note) -> payment.putNote(Payment.ON_PAYMENT_EXTRA_NOTE, note)
                );
    }


    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setWidth("650px");
        window.setCaption(getEntity().isPaid() ? "Recorde payment" : "Record receipt");
        return window;
    }

    @Override
    protected Component createContent() {
        MHorizontalLayout toolbar = getToolbar();
        return new MVerticalLayout(
                time,
                new MHorizontalLayout(amount, method).withFullWidth(),
                new MHorizontalLayout(associate, reference).withFullWidth(),
                new MHorizontalLayout(noteField, attachments).withFullWidth(),
                toolbar
        ).withAlign(toolbar, Alignment.MIDDLE_CENTER).withMargin(true);
    }

    @Override
    public void setEntity(Payment payment) {
        super.setEntity(payment);
        associate.setCaption(payment.isPaid() ? "Payee" : "Payer");
        getSaveButton().setCaption(payment.isPaid() ? "Pay" : "Receive");
    }
}
