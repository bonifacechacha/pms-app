package com.niafikra.dimension.plan.ui.vaadin.budget.execution.requisition;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.plan.domain.*;
import com.niafikra.dimension.plan.service.RequisitionService;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.spring.annotation.SpringComponent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 10/1/17 2:18 PM
 */
@PrototypeScope
@SpringComponent
public class RequisitionsProvider extends PageableDataProvider<Requisition, String> {
    @Inject
    private RequisitionService requisitionService;

    private LocalDateTime startDate, endDate;
    private Budget budget;
    private Role role;
    private Resource resource;
    private User creator;
    private Consumer consumer;
    private String description;
    private Boolean fullFilled;
    private Boolean approved;

    @Override
    protected Page<Requisition> fetchFromBackEnd(Query<Requisition, String> query, Pageable pageable) {
        return requisitionService.getRequisitions(
                null,
                budget,
                role,
                resource,
                startDate,
                endDate,
                creator,
                consumer,
                description,
                approved,
                fullFilled,
                pageable
        );
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        return List.of(new QuerySortOrder("timeCreated", SortDirection.DESCENDING));
    }

    @Override
    protected int sizeInBackEnd(Query<Requisition, String> query) {
        Long count = requisitionService.countRequisitions(
                null,
                budget,
                role,
                resource,
                startDate,
                endDate,
                creator,
                consumer,
                description,
                approved,
                fullFilled
        );

        return count.intValue();
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
        refreshAll();
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
        refreshAll();
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
        refreshAll();
    }


    public void setRole(Role role) {
        this.role = role;
        refreshAll();
    }

    public void setResource(Resource resource) {
        this.resource = resource;
        refreshAll();
    }

    public void setCreator(User creator) {
        this.creator = creator;
        refreshAll();
    }

    public void setConsumer(Consumer consumer) {
        this.consumer = consumer;
        refreshAll();
    }

    public void setDescription(String description) {
        this.description = description;
        refreshAll();
    }

    public void setFullFilled(Boolean fullFilled) {
        this.fullFilled = fullFilled;
        refreshAll();
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
        refreshAll();
    }

    public void setDateRange(LocalDateTime startDateTime, LocalDateTime endDateTime) {
        this.startDate = startDateTime;
        this.endDate = endDateTime;
        refreshAll();
    }
}
