package com.niafikra.dimension.plan.ui.vaadin.budget.execution.requisition;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.security.SecurityUtils;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.plan.domain.Requisition;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.Permission.VIEW_PENDING_PAYMENT_REQUISITIONS;
import static com.niafikra.dimension.plan.ui.vaadin.budget.execution.requisition.PendingPaymentRequisitionsView.VIEW_NAME;

@Secured(VIEW_PENDING_PAYMENT_REQUISITIONS)
@ViewComponent(value = MainDisplay.class, caption = "Pending payments")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.FINANCE, caption = "Pending Payments")
@VaadinFontIcon(VaadinIcons.MONEY_WITHDRAW)
public class PendingPaymentRequisitionsView extends VerticalLayout implements View, TableExportable {
    public static final String VIEW_NAME = "payment-requisitions";

    @Inject
    private RequisitionsGrid requisitionsGrid;
    @Inject
    private RequisitionPaymentForm paymentForm;
    @Inject
    private Navigator navigator;

    @PostConstruct
    private void build() {

        setSizeFull();
        setMargin(false);
        requisitionsGrid.setSizeFull();
        requisitionsGrid.filterFullfilled(false);
        requisitionsGrid.filterApproved(true);
        addComponent(requisitionsGrid);
        setExpandRatio(requisitionsGrid, 1);

        if (SecurityUtils.hasAuthority(Permission.PAY_REQUISITION)) {
            requisitionsGrid.addContextClickListener(event -> {
                Grid.GridContextClickEvent<Requisition> contextEvent = (Grid.GridContextClickEvent<Requisition>) event;
                showPaymentWindow(contextEvent.getItem());
            });
        }

    }

    private void showPaymentWindow(Requisition requisition) {
        paymentForm.setRequisition(requisition);
        paymentForm.openInModalPopup().addCloseListener(e -> UI.getCurrent().access(() -> navigator.reload()));
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(requisitionsGrid);
    }
}
