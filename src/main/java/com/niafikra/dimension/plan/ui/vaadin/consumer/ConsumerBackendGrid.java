package com.niafikra.dimension.plan.ui.vaadin.consumer;

import com.niafikra.dimension.category.ui.vaadin.CategorisedBackendGrid;
import com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils;
import com.niafikra.dimension.plan.domain.Consumer;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/6/17 9:38 PM
 */
@SpringComponent
@PrototypeScope
public class ConsumerBackendGrid extends CategorisedBackendGrid<Consumer> {

    public ConsumerBackendGrid(ConsumerProvider dataProvider) {
        super(dataProvider);
        addColumn(t -> t.getCode())
                .setId("code")
                .setCaption("Code")
                .setWidth(200)
                .setSortable(true);

        addColumn(t -> t.isActive()?"Yes":"No")
                .setId("active")
                .setCaption("Active")
                .setSortable(true)
                .setWidth(100);
        setColumnOrder("code", "name", "category", "active");

        VaadinUtils.createFilteringTextField(
                headerRow,
                "code",
                "Filter by code",
                event -> dataProvider.setCodeFilter(event.getValue())
        );
    }

    @Override
    public ConsumerProvider getDataProvider() {
        return (ConsumerProvider) super.getDataProvider();
    }
}
