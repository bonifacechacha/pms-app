package com.niafikra.dimension.plan.ui.vaadin.resource;

import com.niafikra.dimension.plan.domain.Resource;
import com.niafikra.dimension.plan.service.ResourceService;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.spring.annotation.SpringComponent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/30/17 10:35 AM
 */
@SpringComponent
@PrototypeScope
public class ResourceProvider extends PageableDataProvider<Resource, ResourceService.ResourceFilter> {

    @Inject
    private ResourceService resourceService;

    @Override
    protected Page<Resource> fetchFromBackEnd(Query<Resource, ResourceService.ResourceFilter> query, Pageable pageable) {
        return resourceService.findResources(query.getFilter(), pageable);
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        List<QuerySortOrder> sortOrders = new ArrayList<>();
        sortOrders.add(new QuerySortOrder("name", SortDirection.ASCENDING));
        return sortOrders;
    }

    @Override
    protected int sizeInBackEnd(Query<Resource, ResourceService.ResourceFilter> query) {
        return resourceService.countResources(query.getFilter()).intValue();
    }
}