package com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense;

import com.niafikra.dimension.plan.domain.Expense;
import com.niafikra.dimension.plan.service.ExpenseService;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.spring.annotation.SpringComponent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;
import org.vaadin.spring.annotation.PrototypeScope;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 10/1/17 2:18 PM
 */
@PrototypeScope
@SpringComponent
public class ExpensesProvider extends PageableDataProvider<Expense, ExpenseService.ExpenseFilter> {

    private ExpenseService expenseService;

//    private LocalDateTime startDate, endDate;
//    private CostCenter costCenter;
//    private Budget budget;
//    private Role role;
//    private Resource resource;
//    private User associatedUser;
//    private User creator;
//    private Consumer consumer;
//    private PaymentMethod paymentMethod;
//    private String reference, description, associate;
//    private Boolean retired;
//    private Boolean settled;
//    private Boolean reconciled;

    public ExpensesProvider(ExpenseService expenseService) {
        this.expenseService = expenseService;
    }

    @Override
    protected Page<Expense> fetchFromBackEnd(Query<Expense, ExpenseService.ExpenseFilter> query, Pageable pageable) {
        return expenseService.getExpenses(query.getFilter(), pageable);
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        List<QuerySortOrder> sortOrders = new ArrayList<>();
        sortOrders.add(new QuerySortOrder("payment.time", SortDirection.DESCENDING));
        return sortOrders;
    }

    @Override
    protected int sizeInBackEnd(Query<Expense, ExpenseService.ExpenseFilter> query) {
        Long count = expenseService.countExpenses(query.getFilter());

        return count.intValue();
    }
//
//    public void setRetired(Boolean retired) {
//        this.retired = retired;
//        refreshAll();
//    }
//
//    public void setSettled(Boolean settled) {
//        this.settled = settled;
//        refreshAll();
//    }
//
//    public void setStartDate(LocalDateTime startDate) {
//        this.startDate = startDate;
//        refreshAll();
//    }
//
//    public void setEndDate(LocalDateTime endDate) {
//        this.endDate = endDate;
//        refreshAll();
//    }
//
//    public void setCostCenter(CostCenter costCenter) {
//        this.costCenter = costCenter;
//        refreshAll();
//    }
//
//    public void setBudget(Budget budget) {
//        this.budget = budget;
//        refreshAll();
//    }
//
//    public void setRole(Role role) {
//        this.role = role;
//        refreshAll();
//    }
//
//    public void setResource(Resource resource) {
//        this.resource = resource;
//        refreshAll();
//    }
//
//    public void setAssociatedUser(User associatedUser) {
//        this.associatedUser = associatedUser;
//        refreshAll();
//    }
//
//    public void setCreator(User creator) {
//        this.creator = creator;
//        refreshAll();
//    }
//
//    public void setConsumer(Consumer consumer) {
//        this.consumer = consumer;
//        refreshAll();
//    }
//
//    public void setPaymentMethod(PaymentMethod paymentMethod) {
//        this.paymentMethod = paymentMethod;
//        refreshAll();
//    }
//
//    public void setReference(String reference) {
//        this.reference = reference;
//        refreshAll();
//    }
//
//    public void setDescription(String description) {
//        this.description = description;
//        refreshAll();
//    }
//
//    public void setAssociate(String associate) {
//        this.associate = associate;
//        refreshAll();
//    }
//
//    public void setReconciled(Boolean reconciled) {
//        this.reconciled = reconciled;
//        refreshAll();
//    }
}
