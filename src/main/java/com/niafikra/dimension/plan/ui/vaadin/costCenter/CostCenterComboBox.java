package com.niafikra.dimension.plan.ui.vaadin.costCenter;

import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.plan.domain.CostCenter;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;


@SpringComponent
@PrototypeScope
public class CostCenterComboBox extends MComboBox<CostCenter> {
    public CostCenterComboBox(CostCenterProvider provider) {
        setDataProvider(provider);
    }
}
