package com.niafikra.dimension.inventory.ui.vaadin.transaction;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.security.service.UserService;
import com.niafikra.dimension.core.ui.vaadin.user.UsersProvider;
import com.niafikra.dimension.core.ui.vaadin.util.date.LocalDateTimeRangeSelector;
import com.niafikra.dimension.core.util.DateUtils;
import com.niafikra.dimension.inventory.domain.transaction.Transaction;
import com.niafikra.dimension.inventory.service.StoreService;
import com.niafikra.dimension.inventory.ui.vaadin.item.ItemsProvider;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.HeaderRow;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils.createFilteringComboBox;
import static com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils.createFilteringTextField;

@SpringComponent
@ViewScope
public class TransactionsGrid extends Grid<Transaction> {

    @Inject
    private UsersProvider usersProvider;

    @Inject
    private ItemsProvider itemsProvider;

    @Inject
    private StoreService storeService;

    @Inject
    private User currentUser;

    public TransactionsGrid(TransactionsProvider transactionsProvider) {
        super(transactionsProvider);
    }

    public TransactionsProvider getDataProvider() {
        return (TransactionsProvider) super.getDataProvider();
    }


    @PostConstruct
    private void build() {

        addColumn(transaction -> DateUtils.formatDateTime(transaction.getTime()))
                .setId("time")
                .setCaption("Time")
                .setSortable(true);

        addColumn(transaction -> transaction.getClass().getSimpleName())
                .setId("type")
                .setCaption("Type")
                .setHidable(true)
                .setSortable(false);

        addColumn(transaction -> transaction.getBatch().getItem())
                .setId("batch.item")
                .setCaption("Item")
                .setHidable(true)
                .setSortable(true);

        addColumn(transaction -> transaction.getStore())
                .setId("batch.store")
                .setCaption("Store")
                .setHidable(true)
                .setSortable(true);

        addColumn(transaction -> transaction.getBatch().getCode())
                .setId("batch.code")
                .setCaption("Batch")
                .setHidable(true)
                .setSortable(true);

        addColumn(transaction -> transaction.getQuantity())
                .setId("quantity")
                .setCaption("Quantity")
                .setHidable(true)
                .setSortable(false);

        addColumn(transaction -> transaction.getCreator())
                .setId("creator")
                .setCaption("By")
                .setHidable(true)
                .setSortable(true);

        addColumn(transaction -> transaction.getBatchBalanceBefore())
                .setId("batchBalanceBefore")
                .setCaption("Batch Balance before")
                .setHidable(true)
                .setSortable(false);

        addColumn(transaction -> transaction.getBatchBalanceAfter())
                .setId("batchBalanceAfter")
                .setCaption("Batch Balance after")
                .setHidable(true)
                .setSortable(false);

        addColumn(transaction -> transaction.getItemBalanceBefore())
                .setId("itemBalanceBefore")
                .setCaption("Item balance before")
                .setHidable(true)
                .setSortable(false);

        addColumn(transaction -> transaction.getItemBalanceAfter())
                .setId("itemBalanceAfter")
                .setCaption("Item balance after")
                .setHidable(true)
                .setSortable(false);

        addColumn(transaction -> transaction.getTotalValueBefore())
                .setId("valueBefore")
                .setCaption("Value before")
                .setHidable(true)
                .setSortable(false);

        addColumn(transaction -> transaction.getTotalValueAfter())
                .setId("valueAfter")
                .setCaption("Value after")
                .setHidable(true)
                .setSortable(false);

        addColumn(transaction -> transaction.getBatchBaseUnitPriceBefore())
                .setId("baseUnitPriceBefore")
                .setCaption("Price before")
                .setHidable(true)
                .setSortable(false);

        addColumn(transaction -> transaction.getBatchBaseUnitPriceAfter())
                .setId("baseUnitPriceAfter")
                .setCaption("Price after")
                .setHidable(true)
                .setSortable(false);

        LocalDateTimeRangeSelector timeCreatedRangePanel = new LocalDateTimeRangeSelector();
        timeCreatedRangePanel.setWidth("200px");

        HeaderRow headerRow = appendHeaderRow();
        headerRow.getCell("time").setComponent(timeCreatedRangePanel);
        timeCreatedRangePanel.addValueChangeListener(event -> {
            getDataProvider().setTimeRange(timeCreatedRangePanel.getStart(), timeCreatedRangePanel.getEnd());
        });

        createFilteringComboBox(
                headerRow,
                "creator",
                "Filter creator",
                usersProvider.withConvertedFilter(name -> UserService.UserFilter.builder().name(name).build()),
                event -> getDataProvider().setCreator(event.getValue())
        );

        createFilteringComboBox(
                headerRow,
                "batch.item",
                "Filter item",
                itemsProvider,
                event -> getDataProvider().setItem(event.getValue())
        );

        createFilteringTextField(
                headerRow,
                "batch.code",
                "Filter batch",
                event -> getDataProvider().setBatchCode(event.getValue()));

        createFilteringComboBox(
                headerRow,
                "batch.store",
                "Filter by store",
                storeService.findOperatedStore(currentUser),
                event -> getDataProvider().setStores(List.of(event.getValue()))
        );

        addItemClickListener(event -> showTransaction(event.getItem()));

    }

    private void showTransaction(Transaction transaction) {

    }
}
