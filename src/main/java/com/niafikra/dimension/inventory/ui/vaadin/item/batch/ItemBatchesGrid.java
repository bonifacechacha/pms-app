package com.niafikra.dimension.inventory.ui.vaadin.item.batch;

import com.niafikra.dimension.inventory.domain.Batch;
import com.niafikra.dimension.inventory.domain.Store;
import com.niafikra.dimension.inventory.service.StoreService;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;
import com.vaadin.ui.components.grid.HeaderRow;
import org.apache.commons.lang3.StringUtils;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;

import static com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils.createFilteringComboBox;
import static com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils.createFilteringTextField;

@PrototypeScope
@SpringComponent
public class ItemBatchesGrid extends Grid<Batch> {

    private StoreService storeService;
    private TextField codeFilter;
    private ComboBox<Store> storeFilter;

    public ItemBatchesGrid(StoreService storeService) {
        super(Batch.class);
        this.storeService = storeService;
    }

    @PostConstruct
    private void build() {
        addColumn(batch -> batch.calculateValue()).setId("value");
        setColumns("code", "store", "balance", "baseUnitPrice", "value", "expiryDate");

        HeaderRow headerRow = appendHeaderRow();
        codeFilter = createFilteringTextField(
                headerRow,
                "code",
                "Filter code",
                event -> doFilter()
        );

        storeFilter = createFilteringComboBox(
                headerRow,
                "store",
                "Filter by store",
                storeService.findAll(),
                event -> doFilter()
        );
    }

    private void doFilter() {
        getItemBatchesProvider().clearFilters();

        if (!codeFilter.isEmpty())
            getItemBatchesProvider().addFilter(batch -> StringUtils.containsIgnoreCase(batch.getCode(), codeFilter.getValue()));

        if (!storeFilter.isEmpty())
            getItemBatchesProvider().addFilter(batch -> batch.getStore().equals(storeFilter.getValue()));
    }

    public ListDataProvider<Batch> getItemBatchesProvider() {
        return (ListDataProvider<Batch>) super.getDataProvider();
    }
}
