package com.niafikra.dimension.inventory.ui.vaadin.requisition.item;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.domain.requisition.legacy.ItemRequisition;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import de.steinwedel.messagebox.MessageBox;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.inventory.ui.vaadin.requisition.item.UserItemRequisitionsView.VIEW_NAME;


@Secured(Permission.VIEW_USER_ITEM_REQUESTS)
@ViewComponent(value = MainDisplay.class, caption = "My Item Request")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.INVENTORY, caption = "My Item Request")
@VaadinFontIcon(VaadinIcons.FILE_O)
public class UserItemRequisitionsView extends VerticalLayout implements View, TableExportable {
    public static final String VIEW_NAME = "user-item-requests";

    @Inject
    private ItemsRequisitionGrid requestsGrid;
    @Inject
    private ItemRequisitionCreateForm itemRequisitionForm;
    @Inject
    private ItemRequisitionSelector itemRequisitionSelector;
    private MessageBox itemRequisitionWindow;
    @Inject
    private User currentUser;

    @PostConstruct
    public void build() {

        setSizeFull();
        setMargin(false);
        requestsGrid.setSizeFull();
        addComponent(requestsGrid);
        setExpandRatio(requestsGrid, 1);
        requestsGrid.getDataProvider().setCreator(currentUser);
        itemRequisitionSelector.setSelectionMode(Grid.SelectionMode.NONE);
        itemRequisitionSelector.addItemClickListener(event -> {
            Item item = event.getItem();
            itemRequisitionWindow.close();
            showRequisitionCreateWindow(item);
        });
        itemRequisitionForm.getSaveButton().addClickListener(e -> requestsGrid.getDataProvider().refreshAll());
        itemRequisitionForm.getCreateSubmitButton().addClickListener(e -> requestsGrid.getDataProvider().refreshAll());
    }


    public void showRequisitionCreateWindow(Item item) {
        Window window = itemRequisitionForm.show(new ItemRequisition(item));
        window.addCloseListener(e -> requestsGrid.getDataProvider().refreshAll());
    }

    @Secured(Permission.CREATE_ITEM_REQUISITION)
    @ViewMenuOption(value = "New Request", icon = VaadinIcons.PLUS_CIRCLE)
    public void showItems() {
        if (itemRequisitionSelector.getItems().size() == 1) {
            showRequisitionCreateWindow(itemRequisitionSelector.getItems().stream().findFirst().get());
        } else {
            itemRequisitionWindow = MessageBox.create()
                    .withCaption("Select Item")
                    .withMessage(itemRequisitionSelector)
                    .withWidth("80%")
                    .withHeight("70%")
                    .asModal(true);
            itemRequisitionWindow.open();
        }
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(requestsGrid);
    }
}
