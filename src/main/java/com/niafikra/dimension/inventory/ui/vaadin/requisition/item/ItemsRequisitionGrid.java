package com.niafikra.dimension.inventory.ui.vaadin.requisition.item;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.security.service.UserService;
import com.niafikra.dimension.core.ui.vaadin.user.UsersProvider;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.util.date.LocalDateTimeRangeSelector;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.util.DateUtils;
import com.niafikra.dimension.inventory.domain.requisition.legacy.ItemRequisition;
import com.niafikra.dimension.inventory.service.ItemRequisitionService;
import com.niafikra.dimension.inventory.ui.vaadin.item.ItemsProvider;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.HeaderRow;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils.createFilteringComboBox;

@SpringComponent
@ViewScope
public class ItemsRequisitionGrid extends Grid<ItemRequisition> implements Notifier {

    @Inject
    private ItemRequisitionService itemRequisitionService;
    @Inject
    private ItemRequisitionProvider itemRequisitionProvider;

    @Inject
    private ItemsProvider itemsProvider;
    @Inject
    private UsersProvider usersProvider;

    @Inject
    private User currentUser;

    @Inject
    private Navigator navigator;

    public ItemsRequisitionGrid(ItemRequisitionProvider itemRequisitionProvider) {
        super(itemRequisitionProvider);
    }

    @PostConstruct
    private void build() {

        addColumn(itemRequisition -> DateUtils.formatDateTime(itemRequisition.getTimeCreated()))
                .setId("timeCreated")
                .setCaption("Time Created")
                .setSortable(true);

        addColumn(itemRequisition -> itemRequisition.getCreator())
                .setId("creator")
                .setCaption("Creator")
                .setHidable(true)
                .setSortable(true);

        addColumn(itemRequisition -> itemRequisition.getItem().getName())
                .setId("item")
                .setCaption("Item")
                .setHidable(true)
                .setSortable(true);
        addColumn(itemRequisition -> itemRequisition.getRequestedQty())
                .setId("requestedQty")
                .setCaption("Req. Qty")
                .setHidable(true)
                .setSortable(true);
        addColumn(itemRequisition -> itemRequisition.getApprovedQty())
                .setId("approvedQty")
                .setCaption("App. Qty")
                .setHidable(true)
                .setSortable(true);

        addColumn(itemRequisition -> itemRequisition.getPendingQty())
                .setId("pendingQty")
                .setCaption("Pending")
                .setHidable(true)
                .setSortable(true);

        addColumn(itemRequisition -> itemRequisitionService.getStatus(itemRequisition))
                .setId("status")
                .setCaption("Status")
                .setHidable(true)
                .setSortable(false);

        LocalDateTimeRangeSelector timeCreatedRangePanel = new LocalDateTimeRangeSelector();
        timeCreatedRangePanel.setWidth("200px");

        HeaderRow headerRow = appendHeaderRow();
        headerRow.getCell("timeCreated").setComponent(timeCreatedRangePanel);
        timeCreatedRangePanel.addValueChangeListener(event -> {
            itemRequisitionProvider.setTimeRange(timeCreatedRangePanel.getStart(), timeCreatedRangePanel.getEnd());
        });

        createFilteringComboBox(
                headerRow,
                "creator",
                "Filter creator",
                usersProvider.withConvertedFilter(s -> UserService.UserFilter.builder().name(s).build()),
                event -> itemRequisitionProvider.setCreator(event.getValue())
        );

        createFilteringComboBox(
                headerRow,
                "item",
                "Filter item",
                itemsProvider,
                event -> itemRequisitionProvider.setItem(event.getValue())
        );

        addItemClickListener(event -> showRequisition(event.getItem()));
    }

    private void showRequisition(ItemRequisition itemRequisition) {
        navigator.navigateTo(ItemRequisitionView.class, itemRequisition.getId());
    }


    @Override
    public ItemRequisitionProvider getDataProvider() {
        return (ItemRequisitionProvider) super.getDataProvider();
    }

    private void doDelete(ItemRequisition requisition) {
        try {
            itemRequisitionService.delete(requisition);
            getDataProvider().refreshAll();
            showSuccess("Deleted item requisition successful", requisition.toString());
        } catch (Exception e) {
            showError("Failed to delete item requisition", e);
        }
    }

    private void doSubmit(ItemRequisition requisition) {
        try {
            itemRequisitionService.submit(requisition, currentUser);
            getDataProvider().refreshAll();
            showSuccess("Submitted Item requisition for approval successfully", requisition.toString());
        } catch (Exception e) {
            showError("Failed to submit Item requisition for approval", e);
        }
    }

}
