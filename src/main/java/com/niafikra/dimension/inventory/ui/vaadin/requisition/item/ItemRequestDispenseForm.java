package com.niafikra.dimension.inventory.ui.vaadin.requisition.item;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.inventory.domain.requisition.legacy.ItemRequisition;
import com.niafikra.dimension.inventory.service.ItemRequisitionService;
import com.niafikra.dimension.inventory.service.ItemService;
import com.niafikra.dimension.inventory.transaction.dispense.DispenseBatchData;
import com.niafikra.dimension.inventory.transaction.dispense.DispenseData;
import com.niafikra.dimension.inventory.ui.vaadin.transaction.dispense.DispenseForm;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.fields.LabelField;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@PrototypeScope
@SpringComponent
public class ItemRequestDispenseForm extends DispenseForm {

    private LabelField<User> associatedUser = new LabelField<>("Associated User");
    private ItemRequisitionService requisitionService;
    private User currentUser;

    private ItemRequisition request;

    public ItemRequestDispenseForm(ItemRequisitionService requisitionService,
                                   ItemService itemService,
                                   User currentUser) {
        super(itemService);

        this.requisitionService = requisitionService;
        this.currentUser = currentUser;
    }

    @PostConstruct
    private void build() {

        item.setWidth(FIELD_WIDTH);
        store.setWidth(FIELD_WIDTH);
        associatedUser.setWidth(FIELD_WIDTH);
        time.setWidth(FIELD_WIDTH);

        addSavedHandler(dispenseData -> doDispense(dispenseData));
        setResetHandler(dispenseData -> resetDispenseData());

        store.addValueChangeListener(event -> {

            if (event.getValue() != null) {
                List<DispenseBatchData> batchData = requisitionService.resolveBatchDispense(request, event.getValue());
                batches.setValue(batchData.stream().collect(Collectors.toSet()));
            } else {
                batches.setValue(Collections.emptySet());
            }
        });
    }


    private void doDispense(DispenseData dispenseData) {
        dispenseData.setUser(currentUser);
        try {
            requisitionService.dispense(request, dispenseData);
            showSuccess("Successful dispensed : " + dispenseData.getTotalQuantity(), item.toString());
            closePopup();
        } catch (Exception e) {
            showError("Failed to dispense items", e);
        }
    }

    @Override
    protected Component createContent() {
        return new MVerticalLayout(
                item,
                new MHorizontalLayout(store, time).withFullWidth(),
                associatedUser.withFullWidth(),
                batches,
                getToolbar(),
                itemSummaryLabel
        ).withFullWidth();
    }

    public void setRequisition(ItemRequisition request) {
        this.request = request;
        resetDispenseData();
    }

    private void resetDispenseData() {
        DispenseData dispenseData = new DispenseData(
                request.getItem(),
                request.getCreator()
        );

        setEntity(dispenseData);
    }
}
