package com.niafikra.dimension.inventory.ui.vaadin.requisition.item;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.approval.domain.Tracker;
import com.niafikra.dimension.approval.service.ApprovalTrackerService;
import com.niafikra.dimension.approval.ui.vaadin.tracker.ApprovalDisplay;
import com.niafikra.dimension.approval.ui.vaadin.tracker.ApprovalView;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainHeader;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.TemplatePanel;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.niafikra.dimension.inventory.domain.requisition.legacy.ItemRequisition;
import com.niafikra.dimension.inventory.service.ItemRequisitionService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.Permission.VIEW_ITEM_REQUISITION_APPROVAL_REQUEST;
import static com.niafikra.dimension.inventory.ui.vaadin.requisition.item.ItemRequisitionApprovalView.VIEW_NAME;

@Secured(VIEW_ITEM_REQUISITION_APPROVAL_REQUEST)
@SpringView(name = VIEW_NAME)
@ViewComponent(value = ApprovalDisplay.class, caption = "Item requisition  approval")
public class ItemRequisitionApprovalView extends VerticalLayout implements ApprovalView {
    public static final String VIEW_NAME = "ap-itemrequisition-approval";

    @Inject
    private TemplatePanel templatePanel;

    @Inject
    private ItemRequisitionQuantityChangeForm quantityChangeForm;

    @Inject
    private ApprovalTrackerService trackerService;

    @Inject
    private ItemRequisitionService requisitionService;

    @Inject
    private Navigator navigator;

    @Inject
    private MainHeader mainHeader;

    @Inject
    private User currentUser;

    private ItemRequisition itemRequisition;


    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);
        templatePanel.getTemplateView().setTemplatePath(Templates.ITEM_REQUISITION);
        addComponent(templatePanel);
    }


    @Secured(Permission.CHANGE_ITEM_REQUEST_AMOUNT)
    @ViewMenuOption(value = "Change Amount", icon = VaadinIcons.EDIT)
    public void doChangeAmount() {
        quantityChangeForm.setRequisition(itemRequisition);
        quantityChangeForm.openInModalPopup()
                .addCloseListener(e -> UI.getCurrent().access(
                        () -> setRequisition(requisitionService.getRequisition(itemRequisition.getId())))
                );
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long id = Long.parseLong(event.getParameters());
        Tracker tracker = trackerService.getTracker(id);
        ItemRequisition itemRequisition = requisitionService.getRequisition(tracker);

        setRequisition(itemRequisition);
    }

    private void setRequisition(ItemRequisition requisition) {
        this.itemRequisition = requisition;

        mainHeader.getMenuBar().setMenuVisible("Change Amount", trackerService.canApprove(requisition, currentUser));
        templatePanel.getTemplateView().putBinding("itemRequisition", itemRequisition).render();

    }
}
