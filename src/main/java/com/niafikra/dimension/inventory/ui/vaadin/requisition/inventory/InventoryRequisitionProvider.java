package com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory;

import com.niafikra.dimension.inventory.domain.requisition.InventoryRequisition;
import com.niafikra.dimension.inventory.service.InventoryRequisitionService;
import com.niafikra.dimension.inventory.service.InventoryRequisitionService.InventoryRequisitionFilter;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.spring.annotation.SpringComponent;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;
import org.vaadin.spring.annotation.PrototypeScope;
import java.util.List;
import static com.niafikra.dimension.inventory.service.InventoryRequisitionService.InventoryRequisitionFilter.builder;

@PrototypeScope
@SpringComponent
@RequiredArgsConstructor
public class InventoryRequisitionProvider extends PageableDataProvider<InventoryRequisition, InventoryRequisitionFilter> {

    private final InventoryRequisitionService requisitionService;

    @Override
    protected Page<InventoryRequisition> fetchFromBackEnd(
            Query<InventoryRequisition, InventoryRequisitionFilter> query,
            Pageable pageable) {
        return requisitionService.findAll(
                query.getFilter().orElseGet(builder()::build),
                pageable
        );
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        return QuerySortOrder.desc("timeCreated").build();
    }

    @Override
    protected int sizeInBackEnd(Query<InventoryRequisition, InventoryRequisitionFilter> query) {
        return requisitionService.countAll(
                query.getFilter().orElseGet(builder()::build)
        ).intValue();
    }
}
