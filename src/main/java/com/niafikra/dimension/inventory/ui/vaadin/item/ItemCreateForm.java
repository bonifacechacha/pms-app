package com.niafikra.dimension.inventory.ui.vaadin.item;

import com.niafikra.dimension.category.service.CategoryService;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.service.ItemService;
import com.niafikra.dimension.inventory.service.MeasurementUnitService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Window;
import org.vaadin.spring.annotation.PrototypeScope;

@SpringComponent
@PrototypeScope
public class ItemCreateForm extends ItemForm {

    private ItemService itemService;

    public ItemCreateForm(CategoryService categoryService, MeasurementUnitService unitService, ItemService itemService) {
        super(categoryService, unitService);
        this.itemService = itemService;

        addSavedHandler(item -> {
            doCreate(item);
        });
    }

    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setCaption("Create new inventory item");
        return window;
    }

    private void doCreate(Item item) {
        try {
            itemService.create(item);
            showSuccess("Create new inventory item successful", item.toString());
            closePopup();
        } catch (Exception e) {
            showError("Failed to create item", e);
        }

    }

}
