package com.niafikra.dimension.inventory.ui.vaadin.transaction.dispense;

import com.niafikra.dimension.inventory.service.ItemService;
import com.niafikra.dimension.inventory.transaction.dispense.DispenseData;
import com.niafikra.dimension.inventory.ui.vaadin.transaction.BatchDataField;
import com.niafikra.dimension.inventory.ui.vaadin.transaction.TransactionForm;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Window;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public abstract class DispenseForm extends TransactionForm<DispenseData> {

    public static final String FIELD_WIDTH = "100%";
    @Inject
    protected DispenseBatchDataField batches;

    public DispenseForm(ItemService itemService) {
        super(DispenseData.class, itemService);
    }

    @PostConstruct
    private void build() {

        time.setWidth(FIELD_WIDTH);
        item.setWidth(FIELD_WIDTH);
        store.setWidth(FIELD_WIDTH);
        batches.setWidth(FIELD_WIDTH);
    }

    @Override
    public BatchDataField getBatchDataField() {
        return batches;
    }

    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setCaption("Dispense item batches");
        return window;
    }
}
