package com.niafikra.dimension.inventory.ui.vaadin.requisition.item;

import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.inventory.domain.requisition.legacy.ItemRequisition;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Window;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.button.ConfirmButton;
import org.vaadin.viritin.layouts.MHorizontalLayout;

import javax.annotation.PostConstruct;

@SpringComponent
@PrototypeScope
public class ItemRequisitionCreateForm extends RequisitionEditForm implements Notifier {
    private ConfirmButton createSubmitButton;

    public ItemRequisitionCreateForm() {
        super();
    }

    @PostConstruct
    private void build() {

        getSaveButton().setWidth("200px");
        getSaveButton().setCaption("Create requisition");
        addSavedHandler(itemRequisition -> doCreate(itemRequisition));

        createSubmitButton = new ConfirmButton("Create and submit", "Are you sure",
                event -> createSubmit());
        createSubmitButton.setWidth("200px");
        createSubmitButton.addStyleNames(Theme.BUTTON_PRIMARY);
        createSubmitButton.setEnabled(false);
    }


    private void createSubmit() {
        ItemRequisition itemRequisition = getItemRequisition();
        try {
            itemRequisitionService.createSubmit(itemRequisition, currentUser);
            showSuccess("Successfully submitted requisition");
            closePopup();
        } catch (Exception e) {
            showError("Failed to submit requisition", e);
        }

    }


    private void doCreate(ItemRequisition itemRequisition) {
        try {
            itemRequisitionService.create(itemRequisition, currentUser);
            showSuccess("Successfully created requisition");
            closePopup();
        } catch (Exception e) {
            showError("Failed to create requisition", e);
        }
    }

    @Override
    public MHorizontalLayout getToolbar() {
        return super.getToolbar().withComponentAsFirst(createSubmitButton);
    }

    public ConfirmButton getCreateSubmitButton() {
        return createSubmitButton;
    }

    @Override
    protected void adjustSaveButtonState() {
        super.adjustSaveButtonState();
        createSubmitButton.setEnabled(getSaveButton().isEnabled());
    }

    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setCaption("New item request");
        return window;
    }
}
