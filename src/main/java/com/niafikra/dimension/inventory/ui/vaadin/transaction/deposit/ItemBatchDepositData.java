package com.niafikra.dimension.inventory.ui.vaadin.transaction.deposit;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.domain.Store;
import com.niafikra.dimension.inventory.transaction.deposit.DepositBatchData;
import com.niafikra.dimension.inventory.transaction.deposit.DepositData;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
public class ItemBatchDepositData extends DepositBatchData {
    @NotNull
    private Item item;

    public ItemBatchDepositData() {
        super();
    }

    public static DepositData toDepositData(
            LocalDateTime time,
            Store store,
            User creator,
            Item item,
            List<ItemBatchDepositData> itemBatchDepositData) {

        DepositData depositData = new DepositData(item);
        depositData.setUser(creator);
        depositData.setStore(store);
        depositData.setTime(time);
        depositData.setBatches(
                itemBatchDepositData.stream()
                .map(data -> data.toDepositBatchData())
                .collect(Collectors.toSet())
        );
        return depositData;
    }

    private DepositBatchData toDepositBatchData() {
        DepositBatchData batchData = new DepositBatchData(
                getBatchCode(),
                getQuantity(),
                getNotes(),
                getUnitBuyingPrice(),
                getExpiryDate()
        );

        return batchData;
    }
}
