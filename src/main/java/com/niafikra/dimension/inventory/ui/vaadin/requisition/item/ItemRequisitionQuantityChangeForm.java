package com.niafikra.dimension.inventory.ui.vaadin.requisition.item;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.util.SubWindow;
import com.niafikra.dimension.inventory.domain.requisition.legacy.ItemRequisition;
import com.niafikra.dimension.inventory.service.ItemRequisitionService;
import com.niafikra.dimension.inventory.ui.vaadin.QuantityField;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.TextArea;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class ItemRequisitionQuantityChangeForm extends MVerticalLayout implements Notifier {

    private QuantityField quantity = new QuantityField("Proposed Quantity");
    private TextArea reason = new TextArea("Reason");

    @Inject
    private ItemRequisitionService itemRequisitionService;

    @Inject
    private User currentUser;

    private ItemRequisition requisition;
    private SubWindow subWindow;

    @PostConstruct
    private void build() {
        quantity.setWidth("100%");
        reason.setRows(4);
        reason.setWidth("100%");

        MButton submit = new MButton("Submit")
                .withWidth("200px")
                .addClickListener(() -> doSubmit());
        addComponents(quantity, reason, submit);
    }

    private void doSubmit() {
        try {
            itemRequisitionService.changeApprovedQuantity(
                    requisition,
                    currentUser,
                    quantity.getValue(),
                    reason.getValue()
            );
            subWindow.close();
        } catch (Exception e) {
            showError("Failed to change Item quantity", e);
        }
    }

    public SubWindow openInModalPopup() {
        subWindow = new SubWindow(this);
        subWindow.withCaption("Propose New Quantity for requisition")
                .show()
                .withWidth("650px");

        return subWindow;
    }

    public void setRequisition(ItemRequisition requisition) {
        this.requisition = requisition;
        quantity.setValue(requisition.getApprovedQty());
    }
}
