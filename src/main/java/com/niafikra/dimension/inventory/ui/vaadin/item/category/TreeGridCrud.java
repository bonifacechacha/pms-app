package com.niafikra.dimension.inventory.ui.vaadin.item.category;

import com.vaadin.ui.TreeGrid;
import org.vaadin.crudui.crud.FindAllCrudOperationListener;
import org.vaadin.crudui.crud.impl.GridCrud;
import org.vaadin.crudui.layout.CrudLayout;

import java.util.Collection;
import java.util.function.Function;
import java.util.function.Supplier;

public class TreeGridCrud<T> extends GridCrud<T> {
    private Function<T, Collection<T>> childrenProvider;
    private Supplier<Collection<T>> rootsProvider;

    public TreeGridCrud(Class<T> domainType) {
        super(domainType);
    }

    public TreeGridCrud(Class<T> domainType, CrudLayout crudLayout) {
        super(domainType, crudLayout);
    }

    protected void initLayout() {
        super.initLayout();
        grid = new TreeGrid<>(domainType);
        grid.setSizeFull();
        grid.asSingleSelect().addValueChangeListener(e -> gridSelectionChanged());
//        grid.addSelectionListener(e -> gridSelectionChanged());
        crudLayout.setMainComponent(grid);
    }

    public Function<T, Collection<T>> getChildrenProvider() {
        return childrenProvider;
    }

    public void setChildrenProvider(Function<T, Collection<T>> childrenProvider) {
        this.childrenProvider = childrenProvider;
    }

    public Supplier<Collection<T>> getRootsProvider() {
        return rootsProvider;
    }

    public void setRootsProvider(Supplier<Collection<T>> rootsProvider) {
        this.rootsProvider = rootsProvider;
    }

    @Override
    public void refreshGrid() {
        getGrid().setItems(rootsProvider.get(), t -> childrenProvider.apply(t));
    }

    @Override
    public TreeGrid<T> getGrid() {
        return (TreeGrid<T>) super.getGrid();
    }

    @Override
    public void setFindAllOperation(FindAllCrudOperationListener<T> findAllOperation) {
        throw new UnsupportedOperationException("Use set root provider and set children provider");
    }
}
