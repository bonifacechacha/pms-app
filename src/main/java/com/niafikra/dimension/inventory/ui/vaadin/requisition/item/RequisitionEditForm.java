package com.niafikra.dimension.inventory.ui.vaadin.requisition.item;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.attachment.ui.vaadin.util.AttachmentsField;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.util.AbstractForm;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.domain.requisition.legacy.ItemRequisition;
import com.niafikra.dimension.inventory.service.ItemRequisitionService;
import com.niafikra.dimension.inventory.ui.vaadin.QuantityField;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.viritin.fields.LabelField;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Set;


public class RequisitionEditForm extends AbstractForm<ItemRequisition> {

    @Inject
    protected ItemRequisitionService itemRequisitionService;
    @Inject
    protected User currentUser;
    @Inject
    private AttachmentsField<Set<Attachment>> attachments;
    private LabelField<Item> item = new LabelField<>("Item");
    private QuantityField requestedQty = new QuantityField("Quantity");
    private TextArea notes = new TextArea("");

    public RequisitionEditForm() {
        super(ItemRequisition.class);
    }

    @PostConstruct
    private void build() {
        getSaveButton().setWidth("200px");
        item.setWidth("100%");
        item.setRequiredIndicatorVisible(false);
        item.addStyleNames(ValoTheme.LABEL_BOLD, ValoTheme.LABEL_COLORED, ValoTheme.LABEL_H3);
        requestedQty.setWidth("100%");
        notes.setRows(4);
        notes.setWidth("100%");
        attachments.setWidth("100%");
        attachments.setCaption("Attachments");
        notes.setPlaceholder("Notes");
    }

    @Override
    protected Component createContent() {
        Component toolbar = getToolbar();
        return new MVerticalLayout(
                item,
                requestedQty.withFullWidth(),
                new MHorizontalLayout(notes, attachments).withFullWidth(),
                toolbar
        ).withFullWidth().withAlign(toolbar, Alignment.BOTTOM_CENTER).withMargin(true);

    }

    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setWidth("600px");
        return window;
    }

    public Window show(ItemRequisition requisition) {
        setItemRequisition(requisition);
        return openInModalPopup();
    }

    public ItemRequisition getItemRequisition() {
        return getEntity();
    }

    public void setItemRequisition(ItemRequisition itemRequisition) {
        setEntity(itemRequisition);
    }
}
