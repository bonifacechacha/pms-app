package com.niafikra.dimension.inventory.ui.vaadin.transaction;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.domain.Store;
import com.niafikra.dimension.inventory.domain.transaction.Transaction;
import com.niafikra.dimension.inventory.service.TransactionService;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@SpringComponent
@ViewScope
public class TransactionsProvider extends PageableDataProvider<Transaction, Void> {
    private LocalDateTime startTime, endTime;
    private String batchCode;
    private Collection<Store> stores;
    private Item item;
    private User creator;
    private String notes;

    @Inject
    private TransactionService transactionService;

    @Override
    protected Page<Transaction> fetchFromBackEnd(Query<Transaction, Void> query, Pageable pageable) {
        return transactionService.findAll(
                batchCode,
                stores,
                item,
                creator,
                notes,
                startTime,
                endTime,
                pageable
        );
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        return List.of(new QuerySortOrder("time", SortDirection.DESCENDING));
    }

    @Override
    protected int sizeInBackEnd(Query<Transaction, Void> query) {
        return transactionService.countAll(
                batchCode,
                stores,
                item,
                creator,
                notes,
                startTime,
                endTime
        ).intValue();
    }

    public void setTimeRange(LocalDateTime startDateTime, LocalDateTime endDateTime) {
        this.startTime = startDateTime;
        this.endTime = endDateTime;
        refreshAll();
    }

    public void setCreator(User creator) {
        this.creator = creator;
        refreshAll();
    }

    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
        refreshAll();
    }

    public void setStores(Collection<Store> stores) {
        this.stores = stores;
        refreshAll();
    }

    public void setItem(Item item) {
        this.item = item;
        refreshAll();
    }

    public void setNotes(String notes) {
        this.notes = notes;
        refreshAll();
    }
}
