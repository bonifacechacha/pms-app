package com.niafikra.dimension.inventory.ui.vaadin.requisition.item;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainHeader;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.util.TemplatePanel;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.niafikra.dimension.inventory.domain.requisition.legacy.ItemRequisition;
import com.niafikra.dimension.inventory.service.ItemRequisitionService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.inventory.ui.vaadin.requisition.item.ItemRequisitionView.VIEW_NAME;

@Secured(Permission.VIEW_ITEM_REQUEST)
@ViewComponent(value = MainDisplay.class, caption = "Item Request")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class ItemRequisitionView extends MVerticalLayout implements Notifier, View {
    public static final String VIEW_NAME = "item-requisition";

    @Inject
    private TemplatePanel templatePanel;

    @Inject
    private ItemRequisitionUpdateForm editForm;

    @Inject
    private ItemRequisitionService itemRequisitionService;

    @Inject
    private MainHeader mainHeader;

    @Inject
    private Navigator navigator;

    @Inject
    private User currentUser;

    @Inject
    private ItemRequestDispenseForm dispenseForm;


    private ItemRequisition itemRequisition;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        this.templatePanel.getTemplateView().setTemplatePath(Templates.ITEM_REQUISITION);
        templatePanel.setSizeFull();
        withComponents(templatePanel)
                .withExpand(templatePanel, 1);
    }

    @Secured(Permission.EDIT_ITEM_REQUEST)
    @ViewMenuOption(value = "Edit", menu = "Request", icon = VaadinIcons.EDIT, menuIcon = VaadinIcons.ACCESSIBILITY)
    public void doEdit() {
        editForm.setItemRequisition(itemRequisition);
        Window window = editForm.openInModalPopup();
        window.addCloseListener(e -> {
            setItemRequisition(itemRequisitionService.getRequisition(itemRequisition.getId()));
        });
    }

    @Secured(Permission.DELETE_ITEM_REQUEST)
    @ViewMenuOption(value = "Delete", menu = "Request", icon = VaadinIcons.TRASH, menuIcon = VaadinIcons.ACCESSIBILITY)
    public void doDelete() {
        ConfirmDialog.show(
                getUI(),
                "Are you sure you wish to delete this request?",
                dialog -> {
                    if (dialog.isConfirmed()) {
                        try {
                            itemRequisitionService.delete(itemRequisition);
                            navigator.navigateTo(UserItemRequisitionsView.class);
                            showSuccess("Deleted item requisition successful", itemRequisition.toString());
                        } catch (Exception e) {
                            showError("Failed to delete item requisition", e);
                        }
                    }
                }
        );
    }

    @Secured(Permission.SUBMIT_ITEM_REQUEST)
    @ViewMenuOption(value = "Submit", menu = "Request", icon = VaadinIcons.KEY, menuIcon = VaadinIcons.ACCESSIBILITY)
    public void doSubmit() {
        ConfirmDialog.show(
                getUI(),
                "Are you sure you wish to submit this request for approval?",
                dialog -> {
                    if (dialog.isConfirmed()) {
                        try {
                            itemRequisitionService.submit(itemRequisition, currentUser);
                            UI.getCurrent().access(() -> navigator.reload());
                            showSuccess("Submitted Item requisition for approval successfully", itemRequisition.toString());
                        } catch (Exception e) {
                            showError("Failed to submit Item requisition for approval", e);
                        }
                    }
                }
        );
    }

    @Secured(Permission.DISPENSE_ITEM_REQUEST)
    @ViewMenuOption(value = "Dispense", menu = "Request", icon = VaadinIcons.ARROW_CIRCLE_UP, menuIcon = VaadinIcons.ACCESSIBILITY)
    public void doDispense() {
        dispenseForm.setRequisition(itemRequisition);
        dispenseForm.openInModalPopup().addCloseListener(e -> UI.getCurrent().access(() -> navigator.reload()));
    }

    public void setItemRequisition(ItemRequisition itemRequisition) {
        this.itemRequisition = itemRequisition;
        templatePanel.getTemplateView().putBinding("itemRequisition", itemRequisition).render();
        mainHeader.getMenuBar().setMenuVisible("Edit", itemRequisitionService.isEditable(itemRequisition, currentUser));
        mainHeader.getMenuBar().setMenuVisible("Submit", !itemRequisitionService.isSubmitted(itemRequisition));
        mainHeader.getMenuBar().setMenuVisible("Delete", !itemRequisition.isDispensed());
        mainHeader.getMenuBar().setMenuVisible("Dispense", !itemRequisition.isFulfilled() && !itemRequisition.isCompletelyDispensed());
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long requestId = Long.parseLong(event.getParameters());
        setItemRequisition(itemRequisitionService.getRequisition(requestId));
    }
}
