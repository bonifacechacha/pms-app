package com.niafikra.dimension.inventory.ui.vaadin.requisition.item;


import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.inventory.domain.requisition.legacy.ItemRequisition;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Window;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.button.ConfirmButton;
import org.vaadin.viritin.layouts.MHorizontalLayout;

import javax.annotation.PostConstruct;

@PrototypeScope
@SpringComponent
public class ItemRequisitionUpdateForm extends RequisitionEditForm implements Notifier {

    private ConfirmButton updateSubmitButton;

    public ItemRequisitionUpdateForm() {
        super();
    }

    @PostConstruct
    private void build() {
        addSavedHandler(requisition -> update());
        getSaveButton().setWidth("200px");
        getSaveButton().setCaption("Update requisitRequisitionion");

        updateSubmitButton = new ConfirmButton(
                "Update and submit",
                "Are you sure?",
                event -> updateSubmit()
        );

        updateSubmitButton.setWidth("200px");
        updateSubmitButton.addStyleNames(Theme.BUTTON_PRIMARY);
        updateSubmitButton.setEnabled(false);
    }

    private void updateSubmit() {
        ItemRequisition requisition = getItemRequisition();
        try {
            itemRequisitionService.updateSubmit(requisition, currentUser);
            closePopup();
            showSuccess("Successfully submitted requisition", requisition.toString());
        } catch (Exception e) {
            showError("Failed to submit requisition", e);
        }
    }

    private void update() {
        ItemRequisition requisition = getItemRequisition();
        try {
            itemRequisitionService.update(requisition, currentUser);
            closePopup();
            showSuccess("Successfully updated requisition", requisition.toString());
        } catch (Exception e) {
            showError("Failed to update requisition", e);
        }
    }

    @Override
    public void setEntity(ItemRequisition requisition) {
        super.setEntity(requisition);
        updateSubmitButton.setVisible(!itemRequisitionService.isSubmitted(requisition));
    }

    @Override
    public MHorizontalLayout getToolbar() {
        return super.getToolbar().withComponentAsFirst(updateSubmitButton);
    }

    public ConfirmButton getUpdateSubmitButton() {
        return updateSubmitButton;
    }

    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setCaption("Update item request");
        return window;
    }

    @Override
    protected void adjustSaveButtonState() {
        super.adjustSaveButtonState();
        updateSubmitButton.setEnabled(getSaveButton().isEnabled());
    }
}
