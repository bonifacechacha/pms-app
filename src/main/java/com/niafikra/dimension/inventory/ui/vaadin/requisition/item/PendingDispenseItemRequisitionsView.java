package com.niafikra.dimension.inventory.ui.vaadin.requisition.item;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.inventory.ui.vaadin.requisition.item.PendingDispenseItemRequisitionsView.VIEW_NAME;


@Secured({Permission.VIEW_PENDING_ITEM_REQUISITION})
@ViewComponent(value = MainDisplay.class, caption = "Pending Dispense")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.INVENTORY, caption = "Pending Dispense")
@VaadinFontIcon(VaadinIcons.CUBE)
public class PendingDispenseItemRequisitionsView extends VerticalLayout implements View {
    public static final String VIEW_NAME = "pending-dispense-requisition";

    @Inject
    private ItemsRequisitionGrid pendingItemGrid;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);
        pendingItemGrid.setSizeFull();
        pendingItemGrid.getDataProvider().setApproved(true);
        pendingItemGrid.getDataProvider().setFullFilled(false);
        addComponent(pendingItemGrid);
    }

}
