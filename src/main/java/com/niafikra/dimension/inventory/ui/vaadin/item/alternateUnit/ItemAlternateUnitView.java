package com.niafikra.dimension.inventory.ui.vaadin.item.alternateUnit;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.inventory.domain.unit.AlternateUnit;
import com.niafikra.dimension.inventory.domain.unit.MeasurementUnit;
import com.niafikra.dimension.inventory.service.AlternateUnitService;
import com.niafikra.dimension.inventory.service.ItemService;
import com.niafikra.dimension.inventory.ui.vaadin.item.AbstractItemGridCrud;
import com.niafikra.dimension.inventory.ui.vaadin.item.ItemDisplay;
import com.vaadin.data.HasValue;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.crudui.layout.impl.WindowBasedCrudLayout;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Collections;

import static com.niafikra.dimension.inventory.ui.vaadin.item.alternateUnit.ItemAlternateUnitView.VIEW_NAME;


@Secured(Permission.CONFIGURE_ALTERNATE_UNIT)
@ViewComponent(value = ItemDisplay.class, caption = "Item alternate units")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class ItemAlternateUnitView extends AbstractItemGridCrud<AlternateUnit> implements Notifier {
    public static final String VIEW_NAME = "item-alternate-unit";
    public static final String[] TABLE_VISIBLE_FIELDS = {"unit", "baseUnitRation", "UPC"};
    public static final String[] FORM_VISIBLE_FIELDS = {"unit", "baseUnitRation", "UPC"};

    private AlternateUnitService alternateUnitService;

    public ItemAlternateUnitView(AlternateUnitService alternateUnitService, ItemService itemService) {
        super(itemService, AlternateUnit.class, new WindowBasedCrudLayout());
        this.alternateUnitService = alternateUnitService;
    }

    @PostConstruct
    private void build() {
        setSizeFull();
        getGrid().setColumns(TABLE_VISIBLE_FIELDS);
        getCrudFormFactory().setVisibleProperties(FORM_VISIBLE_FIELDS);
        getCrudFormFactory().setUseBeanValidation(true);
        getCrudFormFactory().setFieldProvider("unit", () -> createUnitSelector());
        setFindAllOperation(() -> fetchAll());
        setAddOperation(unit -> doCreate(unit));
        setUpdateOperation(unit -> doUpdate(unit));
        setDeleteOperation(unit -> doDelete(unit));
    }

    private AlternateUnit doCreate(AlternateUnit unit) {
        try {
            itemService.createAlternateUnit(currentItem, unit);
            showSuccess("Created alternate unit successful");
            return unit;
        } catch (Exception e) {
            showError("Failed to create alternate unit", e);
            return null;
        }
    }

    private Collection<AlternateUnit> fetchAll() {
        if (currentItem == null) return Collections.emptyList();
        else return alternateUnitService.findAll(currentItem);
    }

    private HasValue createUnitSelector() {
        MComboBox<MeasurementUnit> unit = new MComboBox<>();
        unit.setItems(alternateUnitService.findPossibleAlternateMeasurementUnits(currentItem));
        return unit;
    }

    private void doDelete(AlternateUnit unit) {
        try {
            itemService.removeAlternateUnit(currentItem, unit);
            showSuccess("Deleted alternate unit successful");
        } catch (Exception e) {
            showError("Failed to delete alternate unit", e);
        }
    }

    private AlternateUnit doUpdate(AlternateUnit alternateUnit) {
        try {
            alternateUnit = alternateUnitService.update(alternateUnit);
            showSuccess("Updated alternate unit successful");
            return alternateUnit;
        } catch (Exception e) {
            showError("Failed to update alternate unit", e);
            return null;
        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        super.enter(event);
        refreshGrid();
    }
}
