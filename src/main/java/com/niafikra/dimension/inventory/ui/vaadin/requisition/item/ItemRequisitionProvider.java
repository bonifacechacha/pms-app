package com.niafikra.dimension.inventory.ui.vaadin.requisition.item;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.domain.requisition.legacy.ItemRequisition;
import com.niafikra.dimension.inventory.service.ItemRequisitionService;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.spring.annotation.SpringComponent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;
import org.vaadin.spring.annotation.PrototypeScope;

import java.time.LocalDateTime;
import java.util.List;

@PrototypeScope
@SpringComponent
public class ItemRequisitionProvider extends PageableDataProvider<ItemRequisition, Void> {
    private LocalDateTime startTime, endTime;
    private Boolean approved;
    private Boolean fullFilled;
    private User creator;
    private Item item;

    private ItemRequisitionService itemRequisitionService;

    public ItemRequisitionProvider(ItemRequisitionService itemRequisitionService) {
        this.itemRequisitionService = itemRequisitionService;
    }

    @Override
    protected Page<ItemRequisition> fetchFromBackEnd(Query<ItemRequisition, Void> query, Pageable pageable) {
        return itemRequisitionService.findRequisitions(
                creator,
                item,
                startTime,
                endTime,
                approved,
                fullFilled,
                pageable
        );
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        return List.of(new QuerySortOrder("timeCreated", SortDirection.DESCENDING));
    }

    @Override
    protected int sizeInBackEnd(Query<ItemRequisition, Void> query) {
        Long count = itemRequisitionService.countRequisitions(
                creator,
                item,
                startTime,
                endTime,
                approved,
                fullFilled
        );

        return count.intValue();
    }

    public void setTimeRange(LocalDateTime startDateTime, LocalDateTime endDateTime) {
        this.startTime = startDateTime;
        this.endTime = endDateTime;
        refreshAll();
    }

    public void setCreator(User creator) {
        this.creator = creator;
        refreshAll();
    }

    public void setFullFilled(Boolean fullFilled) {
        this.fullFilled = fullFilled;
        refreshAll();
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
        refreshAll();
    }

    public void setItem(Item value) {
        this.item = value;
        refreshAll();
    }
}
