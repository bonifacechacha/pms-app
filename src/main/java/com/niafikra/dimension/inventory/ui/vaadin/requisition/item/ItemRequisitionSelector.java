package com.niafikra.dimension.inventory.ui.vaadin.requisition.item;


import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.service.ItemService;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

import java.util.Collection;

@PrototypeScope
@SpringComponent
public class ItemRequisitionSelector extends ItemSelector {
    protected User currentUser;
    private ItemService itemService;

    public ItemRequisitionSelector(User currentUser, ItemService itemService) {
        this.currentUser = currentUser;
        this.itemService = itemService;
    }

    @Override
    protected Collection<Item> findItems() {
        return itemService.findAll();
    }
}
