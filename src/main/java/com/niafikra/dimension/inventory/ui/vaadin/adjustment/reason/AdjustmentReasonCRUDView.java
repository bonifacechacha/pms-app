package com.niafikra.dimension.inventory.ui.vaadin.adjustment.reason;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingDisplay;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingView;
import com.niafikra.dimension.core.ui.vaadin.util.MGridCrudView;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.niafikra.dimension.inventory.domain.transaction.adjustment.AdjustmentReason;
import com.niafikra.dimension.inventory.service.AdjustmentReasonService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.crudui.layout.impl.HorizontalSplitCrudLayout;

import javax.annotation.PostConstruct;

import static com.niafikra.dimension.inventory.ui.vaadin.adjustment.reason.AdjustmentReasonCRUDView.VIEW_NAME;


@Secured({Permission.CONFIGURE_STOCK_ADJUSTMENT_REASON})
@ViewComponent(value = SettingDisplay.class, caption = "Adjustment Reasons")
@ViewInfo(value = "Adjustment Reasons", section = "Inventory", icon = VaadinIcons.ADJUST)
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class AdjustmentReasonCRUDView extends MGridCrudView<AdjustmentReason> implements SettingView, Notifier {
    public static final String VIEW_NAME = "stock-adjustment-reasons";

    public static final String[] TABLE_VISIBLE_FIELDS = {"title"};
    public static final String[] FORM_VISIBLE_FIELDS = {"title"};

    private AdjustmentReasonService reasonService;

    public AdjustmentReasonCRUDView(AdjustmentReasonService reasonService) {
        super(AdjustmentReason.class, new HorizontalSplitCrudLayout());
        this.reasonService = reasonService;
    }

    @PostConstruct
    private void build() {
        getGrid().setColumns(TABLE_VISIBLE_FIELDS);
        getCrudFormFactory().setVisibleProperties(FORM_VISIBLE_FIELDS);
        getCrudFormFactory().setUseBeanValidation(true);

        setFindAllOperation(() -> reasonService.findAll());
        setAddOperation(reason -> doSave(reason));
        setUpdateOperation(reason -> doUpdate(reason));
        setDeleteOperation(reason -> doDelete(reason));
    }

    private void doDelete(AdjustmentReason reason) {
        try {
            reasonService.delete(reason);
            showSuccess("Deleted reason successful");
        } catch (Exception e) {
            showError("Failed to delete reason", e);
        }
    }

    private AdjustmentReason doUpdate(AdjustmentReason reason) {
        try {
            reason = reasonService.update(reason);
            showSuccess("Updated reason successful");
            return reason;
        } catch (Exception e) {
            showError("Failed to update reason", e);
            return null;
        }
    }

    private AdjustmentReason doSave(AdjustmentReason reason) {
        try {
            reason = reasonService.create(reason);
            showSuccess("Added new reason successful");
            return reason;
        } catch (Exception e) {
            showError("Failed to add new reason", e);
            return null;
        }
    }
}
