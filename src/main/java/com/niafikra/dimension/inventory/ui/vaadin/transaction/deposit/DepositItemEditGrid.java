package com.niafikra.dimension.inventory.ui.vaadin.transaction.deposit;

import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.core.ui.vaadin.util.MoneyField;
import com.niafikra.dimension.core.ui.vaadin.util.grid.ItemEditGrid;
import com.niafikra.dimension.core.util.PrototypeScope;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.service.BatchService;
import com.niafikra.dimension.inventory.ui.vaadin.QuantityField;
import com.niafikra.dimension.inventory.ui.vaadin.item.ItemComboBox;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.DateField;
import com.vaadin.ui.TextArea;
import org.springframework.context.ApplicationContext;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Stream;

@SpringComponent
@PrototypeScope
public class DepositItemEditGrid extends ItemEditGrid<ItemBatchDepositData> {
    @Inject
    private ApplicationContext context;
    @Inject
    private BatchService batchService;

    public DepositItemEditGrid() {
        super(ItemBatchDepositData.class);
    }

    @PostConstruct
    private void build() {
        setBodyRowHeight(90);

        addComponentColumn(key -> {
            ItemComboBox itemSelector = context.getBean(ItemComboBox.class);
            itemSelector.setPlaceholder("Select Item");
            itemSelector.setWidth("100%");

            getBinder(key).removeBinding("item");
            getBinder(key).forField(itemSelector).asRequired("Item must be selected").bind("item");
            itemSelector.setRequiredIndicatorVisible(false);

            MComboBox<String> batchSelector = new MComboBox<String>()
                    .withFullWidth();
            batchSelector.setPlaceholder("Enter the batch code");

            Item bindedDataItem = getBinder(key).getBean().getItem();
            batchSelector.setEnabled(bindedDataItem != null);

            getBinder(key).removeBinding("batchCode");
            getBinder(key).forField(batchSelector).asRequired("Batch must be selected").bind("batchCode");
            batchSelector.setRequiredIndicatorVisible(false);

            getBinder(key).addValueChangeListener(e -> {
                if (e.getSource().equals(itemSelector)) {
                    batchSelector.clear();
                    itemSelector.getOptionalValue().ifPresentOrElse(
                            selectedItem -> {
                                batchSelector.setEnabled(true);
                                batchSelector.setItems(batchService.findBatchCodes(selectedItem));
                            },
                            () -> {
                                batchSelector.setEnabled(false);
                                batchSelector.setItems(Collections.EMPTY_LIST);
                            }
                    );
                }
            });

            batchSelector.setNewItemProvider(newBatchCode -> {
                ListDataProvider<String> dataProvider = (ListDataProvider<String>) batchSelector.getDataProvider();
                batchSelector.setItems(Stream.concat(Stream.of(newBatchCode), dataProvider.getItems().stream()));
                return Optional.of(newBatchCode);
            });

            DateField expiryDateField = new DateField();
            expiryDateField.setPlaceholder("ExpiryDate");
            expiryDateField.setWidth("100%");

            getBinder(key).removeBinding("expiryDate");
            getBinder(key).forField(expiryDateField).bind("expiryDate");

            return new MVerticalLayout(
                    itemSelector,
                    new MHorizontalLayout(batchSelector, expiryDateField).withFullWidth().withMargin(false)
            ).withMargin(false);

        }).setCaption("Item").setId("item").setSortable(false);

        addComponentColumn(key -> {
            QuantityField quantityField = new QuantityField()
                    .withFullWidth();
            quantityField.getQuantity().setDescription("Enter quantity");

            Item bindedDataItem = getBinder(key).getBean().getItem();
            quantityField.setEnabled(bindedDataItem != null);

            getBinder(key).removeBinding("quantity");
            getBinder(key).forField(quantityField).asRequired("Quantity must be specified").bind("quantity");
            quantityField.setRequiredIndicatorVisible(false);

            getBinder(key).addValueChangeListener(e -> {
                if (ItemComboBox.class.isInstance(e.getSource())) {
                    quantityField.clear();
                    e.getSource().getOptionalValue().ifPresentOrElse(
                            selectedItem -> {
                                quantityField.setValue(((Item) selectedItem).getBaseUnit().toZeroQuantity());
                                quantityField.setEnabled(true);
                            },
                            () -> {
                                quantityField.clear();
                                quantityField.setEnabled(false);
                            }
                    );
                }
            });

            MoneyField buyingPriceField = new MoneyField()
                    .withDescription("Enter specified unit buying price")
                    .withFullWidth();

            getBinder(key).removeBinding("unitBuyingPrice");
            getBinder(key).forField(buyingPriceField).asRequired("Buying price must be specified").bind("unitBuyingPrice");
            buyingPriceField.setRequiredIndicatorVisible(false);

            return new MVerticalLayout(quantityField, buyingPriceField).withMargin(false);

        }).setCaption("Quantity").setId("quantity").setSortable(false);

        addComponentColumn(key -> {
            TextArea notesField = new TextArea();
            notesField.setWidthFull();
            notesField.setRows(4);
            notesField.setPlaceholder("Enter notes for this deposit transaction");

            getBinder(key).removeBinding("notes");
            getBinder(key).forField(notesField).bind("notes");

            return notesField;
        }).setCaption("Notes").setId("notes").setSortable(false);
    }
}
