package com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;

import static com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory.InventoryRequisitionView.VIEW_NAME;


@RequiredArgsConstructor
@Secured(Permission.VIEW_INVENTORY_REQUISITION)
@ViewComponent(value = MainDisplay.class, caption = "Inventory Requisition")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class InventoryRequisitionView extends VerticalLayout implements View {
    public static final String VIEW_NAME = "inventory-requisition";

}
