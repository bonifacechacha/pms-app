package com.niafikra.dimension.inventory.ui.vaadin;

import com.niafikra.dimension.core.ui.vaadin.util.IntegerToBigIntegerConverter;
import com.niafikra.dimension.inventory.domain.Quantity;
import com.niafikra.dimension.inventory.domain.unit.ItemUnit;
import com.vaadin.data.BeanValidationBinder;
import com.vaadin.data.Binder;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import lombok.Getter;
import org.vaadin.viritin.fields.IntegerField;
import org.vaadin.viritin.fluency.ui.FluentAbstractComponent;
import org.vaadin.viritin.layouts.MHorizontalLayout;


public class QuantityField extends CustomField<Quantity> implements FluentAbstractComponent<QuantityField> {
    BeanValidationBinder<Quantity> binder = new BeanValidationBinder<>(Quantity.class);
    private Quantity oldValue;
    @Getter
    private IntegerField quantity = new IntegerField();
    @Getter
    private ComboBox<ItemUnit> unit = new ComboBox<>();

    public QuantityField() {
        binder.setRequiredConfigurator(null);
        binder.forField(quantity)
                .withConverter(new IntegerToBigIntegerConverter())
                .bind("quantity");

        binder.forField(unit)
                .bind("unit");

        unit.setWidth("80px");
        quantity.setWidth("100%");

        quantity.addValueChangeListener(event -> fireValueChangeEvent(event.isUserOriginated()));
        unit.addValueChangeListener(event -> fireValueChangeEvent(event.isUserOriginated()));
    }

    public QuantityField(String caption) {
        this();
        setCaption(caption);
    }

    @Override
    protected Component initContent() {
        return new MHorizontalLayout(quantity, unit).withExpand(quantity, 1).withFullWidth();
    }

    @Override
    protected void doSetValue(Quantity value) {
        binder.setBean(value);

        if(value!=null) {
            if (value.isInitialised()) unit.setItems(value.getItem().getUnits());
        }else{
            quantity.clear();
            unit.clear();
        }

        oldValue = value;
    }

    @Override
    public Quantity getValue() {
        return binder.getBean();
    }

    private void fireValueChangeEvent(boolean userOriginated) {
        fireEvent(new ValueChangeEvent<>(this, oldValue, userOriginated));
        oldValue = getValue();
    }

    public Binder<Quantity> getBinder() {
        return binder;
    }
}