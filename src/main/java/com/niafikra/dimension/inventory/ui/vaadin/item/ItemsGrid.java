package com.niafikra.dimension.inventory.ui.vaadin.item;

import com.niafikra.dimension.category.service.CategoryService;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.service.ItemService;
import com.niafikra.dimension.inventory.ui.vaadin.item.batch.ItemBatchesView;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.HeaderRow;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;

import static com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils.createFilteringComboBox;
import static com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils.createFilteringTextField;

@SpringComponent
@PrototypeScope
public class ItemsGrid extends Grid<Item> {
    private ItemsProvider itemsProvider;
    private Navigator navigator;
    private ItemService itemService;
    private CategoryService categoryService;

    public ItemsGrid(Navigator navigator, ItemsProvider itemsProvider, ItemService itemService, CategoryService categoryService) {
        super(itemsProvider);
        this.navigator = navigator;
        this.itemService = itemService;
        this.categoryService = categoryService;
        this.itemsProvider = itemsProvider;
    }

    @PostConstruct
    private void build() {
        addColumn(item -> item.getCode())
                .setId("code")
                .setCaption("Code")
                .setSortable(true)
                .setWidth(100);

        addColumn(item -> item.getName())
                .setId("name")
                .setCaption("Item")
                .setSortable(true);

        addColumn(item -> item.getCategory())
                .setId("category")
                .setCaption("Category")
                .setSortable(true);

        addColumn(item -> item.getBaseUnit())
                .setId("baseUnit")
                .setCaption("Base Unit")
                .setWidth(100);

        addColumn(item -> itemService.calculateStockBalance(item))
                .setId("stock")
                .setCaption("Stock")
                .setWidth(100);

        HeaderRow filterRow = appendHeaderRow();

        createFilteringTextField(
                filterRow,
                "code",
                "Filter by item code",
                event -> itemsProvider.setCodeFilter(event.getValue())
        );
        createFilteringTextField(
                filterRow,
                "name",
                "Filter by item name",
                event -> itemsProvider.setNameFilter(event.getValue())
        );
        createFilteringComboBox(
                filterRow,
                "category",
                "Filter by category",
                categoryService.getAll(Item.CATEGORY_TYPE),
                event -> itemsProvider.setCategory(event.getValue())
        );

        setSelectionMode(SelectionMode.NONE);
        addItemClickListener(event -> {
            Item item = event.getItem();
            navigator.navigateTo(ItemBatchesView.class, item.getId());
        });
    }
}
