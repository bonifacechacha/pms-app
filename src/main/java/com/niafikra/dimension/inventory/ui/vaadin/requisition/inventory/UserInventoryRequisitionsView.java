package com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;
import javax.annotation.PostConstruct;
import static com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory.UserInventoryRequisitionsView.VIEW_NAME;


@RequiredArgsConstructor
@Secured(Permission.VIEW_USER_INVENTORY_REQUISITION)
@ViewComponent(value = MainDisplay.class, caption = "My Inventory Requests")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.INVENTORY, caption = "My Inventory Requests")
@VaadinFontIcon(VaadinIcons.LIST_OL)
public class UserInventoryRequisitionsView extends VerticalLayout implements View {
    public static final String VIEW_NAME = "user-inventory-requisitions";

    private final Navigator navigator;
    private final InventoryRequisitionsGrid requisitionsGrid;

    @PostConstruct
    private void build(){
        setSizeFull();
        setMargin(false);

        requisitionsGrid.setSizeFull();
        addComponent(requisitionsGrid);
    }

    @Secured(Permission.CREATE_INVENTORY_REQUISITION)
    @ViewMenuOption(value = "New Requisition", icon = VaadinIcons.PLUS_CIRCLE)
    public void createInventoryRequisition() {
        navigator.navigateTo(NewInventoryRequisitionView.class);
    }


}
