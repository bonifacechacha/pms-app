package com.niafikra.dimension.core.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/29/17 6:51 PM
 */

@Controller
public class MainController {

    @GetMapping(path = "/login")
    String login(HttpServletRequest request){
        return "login";
    }

    @GetMapping(path = "/logout")
    String logout(HttpServletRequest request) throws ServletException {
        request.logout();
        return "redirect:/app";
    }

    @GetMapping(path = "/")
    String home(HttpServletRequest request) {
        return "redirect:/app";
    }

    @RequestMapping(path = "/vaadinServlet/HEARTBEAT")
    String heatBeatFailure(HttpServletRequest request) {
        return "redirect:/app";
    }
}
