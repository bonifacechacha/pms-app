package com.niafikra.dimension.core.ui.vaadin.util;

import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import org.apache.http.client.utils.CloneUtils;
import org.vaadin.crudui.crud.CrudListener;
import org.vaadin.crudui.crud.impl.GridCrud;
import org.vaadin.crudui.layout.CrudLayout;

import java.util.Collection;

public class GridCrudField<T, V extends Collection<T>> extends CustomField<V> implements CrudListener<T> {

    private V entities;
    private GridCrud<T> gridCrud;

    private Class<T> type;

    public GridCrudField(Class<T> type) {
        this.type = type;
        gridCrud = new GridCrud<T>(type);
        gridCrud.setCrudListener(this);
    }

    public GridCrudField(Class<T> type, CrudLayout layout) {
        this.type = type;
        gridCrud = new GridCrud<T>(type,layout);
        gridCrud.setCrudListener(this);
    }

    public Class<T> getType() {
        return type;
    }

    public void setEntities(V entities) {
        this.entities = entities;
        gridCrud.refreshGrid();
    }

    @Override
    public Collection<T> findAll() {
        return entities;
    }

    @Override
    public T add(T entity) {
        V oldValue = cloneCurrentValue();
        entities.add(entity);
        fireValueChange(oldValue, true);
        return entity;
    }

    private V cloneCurrentValue() {
        try {
            return CloneUtils.cloneObject(entities);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void fireValueChange(V entities, boolean userOriginated) {
        fireEvent(new ValueChangeEvent<V>(this, entities, userOriginated));
    }

    @Override
    public T update(T entity) {
        fireValueChange(entities, true);
        return entity;
    }

    @Override
    public void delete(T entity) {
        V oldValue = cloneCurrentValue();
        entities.remove(entity);
        fireValueChange(oldValue, true);
    }

    @Override
    protected Component initContent() {
        return gridCrud;
    }

    public GridCrud<T> getGridCrud() {
        return gridCrud;
    }

    @Override
    protected void doSetValue(V value) {
        entities = value;
        gridCrud.refreshGrid();
    }

    @Override
    public V getValue() {
        return entities;
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        super.setReadOnly(readOnly);
        gridCrud.setEnabled(!readOnly);
    }

    @Override
    public void setSizeFull() {
        super.setSizeFull();
        getGridCrud().setSizeFull();
    }
}
