package com.niafikra.dimension.core.ui.vaadin.user.group.delegation;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.security.service.UserService;
import com.niafikra.dimension.core.ui.vaadin.util.AbstractForm;
import com.niafikra.dimension.delegation.domain.Delegation;
import com.niafikra.dimension.delegation.service.DelegationService;
import com.niafikra.dimension.group.domain.Group;
import com.niafikra.dimension.group.service.GroupService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.Window;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MFormLayout;
import org.vaadin.viritin.layouts.MHorizontalLayout;

import static com.niafikra.dimension.core.security.SecurityUtils.hasAuthority;

@PrototypeScope
@SpringComponent
public class DelegationRequestForm extends AbstractForm<Delegation> {

    public static final String FIELD_WIDTH = "400px";
    private ComboBox<User> delegator = new ComboBox<>("Delegator");
    private ComboBox<Group> group = new ComboBox<>("Group");
    private ComboBox<User> delegatee = new ComboBox<>("Delegate to");
    private DateField startDate = new DateField("Start");
    private DateField endDate = new DateField("End");
    private TextArea reason = new TextArea("Reason");
    private DelegationService delegationService;
    private GroupService groupService;
    private UserService userService;
    private User user;

    public DelegationRequestForm(DelegationService delegationService,
                                 GroupService groupService,
                                 UserService userService,
                                 User user) {
        super(Delegation.class);
        this.delegationService = delegationService;
        this.groupService = groupService;
        this.userService = userService;
        this.user = user;

        delegator.setWidth(FIELD_WIDTH);
        delegator.setItems(userService.findAll(UserService.UserFilter.builder().build()));
        delegator.addValueChangeListener(event -> {
            group.setItems(groupService.getGroups(event.getValue()));
        });

        group.setWidth(FIELD_WIDTH);

        delegatee.setWidth(FIELD_WIDTH);
        delegatee.setItems(userService.findAll(UserService.UserFilter.builder().build()));

        startDate.setWidth("100%");
        endDate.setWidth("100%");

        reason.setWidth(FIELD_WIDTH);
        reason.setRows(4);

        addSavedHandler(delegation -> doSave(delegation));
        getSaveButton().setCaption("Submit");
    }


    @Override
    public void setEntity(Delegation delegation) {
        if (delegation.getDelegator() == null)
            delegation.setDelegator(user);

        super.setEntity(delegation);
        delegator.setEnabled(hasAuthority(Permission.REQUEST_DELEGATION_FOR_OTHERS));
    }

    private void doSave(Delegation delegation) {
        try {
            delegationService.request(delegation,user);
            showSuccess("Successful submitted a delegation request");
            closePopup();
        } catch (Exception e) {
            showError("Failed to request delegation", e);
        }
    }

    @Override
    protected MFormLayout createContent() {
        return new MFormLayout(
                delegator,
                group,
                delegatee,
                new MHorizontalLayout(startDate, endDate)
                        .withCaption("Period")
                        .withWidth(FIELD_WIDTH),
                reason,
                getToolbar()
        ).withMargin(true);
    }

    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setWidth("650px");
        window.setCaption("Create delegation request");
        return window;
    }
}
