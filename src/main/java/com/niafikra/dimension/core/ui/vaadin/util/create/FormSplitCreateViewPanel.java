package com.niafikra.dimension.core.ui.vaadin.util.create;

import com.niafikra.dimension.core.ui.vaadin.util.AbstractForm;
import com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/6/17 8:06 PM
 */
public abstract class FormSplitCreateViewPanel<T> extends SplitCreateViewPanel<T> {

    private AbstractForm<T> form;

    public FormSplitCreateViewPanel(Grid<T> entriesGrid, AbstractForm<T> form) {
        super(entriesGrid);
        this.form = form;
        this.form.addSavedHandler(t -> resetView());
        VaadinUtils.addRemoveButton(getEntriesGrid(), this::onDelete);
    }

    @Override
    protected Component getCreateComponent() {
        form.setEntity(createNewEntity());
        return form;
    }

    protected abstract T createNewEntity();

    protected abstract void onDelete(T entity);

    @Override
    protected Component getViewComponent(T entry) {
        form.setEntity(entry);
        return form;
    }
}
