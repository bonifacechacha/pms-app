package com.niafikra.dimension.core.ui.vaadin.util.menu;

import com.vaadin.server.Resource;
import com.vaadin.ui.MenuBar;

import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 10/1/17 7:43 PM
 */
public class MMenuBar extends MenuBar {

    public MenuItem addItemAsFirst(String caption, Resource icon, Command command) {
        Optional<MenuItem> firstMenuItem = getItems().stream().findFirst();
        if (firstMenuItem.isPresent())
            return addItemBefore(caption, icon, command, firstMenuItem.get());
        else return addItem(caption, icon, command);
    }

    public boolean contains(String menu) {
        return getItems().stream().anyMatch(menuItem -> menuItem.getText().equals(menu));
    }

    public boolean containsRecursive(String menu) {
        return getItems().stream().anyMatch(menuItem -> containsRecursive(menuItem, menu));
    }

    private boolean containsRecursive(MenuItem menuItem, String menu) {
        if (menuItem.getText().equals(menu)) return true;
        return menuItem.getChildren().stream().anyMatch(item -> containsRecursive(item, menu));
    }

    private boolean contains(MenuItem menuItem, String menu) {
        return menuItem.getChildren().stream().anyMatch(item -> item.getText().equals(menu));
    }

    public Stream<MenuItem> findMenuItems(String menu) {
        return getItems()
                .stream()
                .flatMap(item -> getWithChildrenRecursive(item))
                .filter(menuItem -> menuItem.getText().equals(menu));
    }

    private Stream<MenuItem> getWithChildrenRecursive(MenuItem item) {
        if (!item.hasChildren()) return Stream.of(item);
        return Stream.concat(Stream.of(item), item.getChildren().stream().flatMap(it -> getWithChildrenRecursive(it)));
    }

    public void setMenuVisible(String menu, Supplier<Boolean> visibility) {
        setMenuVisible(menu, visibility.get());
    }

    public void setMenuVisible(String menu, boolean visibility) {
        findMenuItems(menu).forEach(menuItem -> menuItem.setVisible(visibility));
    }

    public void setMenuVisible(Set<String> menus, Supplier<Boolean> visibility) {
        menus.forEach(menu->setMenuVisible(menu,visibility));
    }

    public void setMenuVisible(Set<String> menus, boolean visibility) {
        menus.forEach(menu->setMenuVisible(menu,visibility));
    }
}
