package com.niafikra.dimension.core.ui.vaadin.util;

import com.niafikra.dimension.core.util.HasLogger;
import com.vaadin.server.Page;
import com.vaadin.ui.Notification;
import com.vaadin.ui.themes.ValoTheme;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/21/17 12:35 PM
 */
public interface Notifier extends HasLogger {

    default void show(String message, String description, Notification.Type type, String... styles) {
        Notification notification = new Notification(message, description, type, true);
//        notification.setDelayMsec(-1);
        notification.setStyleName(StringUtils.join(styles, " "));
        notification.show(Page.getCurrent());
    }

    default void showWarning(String message, String description) {
        show(message, description, Notification.Type.WARNING_MESSAGE, ValoTheme.NOTIFICATION_WARNING, ValoTheme.NOTIFICATION_CLOSABLE);
        getLogger().warn(message + " : " + description);
    }

    default void showWarning(String message) {
        showWarning(message, null);
    }

    default void showSuccess(String message, String description) {
        show(message, description, Notification.Type.HUMANIZED_MESSAGE, ValoTheme.NOTIFICATION_CLOSABLE);
        getLogger().info(message + " : " + description);
    }

    default void showSuccess(String message) {
        showSuccess(message, null);
    }

    default void showError(String message, String description) {
        show(message, description, Notification.Type.ERROR_MESSAGE, ValoTheme.NOTIFICATION_ERROR, ValoTheme.NOTIFICATION_CLOSABLE);
        getLogger().error(message + " : " + description);
    }

    default void showError(String message) {
        showError(message, (String) null);
    }

    default void showError(String message, Throwable throwable) {
        showError(message, throwable.getLocalizedMessage());
        getLogger().error(message, throwable);
    }
}
