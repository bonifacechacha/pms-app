package com.niafikra.dimension.core.ui.vaadin.actions;

import com.vaadin.addon.tableexport.TableExport;

public interface TableExportable {

    TableExport getTableExport();

}
