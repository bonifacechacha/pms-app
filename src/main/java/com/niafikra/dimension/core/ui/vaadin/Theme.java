package com.niafikra.dimension.core.ui.vaadin;

import com.vaadin.ui.themes.ValoTheme;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/19/17 1:07 AM
 */
public class Theme extends ValoTheme {
    public static final String THEME_NAME = "dimension";
    public static final String TEXT_SMALL = "text-small";
    public static final String TEXT_RIGHT = "text-right";
    public static final String LABEL_SUMMARY = "summary-label";
    public static final String GRID_ROW_RESOURCE = "resource-row";
    public static final String GRID_ROW_SECTION = "section-row";
    public static final String GRID_ROW_ROLE = "role-row";
    public static final String GRID_NO_STRIPES = "no-stripes";
    public static final String LONG_TEXT = "long-text";
    public static final String SUMMARY_CARD = "summary-card";
    public static final String FONT_COLOR_WARNING = "font-color-warning";
    public static final String BUTTON_WARNING = "warning";
    public static final String FONT_SIZE_HUGE = "font-size-huge";
    public static final String GRID_TEXT_SMALL = "text-small";
    public static final String TEXT_DANGER = "text-danger";
    public static final String TEXT_SUCCESS = "text-success";
    public static final String TEXT_WARNING = "text-warning";
    public static final String PICTURE_FIELD = "picture-field";
    public static final String GRID_LIGHT = "light-grid";
}