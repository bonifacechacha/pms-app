package com.niafikra.dimension.core.ui.vaadin.util;

import com.vaadin.server.StreamResource;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public interface WorkbookSource extends StreamResource.StreamSource {

    default InputStream getStream() {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Workbook workbook = getWorkbook();
            workbook.write(bos);
            return new ByteArrayInputStream(bos.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    Workbook getWorkbook();
}
