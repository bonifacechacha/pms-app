package com.niafikra.dimension.core.ui.vaadin.util;

import com.niafikra.dimension.core.domain.Printable;
import com.vaadin.icons.VaadinIcons;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 10/3/17 5:42 PM
 */
public class PrintButton extends OpenerButton {

    public PrintButton() {
        super("Print", VaadinIcons.PRINT);
    }

    public PrintButton(Printable printable) {
        super("Print", VaadinIcons.PRINT);
        setPrintable(printable);
    }

    public void setPrintable(Printable printable) {
        getOpener().setUrl(printable.getPrintURL());
    }
}
