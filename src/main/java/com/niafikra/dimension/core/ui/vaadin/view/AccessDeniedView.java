package com.niafikra.dimension.core.ui.vaadin.view;

import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Alignment;
import org.vaadin.viritin.label.MLabel;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;

@UIScope
@SpringView
@ViewComponent(value = MainDisplay.class, caption = "Access denied")
public class AccessDeniedView extends MVerticalLayout implements View {

    @PostConstruct
    public void build() {
        setSizeFull();

        MVerticalLayout content = new MVerticalLayout(
                new MLabel("403")
                        .withStyleName(Theme.FONT_SIZE_HUGE, Theme.LABEL_LIGHT),
                new MLabel("You do not have permission to view this page.\n Kindly contact system administrator")
                        .withStyleName(Theme.LABEL_COLORED, Theme.LABEL_H1).withFullWidth()

        ).withUndefinedWidth();

        add(content);
        setComponentAlignment(content, Alignment.MIDDLE_CENTER);
    }
}
