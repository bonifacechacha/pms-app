package com.niafikra.dimension.core.ui.vaadin.exchange;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingDisplay;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingView;
import com.niafikra.dimension.core.ui.vaadin.util.CurrencySelector;
import com.niafikra.dimension.core.ui.vaadin.util.MGridCrudView;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.niafikra.dimension.money.LocalExchange;
import com.niafikra.dimension.money.LocalExchangeService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.crudui.layout.impl.HorizontalSplitCrudLayout;

import javax.annotation.PostConstruct;

import static com.niafikra.dimension.core.ui.vaadin.exchange.LocalExchangeCRUD.VIEW_NAME;

@Secured({Permission.CONFIGURE_EXCHANGE_RATES})
@ViewComponent(value = SettingDisplay.class, caption = "Exchange rates")
@ViewInfo(value = "Exchange Rates", section = "Finance", icon = VaadinIcons.COIN_PILES)
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class LocalExchangeCRUD extends MGridCrudView<LocalExchange> implements SettingView, Notifier {
    public static final String VIEW_NAME = "local-exchange";

    public static final String[] VISIBLE_FIELDS = {"date", "source", "target", "rate"};

    private LocalExchangeService localExchangeService;

    public LocalExchangeCRUD(LocalExchangeService localExchangeService) {
        super(LocalExchange.class, new HorizontalSplitCrudLayout());
        this.localExchangeService = localExchangeService;
    }

    @PostConstruct
    private void build() {
        getGrid().setColumns(VISIBLE_FIELDS);
        getCrudFormFactory().setVisibleProperties(VISIBLE_FIELDS);
        getCrudFormFactory().setUseBeanValidation(true);
        getCrudFormFactory().setFieldProvider("source", () -> new CurrencySelector());
        getCrudFormFactory().setFieldProvider("target", () -> new CurrencySelector());

        setFindAllOperation(() -> localExchangeService.findAll());
        setAddOperation(exchange -> doSave(exchange));
        setUpdateOperation(exchange -> doUpdate(exchange));
        setDeleteOperation(exchange -> doDelete(exchange));
    }

    private void doDelete(LocalExchange exchange) {
        try {
            localExchangeService.delete(exchange);
            showSuccess("Deleted exchange rate successful");
        } catch (Exception e) {
            showError("Failed to delete exchange rate", e);
        }
    }

    private LocalExchange doUpdate(LocalExchange exchange) {
        try {
            exchange = localExchangeService.update(exchange);
            showSuccess("Updated exchange rate successful");
            return exchange;
        } catch (Exception e) {
            showError("Failed to update exchange rate", e);
            return null;
        }
    }

    private LocalExchange doSave(LocalExchange exchange) {
        try {
            exchange = localExchangeService.create(exchange);
            showSuccess("Added new exchange rate successful");
            return exchange;
        } catch (Exception e) {
            showError("Failed to add new exchange rate", e);
            return null;
        }
    }
}
