package com.niafikra.dimension.core.ui.vaadin.util.upload;

import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.util.SubWindow;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Upload;
import org.vaadin.viritin.MSize;
import org.vaadin.viritin.layouts.MVerticalLayout;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/20/16
 */
public class UploadWindow extends SubWindow implements Notifier {
    private List<UploadListener> listeners = new LinkedList<>();
    private Upload upload;
    private Upload.SucceededEvent recentSucceededEvent;

    public UploadWindow(String destinationDir) {
        withSize(MSize.size("60%", "50%"));
        upload = new Upload("", new UploadReceiver(destinationDir));

        build();
    }

    private void build() {
        upload.setButtonCaption("Select file to upload ...");
        upload.addFailedListener(event -> {
            showError("Failed to upload file", event.getReason().getMessage());
        });

        upload.addSucceededListener(event -> {
            recentSucceededEvent = event;
            String tmpFile = new StringBuilder().append(System.getProperty("java.io.tmpdir")).append(File.separator).append(event.getFilename()).toString();
            notifyListeners(tmpFile);
            close();
        });

        withContent(
                new MVerticalLayout(upload)
                        .withFullSize()
                        .withAlign(upload, Alignment.MIDDLE_CENTER)
        );

    }


    public Upload getUpload() {
        return upload;
    }

    public Upload.SucceededEvent getRecentSucceededEvent() {
        return recentSucceededEvent;
    }

    private void notifyListeners(String tmpFile) {
        for (UploadWindow.UploadListener lister : listeners)
            lister.uploadSucceeded(new File(tmpFile));
    }

    public void addUploadListener(UploadWindow.UploadListener listener) {
        listeners.add(listener);
    }

    /**
     * @author Boniface Chacha <boniface.chacha@niafikra.com,bonifacechacha@gmail.com>
     * Date: 1/13/14
     * Time: 2:46 AM
     */
    public interface UploadListener {
        void uploadSucceeded(File file);
    }
}
