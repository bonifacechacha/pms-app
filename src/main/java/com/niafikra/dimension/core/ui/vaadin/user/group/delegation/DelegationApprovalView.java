package com.niafikra.dimension.core.ui.vaadin.user.group.delegation;

import com.niafikra.dimension.approval.domain.Tracker;
import com.niafikra.dimension.approval.service.ApprovalTrackerService;
import com.niafikra.dimension.approval.ui.vaadin.tracker.ApprovalDisplay;
import com.niafikra.dimension.approval.ui.vaadin.tracker.ApprovalView;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.delegation.domain.Delegation;
import com.niafikra.dimension.delegation.service.DelegationService;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.viritin.layouts.MVerticalLayout;

import static com.niafikra.dimension.Permission.VIEW_DELEGATION_APPROVAL_REQUEST;
import static com.niafikra.dimension.core.ui.vaadin.user.group.delegation.DelegationApprovalView.VIEW_NAME;


/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/24/17 7:02 PM
 */

@Secured(VIEW_DELEGATION_APPROVAL_REQUEST)
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@ViewComponent(value = ApprovalDisplay.class, caption = "Delegation approval")
public class DelegationApprovalView extends MVerticalLayout implements ApprovalView {
    public static final String VIEW_NAME = "ap-delegation-approval";
    private DelegationTemplatePanel delegationPanel;
    private DelegationService delegationService;
    private ApprovalTrackerService trackerService;

    public DelegationApprovalView(DelegationTemplatePanel delegationPanel,
                                  DelegationService delegationService,
                                  ApprovalTrackerService trackerService) {
        this.delegationPanel = delegationPanel;
        this.delegationService = delegationService;
        this.trackerService = trackerService;

        setSizeFull();
        setMargin(false);
        delegationPanel.setSizeFull();
        addComponent(delegationPanel);
        setExpandRatio(delegationPanel, 1);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long trackerId = Long.parseLong(event.getParameters());
        Tracker tracker = trackerService.getTracker(trackerId);
        Delegation delegation = delegationService.getDelegation(tracker);
        delegationPanel.setDelegation(delegation);
    }
}
