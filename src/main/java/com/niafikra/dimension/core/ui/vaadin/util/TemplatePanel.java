package com.niafikra.dimension.core.ui.vaadin.util;

import com.niafikra.dimension.core.ui.vaadin.Theme;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MPanel;
import org.vaadin.viritin.layouts.MVerticalLayout;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 5/4/17 8:32 PM
 */
@Component
@PrototypeScope
public final class TemplatePanel extends MPanel implements Cloneable {

    private TemplateView templateView;

    public TemplatePanel(TemplateView templateView) {
        this.templateView = templateView;
        templateView.setSizeFull();
        setContent(new MVerticalLayout(templateView).withMargin(true));
        addStyleName(Theme.PANEL_BORDERLESS);
    }

    public TemplateView getTemplateView() {
        return templateView;
    }
}