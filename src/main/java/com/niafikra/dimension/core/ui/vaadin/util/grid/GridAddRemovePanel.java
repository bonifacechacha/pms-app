package com.niafikra.dimension.core.ui.vaadin.util.grid;

import com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/7/17 5:47 PM
 */
public class GridAddRemovePanel<T> extends VerticalLayout {

    private Grid<T> grid;
    private ComboBox<T> selector = new ComboBox<>();

    private List<AddRemoveListener> listeners = new LinkedList<>();

    public GridAddRemovePanel() {
        this(new Grid<>(DataProvider.ofCollection(new LinkedList<>())));
    }

    public GridAddRemovePanel(Grid<T> grid) {
        this.grid = grid;

        setMargin(false);
        grid.setSizeFull();
        addComponent(grid);
        setExpandRatio(grid, 1);
        VaadinUtils.addRemoveButton(grid, entry -> removeItem(entry));

        selector.setWidth("100%");
        addComponent(selector);
        selector.addValueChangeListener(event -> {
            T selected = event.getValue();
            //if selected null then ignore
            if (selected == null) return;

            listeners.forEach(listener -> listener.onAdd(selected));
            getGridDataProvider().getItems().add(selected);
            getGridDataProvider().refreshAll();
            selector.setValue(null);//reset selection to allow choosing another
        });
    }

    private void removeItem(T entry) {
        listeners.forEach(listener -> listener.onRemove(entry));
        getGridDataProvider().getItems().remove(entry);
        getGridDataProvider().refreshAll();
    }

    public ListDataProvider<T> getGridDataProvider() {
        return (ListDataProvider<T>) grid.getDataProvider();
    }

    public Grid<T> getGrid() {
        return grid;
    }

    public ComboBox<T> getSelector() {
        return selector;
    }

    public void addAddRemoveListener(AddRemoveListener<T> listener) {
        listeners.add(listener);
    }

    public void removeAddRemoveListener(AddRemoveListener<T> listener) {
        listeners.remove(listener);
    }


    public interface AddRemoveListener<T> {
        void onAdd(T entry);

        void onRemove(T entry);
    }
}
