package com.niafikra.dimension.core.ui.vaadin.user.role;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.security.domain.Role;
import com.niafikra.dimension.core.security.service.RoleService;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingDisplay;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingView;
import com.niafikra.dimension.core.ui.vaadin.util.MGridCrudView;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.vaadin.data.HasValue;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TwinColSelect;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.crudui.layout.impl.HorizontalSplitCrudLayout;

import javax.annotation.PostConstruct;
import java.util.Comparator;
import java.util.stream.Collectors;

import static com.niafikra.dimension.core.ui.vaadin.user.role.RoleCrudView.VIEW_NAME;


@Secured({Permission.CONFIGURE_USER_ROLES})
@ViewComponent(value = SettingDisplay.class, caption = "User Roles")
@ViewInfo(value = "User Roles", section = "Security", icon = VaadinIcons.USER_CHECK)
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class RoleCrudView extends MGridCrudView<Role> implements SettingView, Notifier {

    public static final String VIEW_NAME = "user-roles";

    public static final String[] GRID_VISIBLE_FIELDS = {"name", "parent", "permissionCount"};
    public static final String[] FORM_VISIBLE_FIELDS = {"name", "parent", "permissions"};

    private RoleService roleService;

    public RoleCrudView(RoleService roleService) {
        super(Role.class, new HorizontalSplitCrudLayout());
        this.roleService = roleService;
    }

    @PostConstruct
    private void build() {
        getGrid().addColumn(role -> role.getPermissionRecursive().size())
                .setId("permissionCount")
                .setCaption("Permissions")
                .setSortable(false);
        getGrid().setColumns(GRID_VISIBLE_FIELDS);
        getCrudFormFactory().setVisibleProperties(FORM_VISIBLE_FIELDS);
        getCrudFormFactory().setUseBeanValidation(true);

        getCrudFormFactory().setFieldProvider("parent", () -> createRoleSelector());
        getCrudFormFactory().setFieldProvider("permissions", () -> createPermissionsSelector());

        setFindAllOperation(() -> roleService.findAll(RoleService.RoleFilter.builder().build()));
        setAddOperation(role -> doCreate(role));
        setUpdateOperation(role -> doUpdate(role));
        setDeleteOperation(role -> doDelete(role));
    }

    private TwinColSelect<String> createPermissionsSelector() {
        TwinColSelect<String> selector =
                new TwinColSelect<>(
                        "Permissions",
                        Role.getAllPermissions().
                                stream()
                                .sorted()
                                .collect(Collectors.toList())
                );

        selector.setRows(20);
        return selector;
    }

    private HasValue<Role> createRoleSelector() {
        ComboBox<Role> selector = new ComboBox<Role>(
                "Parent",
                roleService.findAll(RoleService.RoleFilter.builder().build())
                        .stream()
                        .sorted(Comparator.comparing(Role::getName))
                        .collect(Collectors.toList())
        );
        return selector;
    }

    private void doDelete(Role role) {
        try {
            roleService.delete(role);
            showSuccess("Successful deleted role", role.toString());
        } catch (Exception e) {
            showError("Failed to delete role", e);
        }
    }

    private Role doUpdate(Role role) {
        try {
            role = roleService.update(role);
            showSuccess("Successful updated role", role.toString());
            return role;
        } catch (Exception e) {
            showError("Failed to update role", e);
            return null;
        }
    }

    private Role doCreate(Role role) {
        try {
            role = roleService.create(role);
            showSuccess("Successful created role", role.toString());
            return role;
        } catch (Exception e) {
            showError("Failed to create role", e);
            return null;
        }
    }
}
