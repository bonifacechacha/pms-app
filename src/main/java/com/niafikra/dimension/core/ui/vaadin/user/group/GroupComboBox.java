package com.niafikra.dimension.core.ui.vaadin.user.group;


import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.group.domain.Group;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;

@Component
@PrototypeScope
public class GroupComboBox extends MComboBox<Group> {

    public GroupComboBox(GroupProvider provider) {
        setDataProvider(provider);
    }
}