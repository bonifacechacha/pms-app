package com.niafikra.dimension.core.ui.vaadin.view;

import com.vaadin.icons.VaadinIcons;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
@Target({METHOD})
@Documented
public @interface ViewMenuOption {

    String value();

    String menu() default "";

    VaadinIcons icon() default VaadinIcons.ESC;

    VaadinIcons menuIcon() default VaadinIcons.ESC;

    boolean separator() default false;

    int order() default 0;
}
