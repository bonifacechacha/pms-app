package com.niafikra.dimension.core.ui.vaadin.view;

import com.vaadin.navigator.View;
import com.vaadin.ui.Component;

public class EntireMainContentDisplay extends AbstractMainContentDisplay {

    public EntireMainContentDisplay() {
        setMargin(false);
        setSizeFull();
    }

    @Override
    public void show(View view) {
        removeAllComponents();
        Component viewComponent = view.getViewComponent();
        addComponent(viewComponent);
        setExpandRatio(viewComponent, 1);
    }
}
