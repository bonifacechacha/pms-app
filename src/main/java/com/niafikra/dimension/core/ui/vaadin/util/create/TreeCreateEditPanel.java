package com.niafikra.dimension.core.ui.vaadin.util.create;

import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.SubWindow;
import com.niafikra.dimension.core.util.HasLogger;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.*;
import org.vaadin.viritin.button.ConfirmButton;
import org.vaadin.viritin.button.MButton;

import java.util.Optional;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/6/17 4:02 PM
 */
public abstract class TreeCreateEditPanel<T> extends VerticalLayout implements HasLogger {
    protected Tree<T> entriesTree;
    protected MButton createButton, editButton;
    protected ConfirmButton deleteButton;

    protected SubWindow subWindow = new SubWindow();


    public TreeCreateEditPanel(Tree<T> entriesTree) {
        this.entriesTree = entriesTree;
        createButton = new MButton("New");
        editButton = new MButton("Edit");
        deleteButton = new ConfirmButton();

        //lazy init
        addAttachListener(event -> build());
    }

    public TreeCreateEditPanel() {
        this(new Tree<>());
    }

    protected void build() {
        setSizeFull();

        createButton
                .withIcon(VaadinIcons.PLUS_CIRCLE)
                .withStyleName(Theme.BUTTON_PRIMARY, Theme.BUTTON_BORDERLESS, Theme.BUTTON_TINY)
                .withListener((Button.ClickListener) event -> showCreateContent());

        editButton
                .withIcon(VaadinIcons.PENCIL)
                .withStyleName(Theme.BUTTON_FRIENDLY, Theme.BUTTON_BORDERLESS, Theme.BUTTON_TINY)
                .withListener((Button.ClickListener) event -> showEditContent())
                .withVisible(false);

        deleteButton
                .withIcon(VaadinIcons.MINUS_CIRCLE)
                .withStyleName(Theme.BUTTON_DANGER, Theme.BUTTON_BORDERLESS, Theme.BUTTON_TINY)
                .addClickListener(() -> doDelete()).withCaption("Delete").withVisible(false);

        addComponent(new HorizontalLayout(createButton, editButton, deleteButton));

        entriesTree.setSizeFull();
        addComponent(entriesTree);
        setExpandRatio(entriesTree, 1);

        entriesTree.addSelectionListener(event -> {
            boolean noneSelected = event.getAllSelectedItems().isEmpty();
            editButton.setVisible(!noneSelected);
            deleteButton.setVisible(!noneSelected);
        });
    }

    private void doDelete() {
        Optional<T> entry = getSelected();
        if (entry.isPresent()) {
            delete(entry.get());
        }
    }

    protected abstract void delete(T entry);

    private void showEditContent() {
        Optional<T> entry = entriesTree.getSelectedItems().stream().findFirst();
        if (entry.isPresent()) {
            subWindow.setContent(getEditContent(entry.get()));
            subWindow.show();
        }
    }

    protected void showCreateContent() {
        subWindow.setContent(getCreateContent());
        subWindow.show();
    }

    public Tree<T> getEntriesTree() {
        return entriesTree;
    }

    public Button getCreateButton() {
        return createButton;
    }

    public Button getEditButton() {
        return editButton;
    }

    public Button getDeleteButton() {
        return deleteButton;
    }

    public SubWindow getSubWindow() {
        return subWindow;
    }

    public Optional<T> getSelected() {
        return entriesTree.getSelectedItems().stream().findFirst();
    }

    protected abstract Component getCreateContent();

    protected abstract Component getEditContent(T entry);
}
