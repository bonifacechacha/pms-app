package com.niafikra.dimension.core.ui.vaadin.settings;

import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewSectionsPanel;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AssignableTypeFilter;

import static com.niafikra.dimension.core.ui.vaadin.settings.SettingsPanel.VIEW_NAME;


@UIScope
@ViewComponent(value = MainDisplay.class, caption = "Settings")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class SettingsPanel extends ViewSectionsPanel {

    public static final String VIEW_NAME = "settings";

    public SettingsPanel(Navigator navigator) {
        super(navigator);
    }

    @Override
    protected void configureViewProvider(ClassPathScanningCandidateComponentProvider provider) {
        provider.addIncludeFilter(new AssignableTypeFilter(SettingView.class));
    }
}
