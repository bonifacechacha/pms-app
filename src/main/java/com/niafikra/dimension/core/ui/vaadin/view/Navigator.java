package com.niafikra.dimension.core.ui.vaadin.view;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.spring.internal.Conventions;
import com.vaadin.spring.navigator.SpringNavigator;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Governs view navigation of the app.
 */
@Component
@UIScope
public class Navigator extends SpringNavigator {
    private ViewChangeEvent recentViewChangeEvent;

    @PostConstruct
    public void init() {
        setErrorView(ErrorView.class);
    }

    /**
     * Find the view id (URI fragment) used for a given view class.
     *
     * @param viewClass the view class to find the id for
     * @return the URI fragment for the view
     */
    public String getViewId(Class<? extends View> viewClass) {
        SpringView springView = viewClass.getAnnotation(SpringView.class);
        if (springView == null) {
            throw new IllegalArgumentException("The target class must be a @SpringView");
        }

        return Conventions.deriveMappingForView(viewClass, springView);
    }

    /**
     * Navigate to the given view class.
     *
     * @param targetView the class of the target view, must be annotated using
     *                   {@link SpringView @SpringView}
     */
    public void navigateTo(Class<? extends View> targetView) {
        String viewId = getViewId(targetView);
        navigateTo(viewId);
    }

    public void navigateTo(Class<? extends View> targetView, Object parameter) {
        String viewId = getViewId(targetView);
        navigateTo(viewId + "/" + parameter.toString());
    }


    /**
     * Update the parameter of the the current view without firing any
     * navigation events.
     *
     * @param parameter the new parameter to set, never <code>null</code>,
     *                  <code>""</code> to not use any parameter
     */
    public void updateViewParameter(String parameter) {
        String viewName = getViewId(getCurrentView().getClass());
        String parameters;
        if (parameter == null) {
            parameters = "";
        } else {
            parameters = parameter;
        }

        updateNavigationState(new ViewChangeEvent(this, getCurrentView(), getCurrentView(), viewName, parameters));
    }

    public void reload() {
        navigateTo(getState());
    }

    @Override
    protected void updateNavigationState(ViewChangeEvent event) {
        this.recentViewChangeEvent = event;
        super.updateNavigationState(event);
    }

    public ViewChangeEvent getRecentViewChangeEvent() {
        return recentViewChangeEvent;
    }

    public void navigateBack() {
        Page.getCurrent().getJavaScript().execute("window.history.back()");
    }
}
