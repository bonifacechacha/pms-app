package com.niafikra.dimension.core.ui.vaadin.util.grid;

import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;

import java.util.List;
import java.util.stream.Collectors;

public class GridAddRemoveField<T> extends CustomField<List<T>> {
    private GridAddRemovePanel<T> addRemovePanel;
    private List<T> oldValue;

    public GridAddRemoveField() {
        addRemovePanel = new GridAddRemovePanel<>();
        addRemovePanel.addAddRemoveListener(new GridAddRemovePanel.AddRemoveListener<T>() {
            @Override
            public void onAdd(T entry) {
                fireValueChangeEvent(true);
            }

            @Override
            public void onRemove(T entry) {
                fireValueChangeEvent(true);
            }
        });

    }

    @Override
    protected Component initContent() {
        return addRemovePanel;
    }

    @Override
    protected void doSetValue(List<T> value) {
        oldValue = getValue();
        addRemovePanel.getGridDataProvider().getItems().clear();
        if (value != null)
            addRemovePanel.getGridDataProvider().getItems().addAll(value);
        addRemovePanel.getGridDataProvider().refreshAll();
    }

    @Override
    public List<T> getValue() {
        return addRemovePanel.getGridDataProvider().getItems().stream().collect(Collectors.toList());
    }


    private void fireValueChangeEvent(boolean userOriginated) {
        fireEvent(createValueChange(oldValue, userOriginated));
        oldValue = getValue();
    }

    public GridAddRemovePanel<T> getAddRemovePanel() {
        return addRemovePanel;
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        super.setReadOnly(readOnly);
        addRemovePanel.getGrid().setEnabled(!readOnly);
        addRemovePanel.getSelector().setEnabled(!readOnly);
    }
}
