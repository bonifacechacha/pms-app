package com.niafikra.dimension.core.ui.vaadin.util.grid;

import com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils;
import com.vaadin.data.BeanValidationBinder;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationResult;
import com.vaadin.data.provider.CallbackDataProvider;
import com.vaadin.ui.Grid;

import java.util.*;
import java.util.stream.Collectors;


/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/15/16 3:02 PM
 */


public class ItemEditGrid<T> extends Grid<Integer> {

    private Map<Integer, BeanValidationBinder<T>> itemBinders = new HashMap<>();
    private Integer recentItemID = 0;

    private Class<T> type;

    public ItemEditGrid(Class<T> type) {
        this.type = type;

        setSizeFull();

        setSelectionMode(Grid.SelectionMode.NONE);

        setDataProvider(
                new CallbackDataProvider<>(
                        query -> itemBinders.keySet().stream().skip(query.getOffset()).limit(query.getLimit()),
                        query -> itemBinders.size()
                )
        );
    }

    public void addRemoveColumn() {
        VaadinUtils.addRemoveButton(
                this,
                i -> removeEntry(i)
        );
    }

    public void removeEntry(Integer key) {
        itemBinders.remove(key);
        getDataProvider().refreshAll();
    }

    public void addEntry(T entry) {
        //increment the recent item id
        recentItemID++;

        BeanValidationBinder<T> binder = new BeanValidationBinder<>(type);
        binder.setRequiredConfigurator(null);

        //during validation if some fields have errrors then ignore them and do not show on the UI with red colors
        binder.setValidationStatusHandler(status -> {
        });
        binder.setBean(entry);

        itemBinders.put(recentItemID, binder);

//      setHeight(ROW_HEIGHT * itemBinders.size() + HEADER_ROW_HEIGHT, Unit.PIXELS);
        getDataProvider().refreshAll();
    }


    /**
     * Reset the state of the table view and its item tracking.
     * Also it initialise empty items on the table ready for editing ONLY IF the cost instance has EMPTY items list
     */
    public void clear() {
        itemBinders.clear();
        recentItemID = 0;
        getDataProvider().refreshAll();
    }

    public void setEntries(Collection<T> entries) {
        clear();
        entries.forEach(entry -> addEntry(entry));
    }


    public List<T> getValidItems() {
        LinkedList<T> items = new LinkedList<>();

        for (Object itemID : itemBinders.keySet().stream().sorted().collect(Collectors.toList())) {
            BeanValidationBinder<T> binder = itemBinders.get(itemID);
            if (isValid(binder))
                items.add(binder.getBean());
        }

        return items;

    }

    private boolean isValid(Binder<T> binder) {
        List<ValidationResult> errors = binder.validate().getValidationErrors();
        return errors.isEmpty();
    }

    public Binder<T> getBinder(Integer key) {
        return itemBinders.get(key);
    }
}
