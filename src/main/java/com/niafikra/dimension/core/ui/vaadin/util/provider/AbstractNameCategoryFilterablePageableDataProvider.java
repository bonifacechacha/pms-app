package com.niafikra.dimension.core.ui.vaadin.util.provider;

import com.niafikra.dimension.category.domain.Category;
import com.niafikra.dimension.category.domain.HasCategory;
import com.niafikra.dimension.core.util.HasName;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/31/17 11:18 AM
 */
public abstract class AbstractNameCategoryFilterablePageableDataProvider<T extends HasName & HasCategory>
        extends AbstractNameFilterablePageableDataProvider<T>
        implements NameCategoryFilterableDataProvider<T> {

    protected Category categoryFilter;

    public Category getCategoryFilter() {
        return categoryFilter;
    }

    public void setCategoryFilter(Category categoryFilter) {
        this.categoryFilter = categoryFilter;
        refreshAll();
    }

}
