package com.niafikra.dimension.core.ui.vaadin.util;

import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.navigator.View;
import org.vaadin.crudui.form.CrudFormFactory;
import org.vaadin.crudui.layout.CrudLayout;

public class MGridCrudView<T> extends MGridCrud<T> implements View, TableExportable {
    public MGridCrudView(Class<T> domainType) {
        super(domainType);
    }

    public MGridCrudView(Class<T> domainType, CrudLayout crudLayout) {
        super(domainType, crudLayout, new MVerticalCrudFormFactory<>(domainType));
    }

    public MGridCrudView(Class<T> domainType, CrudLayout crudLayout, CrudFormFactory<T> crudFormFactory) {
        super(domainType, crudLayout, crudFormFactory);
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(getGrid());
    }
}
