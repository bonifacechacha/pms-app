package com.niafikra.dimension.core.ui.vaadin.util.io;

import com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils;
import com.niafikra.dimension.core.util.DateUtils;
import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * Contains various utilities which help in handling apache POI files
 *
 * @author mbwana jaffari mbura
 * Date: 5/12/14
 * Time: 3:22 PM
 */

@Log
public class POIUtils {

    public static void insertRows(XSSFSheet sheet, int copyRowStart, int copyRowEnd, int insertStart, CellCopyPolicy cellCopyPolicy) {

        if (insertStart < sheet.getLastRowNum())
            sheet.shiftRows(insertStart, sheet.getLastRowNum(), copyRowEnd - copyRowStart + 1);

        sheet.copyRows(copyRowStart, copyRowEnd, insertStart, cellCopyPolicy);
    }

    /**
     * Get the cell value as an object which you can cast to any type you want. Useful
     * for since it can convert blank values and null values to value type null!
     *
     * @param cell
     * @return
     */
    public static Object getCellValue(Cell cell) {
        if (cell == null || cell.getCellType() == CellType.BLANK) return null;

        switch (cell.getCellType()) {
            case BOOLEAN:
                return cell.getBooleanCellValue();


            case STRING:
                return cell.getStringCellValue();


            case NUMERIC:
                return cell.getNumericCellValue();


            case FORMULA:
                return cell.getNumericCellValue();


            case ERROR:
                return cell.getErrorCellValue();

            default:
                return "unknown";
        }
    }

    /**
     * A method used to read a string value from the imported excel data
     *
     * @param row            the row
     * @param column         column index
     * @param isEmptyAllowed if true a null value or bank will be allowed
     * @param minLength      minimum length of string value expected
     * @param maxLength      a maximum length of string value expected
     * @return
     * @throws Exception
     */
    public static String readStringValue(Row row, int column, boolean isEmptyAllowed, String defaultValue,Integer minLength, Integer maxLength) {
        Cell cell = row.getCell(column);
        if (cell == null && !isEmptyAllowed)
            showErrorMessage(row, column, "Value provide must be a string and not blank");

        if (cell == null && isEmptyAllowed)
            return defaultValue;

        if (isEmptyAllowed && cell.getCellType() == CellType.BLANK)
            return defaultValue;

//        if (cell.getCellType() != CellType.STRING)
//            showErrorMessage(row, column, "Value provided must be a string " + cell);


        String stringCellValue = getCellValue(cell).toString();

        if (StringUtils.isEmpty(stringCellValue) && !isEmptyAllowed)
            showErrorMessage(row, column, "Value provide must be a string and not blank");

        if (minLength != null && maxLength != null) {
            if (StringUtils.isNotEmpty(stringCellValue) &&
                    (stringCellValue.length() < minLength ||
                            stringCellValue.length() > maxLength))
                showErrorMessage(row, column, "Value provide must have length greater than " +
                        minLength + " and less than " + maxLength + " characters ");
        }
        return stringCellValue;
    }

    /**
     * Read a numeric value from imported excel
     *
     * @param row                 the row
     * @param column              column index
     * @param isEmptyAllowed      if true a null value or bank will be allowed
     * @param minValue            a minimum allowed value for this numeric value
     * @param maxValue            a maximum value allowed for this numeric value
     * @param defaultValueOnEmpty a default value when empty is allowed if not provided zero will be returned
     * @return
     * @throws Exception
     */
    public static Double readNumericValue(Row row, int column, boolean isEmptyAllowed, Double defaultValueOnEmpty, Double minValue, Double maxValue) {

        Double numericCellValue = defaultValueOnEmpty;
        Cell cell = row.getCell(column);

        if (cell == null && !isEmptyAllowed)
            showErrorMessage(row, column, "Value provide must be a numeric and not blank");

        if (cell == null && isEmptyAllowed)
            return defaultValueOnEmpty;

        if (isEmptyAllowed && cell.getCellType() == CellType.BLANK)
            return defaultValueOnEmpty;

        if (cell.getCellType() != CellType.NUMERIC)
            showErrorMessage(row, column, "Value provided must be a numeric " + cell);


        numericCellValue = cell.getNumericCellValue();

        if (minValue != null && maxValue != null) {
            if ((numericCellValue < minValue ||
                    numericCellValue > maxValue))
                showErrorMessage(row, column, "Value provide must be greater than " +
                        minValue + " and less than " + maxValue + " numerically ");
        }
        return numericCellValue;

    }

    /**
     * Read a date value from uploaded excel
     * The date returned will be formatted based on system base date time format
     *
     * @param row            the row
     * @param column         column index
     * @param isEmptyAllowed if empty date is allowed
     * @param after          specify that the read date must be after this date
     * @param before         specify that the read date must be before this date
     * @return
     * @throws Exception
     */
    public static LocalDateTime readLocalDateTimeValue(Row row, int column, boolean isEmptyAllowed, LocalDateTime defaultValue, LocalDateTime after, LocalDateTime before) {
        LocalDateTime dateCellValue = null;
  
        Cell cell = row.getCell(column);

        if (cell == null && !isEmptyAllowed)
            showErrorMessage(row, column, "Value provide must be a date and not blank");

        if (cell == null && isEmptyAllowed)
            return defaultValue;

        if (isEmptyAllowed && cell.getCellType() == CellType.BLANK)
            return defaultValue;

        try {
            if (cell.getCellType() == CellType.STRING) {
                dateCellValue = DateUtils.parseLocalDateTime(cell.getStringCellValue());
            } else {
                dateCellValue = LocalDateTime.ofInstant(cell.getDateCellValue().toInstant(), ZoneId.systemDefault());
            }
        } catch (Exception e) {
            showErrorMessage(row, column, "Date value provided doesnt use the supported date format.<br/><br/>Use (" + DateUtils.DATE_TIME_FORMAT + " ) as a format");
        }

        if (before != null && !dateCellValue.isBefore(before))
            showErrorMessage(row, column, "Date value provide must be before " +
                    DateUtils.formatDateTime(before));

        if (after != null && !dateCellValue.isAfter(after))
            showErrorMessage(row, column, "Date value provide must be after " +
                    DateUtils.formatDateTime(after));

        return dateCellValue;
    }


    public static LocalDate readLocalDateValue(Row row, int column, boolean isEmptyAllowed, LocalDate defaultValue, LocalDate after, LocalDate before) {
        LocalDate dateCellValue = null;

        Cell cell = row.getCell(column);

        if (cell == null && !isEmptyAllowed)
            showErrorMessage(row, column, "Value provide must be a date and not blank");

        if (cell == null && isEmptyAllowed)
            return defaultValue;

        if (isEmptyAllowed && cell.getCellType() == CellType.BLANK)
            return defaultValue;

        try {
            if (cell.getCellType() == CellType.STRING) {
                dateCellValue = DateUtils.parseLocalDate(cell.getStringCellValue());
            } else {
                dateCellValue = LocalDate.ofInstant(cell.getDateCellValue().toInstant(), ZoneId.systemDefault());
            }
        } catch (Exception e) {
            showErrorMessage(row, column, "Date value provided doesnt use the supported date format.<br/><br/>Use (" + DateUtils.DATE_FORMAT + " ) as a format");
        }

        if (before != null && !dateCellValue.isBefore(before))
            showErrorMessage(row, column, "Date value provide must be before " +
                    DateUtils.formatDate(before));

        if (after != null && !dateCellValue.isAfter(after))
            showErrorMessage(row, column, "Date value provide must be after " +
                    DateUtils.formatDate(after));

        return dateCellValue;
    }


    public static void showErrorMessage(Row row, int column, String errorMessage) {
        String error = errorMessage +
                " at sheet " + row.getSheet().getSheetName() +
                " at row " + (row.getRowNum() + 1) + ", " + (column + 1);
        NotificationUtils.showError("Failed to read spreadsheet document", error);
        log.severe(error);
        throw new RuntimeException("Failed to import excel");
    }

    public static boolean isEmpty(Row row, int cellIndex) {
        Cell cell = row.getCell(cellIndex);
        return cell == null || cell.getStringCellValue().isEmpty();
    }

    public static void writeInRow(Row row, Object... contents) {

        int cellIndex = 0;
        for (Object content : contents) {
            if (content == null) {
                cellIndex++;//step and dont write anything
                continue;
            }
            Cell cell = row.getCell(cellIndex++, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);


            if (Number.class.isAssignableFrom(content.getClass()))
                cell.setCellValue(Double.valueOf(content.toString()));
            else cell.setCellValue(content.toString());
        }

    }

    public static void autoSizeColumns(Sheet sheet, int columnCount) {
        for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
            sheet.autoSizeColumn(columnIndex);
        }
    }

    public static Workbook createHSSFWorkBook(String fileResourceName) {
        try {
            return new HSSFWorkbook(POIUtils.class.getClassLoader().getResourceAsStream(fileResourceName));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Workbook createXSSFWorkBook(String fileResourceName) {
        try {
            return new XSSFWorkbook(POIUtils.class.getClassLoader().getResourceAsStream(fileResourceName));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void applyStyle(Row row, CellStyle cellStyle) {
        row.cellIterator().forEachRemaining(cell -> {
            cell.setCellStyle(cellStyle);
        });
    }
}
