package com.niafikra.dimension.core.ui.vaadin.util;

import com.niafikra.dimension.core.domain.Printable;
import de.steinwedel.messagebox.MessageBox;

public class PrintWindow {

    public static MessageBox create(Printable printable, String message) {
        return MessageBox.createQuestion()
                .withCaption(printable.toString())
                .withMessage(message)
                .withButton(new PrintButton(printable).withCaption("Print"))
                .withCloseButton();
    }
}
