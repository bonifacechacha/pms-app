package com.niafikra.dimension.core.ui.vaadin;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.security.service.UserService;
import com.niafikra.dimension.core.ui.vaadin.actions.ViewActions;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import lombok.RequiredArgsConstructor;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/19/17 8:01 PM
 */

@UIScope
@SpringComponent
@RequiredArgsConstructor
public class MainScreen extends HorizontalLayout {

    private final SideBar sideBar;
    private final ViewActions viewActions;
    private final MainContent content;
    private final MainHeader mainHeader;
    private final WelcomeContent welcomeContent;

    private final User currentUser;

    private final UserService userService;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        viewActions.setWidth("100%");
        addComponent(
                new MVerticalLayout(sideBar, viewActions)
                        .withExpand(sideBar, 1)
                        .withFullHeight()
                        .withSpacing(false)
                        .withMargin(false)
                        .withAlign(viewActions, Alignment.MIDDLE_RIGHT)
                        .withWidth(sideBar.getWidth(), sideBar.getWidthUnits())
        );

        VerticalLayout contentBase = new VerticalLayout(mainHeader, content);
        contentBase.setSizeFull();
        contentBase.setExpandRatio(content, 1);
        contentBase.setMargin(false);
        contentBase.setSpacing(false);

        addComponent(contentBase);
        setExpandRatio(contentBase, 1);

//        currentUser = SecurityUtils.getCurrentUser(userService);

        sideBar.setHeader(welcomeContent);
    }

}
