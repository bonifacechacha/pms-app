package com.niafikra.dimension.core.ui.vaadin.user;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.util.create.NameFilterableListGrid;

import java.util.Collection;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/29/17 2:29 PM
 */
public class UsersListGrid extends NameFilterableListGrid<User> {

    public Collection<User> getUsers() {
        return getItems();
    }
}
