package com.niafikra.dimension.core.ui.vaadin.user.group;

import com.niafikra.dimension.core.ui.vaadin.util.NameDescriptionForm;
import com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils;
import com.niafikra.dimension.group.domain.Group;
import com.niafikra.dimension.group.service.GroupService;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/30/17 9:18 PM
 */
@Component
@PrototypeScope
public class GroupForm extends NameDescriptionForm<Group> {

    private GroupService groupService;

    public GroupForm(GroupService groupService) {
        super(Group.class);
        this.groupService = groupService;

        addSavedHandler(group -> doSave(group));
    }

    private void doSave(Group group) {
        try {
            group = groupService.save(group);
            setEntity(group);

            String msg = "Saved group successful";
            getLogger().info(msg);
            NotificationUtils.showSuccess(msg);
        } catch (Exception e) {
            String msg = "Failed to save group";
            getLogger().error(msg, e);
            NotificationUtils.showError(msg);
        }
    }
}
