package com.niafikra.dimension.core.ui.vaadin.util;

import com.vaadin.navigator.ViewChangeListener;

public interface MViewChangeListener extends ViewChangeListener {
    default boolean beforeViewChange(ViewChangeEvent event) {
        return true;
    }
}
