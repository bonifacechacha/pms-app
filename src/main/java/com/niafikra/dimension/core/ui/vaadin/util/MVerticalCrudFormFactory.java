package com.niafikra.dimension.core.ui.vaadin.util;

import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.crudui.crud.CrudOperation;
import org.vaadin.crudui.form.impl.form.factory.VerticalCrudFormFactory;
import org.vaadin.viritin.layouts.MMarginInfo;

public class MVerticalCrudFormFactory<T> extends VerticalCrudFormFactory<T> {

    public MVerticalCrudFormFactory(Class<T> domainType) {
        super(domainType);
        setButtonCaption(CrudOperation.ADD, "Save");
    }

    @Override
    public Component buildNewForm(CrudOperation operation, T object, boolean readOnly, Button.ClickListener cancelListener, Button.ClickListener operationListener) {
        VerticalLayout formLayout = (VerticalLayout) super.buildNewForm(operation, object, readOnly, cancelListener, operationListener);
        formLayout.setMargin(new MMarginInfo(false, true, true, true));
        return formLayout;
    }
}
