package com.niafikra.dimension.core.ui.vaadin.view;

import com.niafikra.dimension.core.ui.vaadin.MainContent;
import com.niafikra.dimension.core.ui.vaadin.MainHeader;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.Registration;
import com.vaadin.ui.MenuBar;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.inject.Inject;
import java.util.Set;
import java.util.function.Supplier;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/24/17 6:07 PM
 */
public abstract class AbstractMainContentDisplay extends MVerticalLayout implements Display, ViewChangeListener, Notifier {

    @Inject
    protected MainContent mainContent;

    @Inject
    protected Navigator navigator;

    @Inject
    protected MainHeader mainHeader;
    protected Registration viewChangeRegistration;

    //we use supplier to prevent unnecessary check logic execution incase the menu is null
    public static void setMenuVisible(MenuBar.MenuItem menuItem, Supplier<Boolean> visibility) {
        if (menuItem != null) menuItem.setVisible(visibility.get());
    }

    @Override
    public void attach() {
        super.attach();
        viewChangeRegistration = navigator.addViewChangeListener(this);
    }

    @Override
    public void detach() {
        super.detach();
        viewChangeRegistration.remove();
    }

    @Override
    public void start() {
        mainContent.show(this);
    }

    @Override
    public boolean beforeViewChange(ViewChangeEvent event) {
        return true;
    }

    public void setMenuVisible(String menu, Supplier<Boolean> visibility) {
        mainHeader.getMenuBar().setMenuVisible(menu, visibility);
    }

    public void setMenuVisible(Set<String> menus, Supplier<Boolean> visibility) {
        menus.forEach(menu->setMenuVisible(menu,visibility));
    }
}
