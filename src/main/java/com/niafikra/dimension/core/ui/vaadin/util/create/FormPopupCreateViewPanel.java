package com.niafikra.dimension.core.ui.vaadin.util.create;

import com.niafikra.dimension.core.ui.vaadin.util.AbstractForm;
import com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/6/17 9:01 PM
 */
public abstract class FormPopupCreateViewPanel<T> extends PopupCreateViewPanel<T> {

    private AbstractForm<T> form;

    public FormPopupCreateViewPanel(Grid<T> entriesGrid, AbstractForm<T> form) {
        super(entriesGrid);
        this.form = form;
        this.form.addSavedHandler(t -> subWindow.close());

        subWindow.setWidth("40%");
        subWindow.setHeightUndefined();

        VaadinUtils.addRemoveButton(getEntriesGrid(), this::onDelete);
    }

    protected abstract T createNewEntity();

    protected abstract void onDelete(T entity);

    @Override
    protected Component getCreateComponent() {
        form.setEntity(createNewEntity());
        return form;
    }

    @Override
    protected Component getViewComponent(T entry) {
        form.setEntity(entry);
        return form;
    }

    public AbstractForm<T> getForm() {
        return form;
    }
}
