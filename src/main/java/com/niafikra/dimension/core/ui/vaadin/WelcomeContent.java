package com.niafikra.dimension.core.ui.vaadin;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.util.TemplateView;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.VerticalLayout;
import lombok.RequiredArgsConstructor;

import javax.annotation.PostConstruct;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/20/17 3:15 PM
 */
@SpringComponent
@UIScope
@RequiredArgsConstructor
public class WelcomeContent extends VerticalLayout {

    private final User currentUser;
    private final TemplateView templateView;

    @PostConstruct
    private void build() {
        setMargin(false);
        addComponent(templateView);
        templateView.setTemplatePath(Templates.WELCOME);
        templateView.putBinding("user", currentUser).render();
    }

}
