package com.niafikra.dimension.core.ui.vaadin.user.group.delegation;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.user.UserComboBox;
import com.niafikra.dimension.core.ui.vaadin.user.group.GroupComboBox;
import com.niafikra.dimension.core.ui.vaadin.util.date.LocalDateTimeRangeSelector;
import com.niafikra.dimension.core.util.DateUtils;
import com.niafikra.dimension.delegation.domain.Delegation;
import com.niafikra.dimension.delegation.service.DelegationService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.HeaderRow;

@ViewScope
@SpringComponent
public class DelegationsGrid extends Grid<Delegation> {
    private DelegationService delegationService;
    private DelegationPanel delegationPanel;

    private DelegationsProvider delegationsProvider;

    public DelegationsGrid(DelegationsProvider delegationsProvider,
                           DelegationPanel delegationPanel,
                           GroupComboBox groupFilter,
                           UserComboBox delegatorFilter,
                           UserComboBox delegateeFilter,
                           DelegationService delegationService) {
        super(delegationsProvider);
        this.delegationPanel = delegationPanel;
        this.delegationService = delegationService;
        this.delegationsProvider = delegationsProvider;

        addColumn(delegation -> DateUtils.formatDateTime(delegation.getTimeCreated()))
                .setId("timeCreated")
                .setCaption("Date")
                .setHidable(true)
                .setSortable(true);

        addColumn(Delegation::getGroup)
                .setId("group")
                .setCaption("Group")
                .setHidable(true)
                .setSortable(true);

        addColumn(Delegation::getDelegator)
                .setId("delegator")
                .setCaption("Delegator")
                .setHidable(true)
                .setSortable(true);

        addColumn(Delegation::getDelegatee)
                .setId("delegatee")
                .setCaption("Delegatee")
                .setHidable(true)
                .setSortable(true);

        addColumn(delegation -> DateUtils.formatDate(delegation.getStartDate()))
                .setId("startDate")
                .setCaption("Start")
                .setHidable(true)
                .setSortable(true);

        addColumn(delegation -> DateUtils.formatDate(delegation.getEndDate()))
                .setId("endDate")
                .setCaption("End")
                .setHidable(true)
                .setSortable(true);

        addColumn(delegation -> delegationService.getStatus(delegation))
                .setId("status")
                .setCaption("Status")
                .setHidable(true)
                .setSortable(true);

        addItemClickListener(itemClick -> {
            showDelegation(itemClick.getItem());
        });

        LocalDateTimeRangeSelector timeCreatedRangePanel = new LocalDateTimeRangeSelector();
        timeCreatedRangePanel.setWidth("200px");

        HeaderRow headerRow = appendHeaderRow();
        headerRow.getCell("timeCreated").setComponent(timeCreatedRangePanel);
        timeCreatedRangePanel.addValueChangeListener(event -> {
            delegationsProvider.setDateRange(timeCreatedRangePanel.getStart(), timeCreatedRangePanel.getEnd());
        });
        headerRow.getCell("group").setComponent(groupFilter.withFullWidth());
        groupFilter.addValueChangeListener(event -> {
            delegationsProvider.setGroup(event.getValue());
        });
        headerRow.getCell("delegator").setComponent(delegatorFilter.withFullWidth());
        delegatorFilter.addValueChangeListener(event -> {
            delegationsProvider.setDelegator(event.getValue());
        });

        headerRow.getCell("delegatee").setComponent(delegateeFilter.withFullWidth());
        delegateeFilter.addValueChangeListener(event -> {
            delegationsProvider.setDelegatee(event.getValue());
        });
    }

    private void showDelegation(Delegation delegation) {
        delegationPanel
                .popUp(delegation)
                .withCloseListener(closeEvent -> getDataProvider().refreshAll());
    }

    public void filterByCreator(User currentUser) {
        delegationsProvider.setCreator(currentUser);
    }
}
