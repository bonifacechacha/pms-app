package com.niafikra.dimension.core.ui.vaadin;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;

import javax.annotation.PostConstruct;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/19/17 8:38 PM
 */

@UIScope
@SpringComponent
public class MainContent extends VerticalLayout {

    @PostConstruct
    private void build() {
        setMargin(false);
        setSizeFull();
    }

    public void show(Component component) {
        removeAllComponents();
        addComponent(component);
        setExpandRatio(component, 1);
    }
}
