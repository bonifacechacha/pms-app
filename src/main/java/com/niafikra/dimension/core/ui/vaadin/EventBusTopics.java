package com.niafikra.dimension.core.ui.vaadin;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/6/17 12:39 PM
 */
public class EventBusTopics {
    public static final String BUDGET_CHANGE = "Budget changed";
//    public static final String TOPIC_COST_CENTER_SAVED = "Cost center saved";
//    public static final String TOPIC_COST_CENTER_DELETED = "Cost center deleted";
//    public static final String TOPIC_LEVEL_SAVED = "Level saved";
//    public static final String TOPIC_LEVEL_DELETED= "Level deleted";

    private EventBusTopics() {
    }


}
