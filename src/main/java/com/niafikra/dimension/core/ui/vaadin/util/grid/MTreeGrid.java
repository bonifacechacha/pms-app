package com.niafikra.dimension.core.ui.vaadin.util.grid;

import com.vaadin.ui.TreeGrid;

public class MTreeGrid<T> extends TreeGrid<T> {

    public void expandAll() {
        expandRoots();
        getTreeData().getRootItems().forEach(root -> {
            expand(getTreeData().getChildren(root));
        });
    }

    public void expandRoots() {
        expand(getTreeData().getRootItems());
    }


}
