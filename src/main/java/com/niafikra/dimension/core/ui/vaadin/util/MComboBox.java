package com.niafikra.dimension.core.ui.vaadin.util;

import com.vaadin.shared.Registration;
import com.vaadin.ui.ComboBox;
import org.vaadin.viritin.fluency.ui.FluentAbstractField;
import org.vaadin.viritin.fluency.ui.FluentHasValue;

import java.util.Collection;
import java.util.Collections;
import java.util.function.Function;

public class MComboBox<T> extends ComboBox<T> implements FluentAbstractField<MComboBox<T>, T>, FluentHasValue<MComboBox<T>, T> {
    public MComboBox() {
    }

    public MComboBox(String caption) {
        super(caption);
    }

    public static <T, R> Registration link(ComboBox<T> linker, ComboBox<R> linkee, Function<T, Collection<R>> provider, boolean emptyOnNullSelection) {
        return linker.addValueChangeListener(event -> {
            T t = event.getValue();
            linkee.clear();

            if (emptyOnNullSelection && t == null) {
                linkee.setItems(Collections.emptyList());
            } else {
                Collection<R> rs = provider.apply(t);
                linkee.setItems(rs);
                if (rs.size() == 1)
                    linkee.setValue(rs.stream().findFirst().get());
            }
        });
    }

    public MComboBox<T> withItems(Collection<T> items) {
        setItems(items);
        return this;
    }

    public <R> Registration link(ComboBox<R> linkee, Function<T, Collection<R>> provider) {
        return link(linkee, provider, true);
    }

    public <R> Registration link(ComboBox<R> linkee, Function<T, Collection<R>> provider, boolean emptyOnNullSelection) {
        return link(this, linkee, provider, emptyOnNullSelection);
    }
}
