package com.niafikra.dimension.core.ui.vaadin.user;

import com.niafikra.dimension.core.security.domain.Role;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.security.service.RoleService;
import com.niafikra.dimension.core.security.service.UserService;
import com.niafikra.dimension.core.ui.vaadin.util.AbstractForm;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TwinColSelect;
import org.springframework.dao.DataIntegrityViolationException;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.fields.EmailField;
import org.vaadin.viritin.fields.LabelField;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.layouts.MFormLayout;

import javax.annotation.PostConstruct;

@SpringComponent
@PrototypeScope
public class UserForm extends AbstractForm<User> {

    private LabelField<String> loginId = new LabelField<String>("Username").withFullWidth();
    private TextField name = new MTextField("Name").withFullWidth();
    private EmailField email = new EmailField("Email").withFullWidth();
    private TextField phoneNumber = new MTextField("Phone").withFullWidth();
    private TwinColSelect<Role> roles = new TwinColSelect<>("Roles");
    private UserService userService;
    private RoleService authoritiyService;

    public UserForm(
            UserService userService,
            RoleService authoritiyService) {
        super(User.class);
        this.userService = userService;
        this.authoritiyService = authoritiyService;

        addSavedHandler(user -> doSave(user));
        addDeleteHandler(user -> doDelete(user));
    }


    @PostConstruct
    private void build() {

        roles.setWidth("100%");
        roles.setItems(authoritiyService.findAll(RoleService.RoleFilter.builder().build()));
    }

    private void doDelete(User user) {
        try {
            userService.delete(user);
            showSuccess("Successful deleted user details");
        } catch (DataIntegrityViolationException e) {
            showError("Failed to delete user details", "User details are referenced on another entity");
        } catch (Exception e) {
            showError("Failed to delete user details", e);
        }
    }

    private void doSave(User user) {
        try {
            userService.save(user);
            showSuccess("Successful saved user details");
        } catch (Exception e) {
            showError("Failed to save user details", e);
        }
    }

    @Override
    protected Component createContent() {
        return new MFormLayout(loginId, name, email, phoneNumber, roles, getToolbar()).withMargin(true);
    }
}
