/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.niafikra.dimension.core.ui.vaadin.util.upload;

import com.vaadin.ui.Upload.Receiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * This class receives a file and writes by default to the system temporary
 * folder. You can change this behaviour by using the constructor that specifies
 * where to upload the file to
 *
 * @author mbwana jaffari mbura
 */
public class UploadReceiver implements Receiver, Serializable {

    private String fileName;
    private String mtype;
    private Logger logger = LoggerFactory.getLogger(UploadReceiver.class);
    private String destination;

    /**
     * Default constructor will upload files to temporary directory
     */
    public UploadReceiver() {
        this(null);
    }

    /**
     * @param destination the directory to load the files to
     */
    public UploadReceiver(String destination) {
        this.destination = destination != null ? destination : System.getProperty("java.io.tmpdir");
    }

    public OutputStream receiveUpload(String filename, String mimetype) {
        fileName = filename;
        mtype = mimetype;
        File tmpStore = new File(
                new StringBuilder(destination)
                        .append(File.separator)
                        .append(filename)
                        .toString());

        FileOutputStream out = null;
        try {
            out = new FileOutputStream(tmpStore);
        } catch (FileNotFoundException ex) {
            logger.error("Couldn't write to file " + destination, ex);
        }

        return out;
    }

    public String getFileName() {
        return fileName;
    }

    public String getMimeType() {
        return mtype;
    }

}

