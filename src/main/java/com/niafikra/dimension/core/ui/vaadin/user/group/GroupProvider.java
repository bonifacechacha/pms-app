package com.niafikra.dimension.core.ui.vaadin.user.group;

import com.niafikra.dimension.core.ui.vaadin.util.provider.AbstractNameFilterablePageableDataProvider;
import com.niafikra.dimension.group.domain.Group;
import com.niafikra.dimension.group.service.GroupService;
import com.vaadin.data.provider.Query;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/28/17 11:40 AM
 */
@Component
@PrototypeScope
public class GroupProvider extends AbstractNameFilterablePageableDataProvider<Group> {

    private GroupService groupService;

    public GroupProvider(GroupService groupService) {
        this.groupService = groupService;
    }

    @Override
    protected Page<Group> fetchFromBackEnd(Query<Group, String> query, Pageable pageable) {
        Page<Group> groups = groupService.findGroups(getFilter(query), pageable);
        return groups;
    }

    @Override
    protected int sizeInBackEnd(Query<Group, String> query) {
        Long count = groupService.countGroups(getFilter(query));
        return count.intValue();
    }

}
