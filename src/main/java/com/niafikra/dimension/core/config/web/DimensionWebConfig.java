package com.niafikra.dimension.core.config.web;

import com.niafikra.dimension.core.util.DateUtils;
import com.niafikra.dimension.hr.api.controller.employee.EmployeeData;
import com.niafikra.dimension.hr.domain.Employee;
import com.niafikra.dimension.money.Money;
import com.niafikra.dimension.payroll.api.controller.element.deduction.DeductionData;
import com.niafikra.dimension.payroll.domain.element.Deduction;
import com.niafikra.dimension.plan.api.controller.budget.AllocationData;
import com.niafikra.dimension.plan.api.controller.expense.ExpenseData;
import com.niafikra.dimension.plan.api.controller.imprest.ImprestData;
import com.niafikra.dimension.plan.domain.Allocation;
import com.niafikra.dimension.plan.domain.Expense;
import com.niafikra.dimension.plan.domain.Requisition;
import org.modelmapper.AbstractProvider;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.time.LocalDate;
import java.time.LocalDateTime;

//TODO USE MAPSTRUCT
@Configuration
public class DimensionWebConfig {


    //TODO REMOVE THE IDEA OF HAVING A SINGLE MODEL MAPPER USED BY ALL COMPONENTS BECAUSE DIFFERENT COMPONENTS
    //TODO HAVE DIFFERENT NEEDS
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    @Bean
    ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
//                .setMatchingStrategy(MatchingStrategies.STRICT)
                .setFieldMatchingEnabled(false)
                .setPropertyCondition(Conditions.isNotNull());

        configure(modelMapper);

        modelMapper.addMappings(new PropertyMap<Expense, ExpenseData>() {
            @Override
            protected void configure() {
                map().setPaymentTime(source.getPaymentTime());
                map().setPaymentReference(source.getPaymentReference());
                map().setPaidAmount(source.getPaidAmount().toString());
                map().setActualAmount(source.getActualAmount().toString());
                map().setPaymentMethod(source.getPaymentMethod().getName());
            }
        });

        modelMapper.addMappings(new PropertyMap<Requisition, ImprestData>() {
            @Override
            protected void configure() {
                map().setPaidAmount(source.getPaidAmount().toString());
                map().setRequestedAmount(source.getRequestedAmount().toString());
                map().setApprovedAmount(source.getApprovedAmount().toString());
                map().setPendingAmount(source.getPendingAmount().toString());
            }
        });

        modelMapper.addMappings(new PropertyMap<Allocation, AllocationData>() {
            @Override
            protected void configure() {
                map().setProposedAmount(source.getProposedAmount().toString());
                map().setAllocatedAmount(source.getAllocatedAmount().toString());
            }
        });

        modelMapper.addMappings(new PropertyMap<Employee, EmployeeData>() {
            @Override
            protected void configure() {
                map().setEmail(source.getContact().getEmail());
                map().setPhone(source.getContact().getPhone());
                map().setAddress(source.getContact().getAddress());
            }
        });

        modelMapper.addMappings(new PropertyMap<Deduction, DeductionData>() {
            @Override
            protected void configure() {
                map().setEmployeeContr(source.getEmployeeContr().toString());
                map().setEmployerContr(source.getEmployerContr().toString());
            }
        });

        return modelMapper;
    }

    private void configure(ModelMapper modelMapper) {

        modelMapper.createTypeMap(LocalDate.class,String.class);
        modelMapper.addConverter(context -> DateUtils.formatDate(context.getSource()),LocalDate.class,String.class);
        modelMapper.createTypeMap(String.class, LocalDate.class);
        modelMapper.addConverter(context -> DateUtils.parseLocalDate(context.getSource()), String.class, LocalDate.class);
        modelMapper.getTypeMap(String.class, LocalDate.class).setProvider(new AbstractProvider<>() {
            @Override
            protected LocalDate get() {
                return LocalDate.now();
            }
        });


        modelMapper.createTypeMap(LocalDateTime.class,String.class);
        modelMapper.addConverter(context -> DateUtils.formatDateTime(context.getSource()),LocalDateTime.class,String.class);

        modelMapper.createTypeMap(String.class, LocalDateTime.class);
        modelMapper.addConverter(context -> DateUtils.parseLocalDateTime(context.getSource()), String.class, LocalDateTime.class);
        modelMapper.getTypeMap(String.class, LocalDateTime.class).setProvider(new AbstractProvider<>() {
            @Override
            protected LocalDateTime get() {
                return LocalDateTime.now();
            }
        });

        modelMapper.createTypeMap(String.class,Money.class)
                .setConverter(context -> Money.parse(context.getSource()))
        .setProvider(request -> Money.getZERO());
        modelMapper.createTypeMap(Money.class,String.class).setConverter(context -> context.getSource().toString());
//        modelMapper.addConverter(context -> Money.parse(context.getSource()),String.class,Money.class);
//        modelMapper.addConverter(context -> context.getSource().toString(),Money.class,String.class);
//
//        modelMapper.getTypeMap(String.class,Money.class).setProvider(new AbstractProvider<Money>() {
//            @Override
//            protected Money get() {
//                return Money.getZERO();
//            }
//        });
    }
}
