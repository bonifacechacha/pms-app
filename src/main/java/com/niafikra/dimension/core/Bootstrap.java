package com.niafikra.dimension.core;

import com.niafikra.dimension.PMSApp;
import com.niafikra.dimension.core.util.HasLogger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;


/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/11/16 7:12 PM
 */
@Service
public class Bootstrap implements InitializingBean, HasLogger {
    @Value("${dimension.server.address}")
    protected String serverAddress;

    @Value("${server.port}")
    protected int serverPort;

    @Override
    @Transactional()
    public void afterPropertiesSet() {
        getLogger().info("Bootstrapping data...");

        PMSApp.baseUrl = serverPort == 80 ? serverAddress : serverAddress + ":" + serverPort;
        // do some setup here
        getLogger().info("Bootstrapping completed");
    }
}
