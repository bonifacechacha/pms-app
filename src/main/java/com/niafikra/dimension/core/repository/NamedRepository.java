package com.niafikra.dimension.core.repository;

import com.niafikra.dimension.core.util.HasName;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Collection;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/30/17 11:30 AM
 */
@NoRepositoryBean
public interface NamedRepository<T extends HasName> extends DimensionRepository<T, Long> {

    Page<T> findAllByNameContainingIgnoreCase(String nameFilter, Pageable pageable);

    Long countAllByNameContainingIgnoreCase(String nameFilter);

    T findByName(String name);

    T findByNameContainingIgnoreCase(String name);

    Page<T> findAllByNameContainingIgnoreCaseAndIdIsNotIn(String nameFilter, Collection<Long> idsToExclude, Pageable pageable);

    Long countAllByNameContainingIgnoreCaseAndIdIsNotIn(String nameFilter, Collection<Long> idsToExclude);
}
