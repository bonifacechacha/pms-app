package com.niafikra.dimension.payroll.ui.vaadin.payroll;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.ui.vaadin.view.DisplayComponent;
import com.niafikra.dimension.core.ui.vaadin.view.EntireMainContentDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.niafikra.dimension.payroll.domain.contract.pay.Payroll;
import com.niafikra.dimension.payroll.service.PayrollService;
import com.niafikra.dimension.payroll.ui.vaadin.payroll.pay.PayrollPayView;
import com.vaadin.icons.VaadinIcons;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.dialogs.ConfirmDialog;

import javax.inject.Inject;
import java.util.Set;

@DisplayComponent
public class PayrollDisplay extends EntireMainContentDisplay {

    @Inject
    private PayrollService payrollService;

    @Inject
    private Navigator navigator;

    private Payroll payroll;

    public void setPayroll(Payroll payroll) {
        this.payroll = payroll;
        this.mainHeader.setHeading(payroll.toString());
        setMenuVisible(Set.of("Delete","Run","Close"), () -> !payroll.isClosed());
        setMenuVisible(Set.of("Open"), () -> payroll.isClosed());
    }

    @Override
    public void afterViewChange(ViewChangeEvent event) {
        Long payrollId = Long.parseLong(event.getParameters());
        setPayroll(payrollService.findPayroll(payrollId));
    }

    @Secured(Permission.VIEW_PAYROLL)
    @ViewMenuOption(value = "View", menu = "Payroll", order = 0, icon = VaadinIcons.FILE_PROCESS, menuIcon = VaadinIcons.MONEY_EXCHANGE)
    public void showPayrollSummary() {
        navigator.navigateTo(PayrollView.class, payroll.getId());
    }

    @Secured(Permission.DELETE_PAYROLL)
    @ViewMenuOption(value = "Delete", menu = "Payroll", order = 1, icon = VaadinIcons.TRASH, menuIcon = VaadinIcons.MONEY_EXCHANGE)
    public void deletePayroll() {
        ConfirmDialog.show(
                getUI(),
                "Please confirm",
                "Are you sure, you want to delete this payroll?",
                "Delete",
                "Cancel",
                dialog -> {
                    if (dialog.isConfirmed()) {
                        try {
                            payrollService.delete(payroll);
                            showSuccess("Deleted payroll successful", payroll.toString());
                            navigator.navigateTo(PayrollsView.class);
                        } catch (Exception e) {
                            showError("Failed to delete payroll", e);
                        }
                    }
                }
        );
    }


    @Secured(Permission.RUN_PAYROLL)
    @ViewMenuOption(value = "Run", menu = "Payroll", order = 2, icon = VaadinIcons.CALC, menuIcon = VaadinIcons.MONEY_EXCHANGE)
    public void runPayroll() {
        ConfirmDialog.show(
                getUI(),
                "Please confirm",
                "Are you sure, you want to run this payroll?",
                "Run",
                "Cancel",
                dialog -> {
                    if (dialog.isConfirmed()) {

                        try {
                            payrollService.run(payroll);
                            showSuccess("Payroll has run successful", payroll.toString());
                            navigator.navigateTo(PayrollView.class, payroll.getId());
                        } catch (Exception e) {
                            showError("Failed to ran payroll", e);
                        }
                    }
                }
        );
    }

    @Secured(Permission.CLOSE_PAYROLL)
    @ViewMenuOption(value = "Close", menu = "Payroll", order = 3, icon = VaadinIcons.RECORDS, menuIcon = VaadinIcons.MONEY_EXCHANGE, separator = true)
    public void closePayroll() {
        ConfirmDialog.show(
                getUI(),
                "Please confirm",
                "Are you sure, you want to close this payroll?",
                "Close",
                "Cancel",
                dialog -> {
                    if (dialog.isConfirmed()) {

                        try {
                            payrollService.close(payroll);
                            showSuccess("Payroll has been closed successful", payroll.toString());
                            navigator.navigateTo(PayrollView.class, payroll.getId());
                        } catch (Exception e) {
                            showError("Failed to close payroll", e);
                        }
                    }
                }
        );
    }

    @Secured(Permission.OPEN_PAYROLL)
    @ViewMenuOption(value = "Open", menu = "Payroll", order = 3, icon = VaadinIcons.RECYCLE, menuIcon = VaadinIcons.MONEY_EXCHANGE, separator = true)
    public void openPayroll() {
        ConfirmDialog.show(
                getUI(),
                "Please confirm",
                "Are you sure, you want to open this payroll?",
                "Open",
                "Cancel",
                dialog -> {
                    if (dialog.isConfirmed()) {

                        try {
                            payrollService.open(payroll);
                            showSuccess("Payroll has been opened successful", payroll.toString());
                            navigator.navigateTo(PayrollView.class, payroll.getId());
                        } catch (Exception e) {
                            showError("Failed to open payroll", e);
                        }
                    }
                }
        );
    }


    @Secured(Permission.VIEW_PAYROLL_PAYS)
    @ViewMenuOption(value = "Salaries/Wages", menu = "Payroll", order = 4, icon = VaadinIcons.FILE_TABLE, menuIcon = VaadinIcons.MONEY_EXCHANGE)
    public void showPayrollPays() {
        navigator.navigateTo(PayrollPayView.class, payroll.getId());
    }

}
