package com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.income;

import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.payroll.domain.contract.pay.submitted.SubmittedIncome;
import com.niafikra.dimension.payroll.service.IncomeService.SubmittedIncomeFilter;
import com.niafikra.dimension.payroll.ui.vaadin.element.income.IncomeComboBox;
import com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.AbstractSubmittedPayGrid;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class SubmittedIncomeGrid extends AbstractSubmittedPayGrid<SubmittedIncome, SubmittedIncomeFilter> {

    @Inject
    private IncomeComboBox incomeFilter;

    public SubmittedIncomeGrid(SubmittedIncomesProvider provider) {
        super(provider, new SubmittedIncomeFilter());
    }

    @PostConstruct
    private void build() {

        addColumn(SubmittedIncome::getIncome)
                .setId("income")
                .setCaption("Income")
                .setSortable(true);

        addColumn(SubmittedIncome::getFormula)
                .setId("formula")
                .setCaption("Formula")
                .setSortable(true);

        addColumn(SubmittedIncome::getCalculatedAmount)
                .setId("calculatedAmount")
                .setCaption("Calculated Amount")
                .setSortable(true);

        incomeFilter.withFullWidth().addStyleNames(Theme.COMBOBOX_TINY);
        incomeFilter.addValueChangeListener(event -> {
            filter.setIncome(event.getValue());
            getDataProvider().refreshAll();
        });
        getFiltersRow().getCell("income").setComponent(incomeFilter);
    }

}
