package com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.deduction;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.payroll.domain.contract.pay.submitted.SubmittedDeduction;
import com.niafikra.dimension.payroll.service.DeductionService;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.deduction.SubmittedDeductionUpdateView.VIEW_NAME;


@Secured(Permission.SUBMIT_EMPLOYEE_DEDUCTION)
@ViewComponent(value = MainDisplay.class, caption = "Update employee deduction")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class SubmittedDeductionUpdateView extends SubmittedDeductionForm implements View, Notifier {

    public static final String VIEW_NAME = "update-deduction";

    @Inject
    private DeductionService deductionService;

    @Inject
    private Navigator navigator;

    @PostConstruct
    private void build() {
        setSizeFull();

        addSavedHandler(deduction -> doUpdate(deduction));
    }

    private void doUpdate(SubmittedDeduction deduction) {
        try {
            deduction = deductionService.update(deduction);
            showSuccess(String.format("Successful updated deduction for %s", deduction.getEmployee()));
            navigator.navigateTo(SubmittedDeductionView.class, deduction.getId());
        } catch (Exception e) {
            showError(String.format("Failed to update deduction for %s", deduction.getEmployee()), e);
        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long deductionId = Long.parseLong(event.getParameters());
        setEntity(deductionService.findSubmittedDeduction(deductionId));
    }

}
