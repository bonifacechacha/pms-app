package com.niafikra.dimension.payroll.ui.vaadin.contract;

import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.TemplatePanel;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.payroll.domain.contract.Contract;
import com.niafikra.dimension.payroll.service.ContractService;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.Permission.VIEW_CONTRACTS;
import static com.niafikra.dimension.payroll.ui.vaadin.contract.ContractView.VIEW_NAME;

@Secured(VIEW_CONTRACTS)
@ViewComponent(value = ContractDisplay.class, caption = "View Contract Details")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class ContractView extends VerticalLayout implements View {
    public static final String VIEW_NAME = "contract";

    @Inject
    private ContractService contractService;
    @Inject
    private TemplatePanel templatePanel;
    @Inject
    private Navigator navigator;

    private Contract contract;

    @PostConstruct
    private void build() {
        setSizeFull();
        templatePanel.setSizeFull();
        templatePanel.getTemplateView().setTemplatePath(Templates.CONTRACT);
        addComponentsAndExpand(templatePanel);
    }

    public void setContract(Contract contract) {
        this.contract = contract;
        templatePanel.getTemplateView().putBinding("contract", contract).render();
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long contractId = Long.parseLong(event.getParameters());
        setContract(contractService.findContract(contractId));
    }

}
