package com.niafikra.dimension.payroll.ui.vaadin.formular;

import com.niafikra.dimension.money.Money;
import com.niafikra.dimension.payroll.domain.formula.AbstractFormula;
import com.niafikra.dimension.payroll.domain.formula.Formula;
import com.niafikra.dimension.payroll.ui.vaadin.formular.editor.FormulaEditor;
import com.niafikra.dimension.payroll.ui.vaadin.formular.editor.FormulaEditorFactory;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.HorizontalSplitPanel;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.label.MLabel;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;


@PrototypeScope
@SpringComponent
public class FormulaPanel extends MVerticalLayout implements View {

    @Inject
    private FormulaEditorFactory formularEditorFactory;

    private FormulaTree<Money> formulaTree = new FormulaTree<>();

    @Inject
    private MoneyFormulaSelector baseFormulaSelector;

    private MLabel selectedFormulaLabel = new MLabel("No formula selected");
    private MVerticalLayout formularEditorBase = new MVerticalLayout().withFullSize();

    private AbstractFormula<Money> formula;

    @PostConstruct
    private void build() {
        setSizeFull();

        baseFormulaSelector.forAllFormulas();
        baseFormulaSelector.setWidth("100%");
        baseFormulaSelector.setEmptySelectionAllowed(false);
        baseFormulaSelector.addValueChangeListener(event -> {
            if (!baseFormulaSelector.isEmpty())
                setFormula(baseFormulaSelector.initSelectedFormula());
        });

        formulaTree.setSizeFull();
        formulaTree.addItemClickListener(event -> showEditor(event.getItem()));
        HorizontalSplitPanel splitPanel = new HorizontalSplitPanel();
        splitPanel.setSizeFull();
        splitPanel.setSplitPosition(40);
        splitPanel.setFirstComponent(
                new MVerticalLayout(baseFormulaSelector, formulaTree)
                        .withFullSize()
                        .withExpand(formulaTree, 1)
                        .withMargin(true)
        );

        formularEditorBase.setMargin(false);
        splitPanel.setSecondComponent(
                new MVerticalLayout(
                        selectedFormulaLabel,
                        formularEditorBase)
                        .withExpand(formularEditorBase, 1)
                        .withMargin(true)
                        .withFullSize()
        );

        add(splitPanel).withExpand(splitPanel, 1);
    }

    public AbstractFormula<Money> getFormula() {
        return formula;
    }

    private void setFormula(AbstractFormula<Money> formula) {
        this.formula = formula;

        formulaTree.setFormula(formula);
        formulaTree.expandAll();
    }

    private void showEditor(Formula formula) {
        selectedFormulaLabel.setValue(formula.getName());

        FormulaEditor editor = formularEditorFactory.create(formula);
        editor.setUpdateCallback(() -> setFormula(this.formula));

        formularEditorBase.removeAllComponents();
        formularEditorBase.addComponent(editor);
    }

    public void init() {
        baseFormulaSelector.clear();
        formulaTree.getTreeData().clear();
        formularEditorBase.removeAllComponents();
    }
}
