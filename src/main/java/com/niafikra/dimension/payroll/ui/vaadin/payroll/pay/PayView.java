package com.niafikra.dimension.payroll.ui.vaadin.payroll.pay;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.domain.Printable;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.TemplatePanel;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.payroll.domain.contract.pay.Pay;
import com.niafikra.dimension.payroll.service.PayrollService;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.payroll.ui.vaadin.payroll.pay.PayView.VIEW_NAME;

@Secured(Permission.VIEW_PAY)
@ViewComponent(value = MainDisplay.class, caption = "View Pay Details")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class PayView extends VerticalLayout implements View, Printable {
    public static final String VIEW_NAME = "pay";

    @Inject
    private PayrollService payService;
    @Inject
    private TemplatePanel templatePanel;

    private Pay pay;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);
        templatePanel.setSizeFull();
        templatePanel.getTemplateView().setTemplatePath(Templates.PAY);
        addComponentsAndExpand(templatePanel);
    }

    public void setPay(Pay pay) {
        this.pay = pay;
        templatePanel.getTemplateView().putBinding("pay", pay).render();
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long payId = Long.parseLong(event.getParameters());
        setPay(payService.findPay(payId));
    }

    @Override
    public String getPrintURL() {
        return "/pay/print/" + pay.getId();
    }
}
