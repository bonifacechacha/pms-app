package com.niafikra.dimension.payroll.ui.vaadin.formular.editor;

import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.payroll.domain.formula.*;
import com.niafikra.dimension.payroll.domain.formula.system.BasicIncomeFormula;
import com.niafikra.dimension.payroll.domain.formula.system.GrossIncomeFormula;
import com.niafikra.dimension.payroll.domain.formula.system.OtherIncomeFormula;
import com.niafikra.dimension.payroll.domain.formula.table.MoneyTableEval;
import com.niafikra.dimension.payroll.domain.formula.wrapper.MoneySavedFormula;
import com.niafikra.dimension.payroll.ui.vaadin.formular.table.MoneyTableEvalEditor;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class FormulaEditorFactory implements Notifier {

    @Inject
    private ActualMoneyEditor actualMoneyEditor;

    @Inject
    private ActualNumberEditor actualNumberEditor;

    @Inject
    private CollectionFormulaEditor collectionFormulaEditor;

    @Inject
    private MoneyDecimalEditor moneyDecimalEditor;

    @Inject
    private MoneyTableEvalEditor moneyTableEvalEditor;

    @Inject
    private IncomeBasedFormulaEditor incomeBasedFormulaEditor;

    @Inject
    private DeductionBasedFormulaEditor deductionBasedFormulaEditor;

    @Inject
    private MoneyPercentageEditor moneyPercentageEditor;

    @Inject
    private MoneySavedFormulaEditor savedFormulaEditor;

    @Inject
    private IncomeSystemFormulaEditor<GrossIncomeFormula> grossIncomeFormulaEditor;

    @Inject
    private IncomeSystemFormulaEditor<OtherIncomeFormula> otherIncomeFormulaEditor;

    @Inject
    private ScriptFormulaEditor scriptFormulaEditor;

    public FormulaEditor create(Formula formula) {
        FormulaEditor editor = create(formula.getName());
        editor.setFormula(formula);
        return editor;
    }

    public FormulaEditor create(String name) {
        switch (name) {
            case ActualMoney.NAME:
                return actualMoneyEditor;
            case ActualNumber.NAME:
                return actualNumberEditor;
            case MoneySum.NAME:
            case MoneySubtraction.NAME:
                return collectionFormulaEditor;
            case MoneyMultiply.NAME:
            case MoneyDivision.NAME:
                return moneyDecimalEditor;
            case GrossIncomeFormula.NAME:
                return grossIncomeFormulaEditor;
            case OtherIncomeFormula.NAME:
                return otherIncomeFormulaEditor;
            case BasicIncomeFormula.NAME:
                return new ReadOnlyFormulaEditor();
            case MoneyTableEval.NAME:
                return moneyTableEvalEditor;
            case IncomeBasedFormula.NAME:
                return incomeBasedFormulaEditor;
            case DeductionBasedFormula.NAME:
                return deductionBasedFormulaEditor;
            case MoneyPercentageFormula.NAME:
                return moneyPercentageEditor;
            case MoneySavedFormula.NAME:
                return savedFormulaEditor;
            case MoneyScriptFormula.NAME:
                return scriptFormulaEditor;
            default:
                String msg = String.format("There is no formula definition with name %s", name);
                showError(String.format("Failed to create editor for formula type :%s", name), msg);
                throw new IllegalArgumentException(msg);
        }
    }

}
