package com.niafikra.dimension.payroll.ui.vaadin.element.income;

import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.payroll.domain.element.Income;
import com.niafikra.dimension.payroll.service.IncomeService;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;


@PrototypeScope
@SpringComponent
public class IncomeComboBox extends MComboBox<Income> {

    public IncomeComboBox(IncomeService incomeService) {
        setItems(incomeService.findAll());
    }
}
