package com.niafikra.dimension.payroll.ui.vaadin.payroll;

import com.niafikra.dimension.payroll.domain.contract.pay.Payroll;
import com.niafikra.dimension.payroll.service.PayrollService;
import com.niafikra.dimension.payroll.service.PayrollService.PayrollFilter;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.spring.annotation.SpringComponent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.inject.Inject;
import java.util.List;


@PrototypeScope
@SpringComponent
public class PayrollsProvider extends PageableDataProvider<Payroll, PayrollFilter> {

    @Inject
    private PayrollService payrollService;

    @Override
    protected Page<Payroll> fetchFromBackEnd(Query<Payroll, PayrollFilter> query, Pageable pageable) {
        return payrollService.findPayrolls(query.getFilter(),pageable);
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        return List.of(
                new QuerySortOrder("period.start", SortDirection.DESCENDING),
                new QuerySortOrder("timeCreated", SortDirection.DESCENDING)
        );
    }

    @Override
    protected int sizeInBackEnd(Query<Payroll, PayrollFilter> query) {
        return payrollService.countAll(query.getFilter()).intValue();
    }
}
