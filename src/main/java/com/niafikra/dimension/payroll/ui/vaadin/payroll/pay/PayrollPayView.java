package com.niafikra.dimension.payroll.ui.vaadin.payroll.pay;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.payroll.domain.contract.pay.Payroll;
import com.niafikra.dimension.payroll.service.PayrollService;
import com.niafikra.dimension.payroll.ui.vaadin.payroll.PayrollDisplay;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.payroll.ui.vaadin.payroll.pay.PayrollPayView.VIEW_NAME;


@Secured(Permission.VIEW_PAYROLL_PAYS)
@ViewComponent(value = PayrollDisplay.class, caption = "View Pays")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class PayrollPayView  extends VerticalLayout implements View, TableExportable {
    public static final String VIEW_NAME = "payroll-pays";

    @Inject
    private PayGrid payGrid;

    @Inject
    private Navigator navigator;

    @Inject
    private PayrollService payrollService;

    private Payroll payroll;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);
        payGrid.setSizeFull();
        payGrid.setSelectionMode(Grid.SelectionMode.NONE);
        payGrid.addItemClickListener(event -> {
            navigator.navigateTo(PayView.class, event.getItem().getId());
        });

        addComponentsAndExpand(payGrid);
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(payGrid);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long payrollId = Long.parseLong(event.getParameters());
        setPayroll(payrollService.findPayroll(payrollId));
    }

    public void setPayroll(Payroll payroll) {
        this.payroll = payroll;

        payGrid.getFilter().setPayroll(payroll);
        payGrid.getDataProvider().refreshAll();
    }

}
