package com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.deduction;

import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.payroll.domain.contract.pay.submitted.SubmittedDeduction;
import com.niafikra.dimension.payroll.ui.vaadin.element.deduction.DeductionComboBox;
import com.niafikra.dimension.payroll.ui.vaadin.formular.MoneyFormulaField;
import com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.AbstractPaySubmitForm;
import com.vaadin.data.validator.BeanValidator;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MFormLayout;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MPanel;

import javax.inject.Inject;


@PrototypeScope
@SpringComponent
public class SubmittedDeductionForm extends AbstractPaySubmitForm<SubmittedDeduction> {

    @Inject
    protected DeductionComboBox deduction;

    @Inject
    private MoneyFormulaField employeeContrFormulaField;
    @Inject
    private MoneyFormulaField employerContrFormulaField;

    public SubmittedDeductionForm() {
        super(SubmittedDeduction.class);
    }

    @Override
    protected void bind() {
        super.bind();

        getBinder().forField(employeeContrFormulaField)
                .withValidator(new BeanValidator(SubmittedDeduction.class, "employeeContr"))
                .bind("employeeContr");


        getBinder().forField(employerContrFormulaField)
                .withValidator(new BeanValidator(SubmittedDeduction.class, "employerContr"))
                .bind("employerContr");
    }

    @Override
    protected Component createContent() {
        deduction.setWidth(FIELD_WIDTH);
        deduction.setCaption("Deduction");
        employeeContrFormulaField.setWidth(FIELD_WIDTH);
        employeeContrFormulaField.setCaption("Employee Contribution");
        employerContrFormulaField.setWidth(FIELD_WIDTH);
        employerContrFormulaField.setCaption("Employer Contribution");

        return new MPanel(
                new MFormLayout(
                        employee,
                        deduction,
                        employeeContrFormulaField,
                        employerContrFormulaField,
                        new MHorizontalLayout(date, dueDate),
                        description,
                        attachments,
                        getToolbar()
                )

        ).withStyleName(Theme.PANEL_BORDERLESS).withFullSize();
    }
}
