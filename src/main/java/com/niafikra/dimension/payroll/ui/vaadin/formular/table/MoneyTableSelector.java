package com.niafikra.dimension.payroll.ui.vaadin.formular.table;

import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.payroll.domain.formula.table.MoneyTable;
import com.niafikra.dimension.payroll.service.TableService;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class MoneyTableSelector extends MComboBox<MoneyTable> {

    @Inject
    private TableService tableService;

    @PostConstruct
    private void build() {
        setItems(tableService.findMoneyTables());
    }
}
