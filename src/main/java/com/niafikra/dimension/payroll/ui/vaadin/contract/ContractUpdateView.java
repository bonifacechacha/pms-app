package com.niafikra.dimension.payroll.ui.vaadin.contract;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.payroll.domain.contract.Contract;
import com.niafikra.dimension.payroll.service.ContractService;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.payroll.ui.vaadin.contract.ContractUpdateView.VIEW_NAME;

@Secured(Permission.UPDATE_CONTRACT)
@ViewComponent(value = MainDisplay.class, caption = "Update Contract")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class ContractUpdateView extends ContractForm implements View, Notifier {

    public static final String VIEW_NAME = "update-contract";

    @Inject
    private ContractService contractService;
    @Inject
    private Navigator navigator;

    @PostConstruct
    private void build() {
        setSizeFull();

        employee.setEnabled(false);
        periodType.setEnabled(false);

        addSavedHandler(contract -> doUpdate(contract));
    }

    private void doUpdate(Contract contract) {
        try {
            contract = contractService.update(contract);
            showSuccess("Contract updated successful", contract.toString());
            navigator.navigateTo(ContractView.class, contract.getId());
        } catch (Exception e) {
            showError("Failed to update contract", e);
        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long contractId = Long.parseLong(event.getParameters());
        setEntity(contractService.findContract(contractId));
    }
}
