package com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.income;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.util.DateUtils;
import com.niafikra.dimension.payroll.domain.contract.pay.submitted.SubmittedIncome;
import com.niafikra.dimension.payroll.service.IncomeService;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;

import static com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.income.IncomeSubmitView.VIEW_NAME;


@Secured(Permission.SUBMIT_EMPLOYEE_INCOME)
@ViewComponent(value = MainDisplay.class, caption = "Submit employee income")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class IncomeSubmitView extends SubmitedIncomeForm implements View, Notifier {

    public static final String VIEW_NAME = "submit-income";

    @Inject
    private IncomeService incomeService;


    @PostConstruct
    private void build() {
        setSizeFull();

        addSavedHandler(income -> doSubmit(income));
    }

    private void doSubmit(SubmittedIncome income) {
        try {
            income = incomeService.submit(income);
            showSuccess(String.format("Successful submitted income for %s", income.getEmployee()));
            loadNewIncome();
        } catch (Exception e) {
            showError(String.format("Failed to submit income for %s", income.getEmployee()), e);
        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        loadNewIncome();
    }

    private void loadNewIncome() {
        SubmittedIncome income = new SubmittedIncome();
        income.setDate(LocalDate.now());
        income.setDueDate(DateUtils.getStartOfNextMonth().toLocalDate());

        setEntity(income);
    }
}
