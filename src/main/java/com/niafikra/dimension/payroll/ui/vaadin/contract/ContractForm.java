package com.niafikra.dimension.payroll.ui.vaadin.contract;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.attachment.ui.vaadin.util.AttachmentsField;
import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.AbstractForm;
import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.core.ui.vaadin.util.MoneyField;
import com.niafikra.dimension.hr.ui.vaadin.employee.EmployeeComboBox;
import com.niafikra.dimension.payroll.domain.Bank;
import com.niafikra.dimension.payroll.domain.contract.Contract;
import com.niafikra.dimension.payroll.domain.period.PayPeriodType;
import com.niafikra.dimension.payroll.service.BankService;
import com.niafikra.dimension.payroll.service.PayPeriodService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.layouts.MFormLayout;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MPanel;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Set;


@PrototypeScope
@SpringComponent
public class ContractForm extends AbstractForm<Contract> {

    public static final String FIELD_WIDTH = "300px";

    protected MTextField code = new MTextField("Code");

    @Inject
    protected EmployeeComboBox employee;

    protected DateField startDate = new DateField("Start Date");
    protected DateField endDate = new DateField("End Date");

    protected CheckBox hourly = new CheckBox("Is calculated hourly?");
    protected MoneyField basicRate = new MoneyField();
    protected MComboBox<PayPeriodType> periodType = new MComboBox<>("Salary Period");

    protected MComboBox<Bank> bank = new MComboBox<>("Bank");
    protected MTextField accountNumber = new MTextField("Account Number");

    @Inject
    protected AttachmentsField<Set<Attachment>> attachments;

    @Inject
    private PayPeriodService periodService;

    @Inject
    private BankService bankService;


    public ContractForm() {
        super(Contract.class);
    }

    @PostConstruct
    private void build() {
        employee.setCaption("Employee");
        periodType.setItems(periodService.findAllPayPeriodTypes());
        bank.setItems(bankService.findAll());
    }

    @Override
    protected Component createContent() {
        code.withWidth(FIELD_WIDTH);
        employee.withWidth(FIELD_WIDTH);
        startDate.setWidth(FIELD_WIDTH);
        endDate.setWidth(FIELD_WIDTH);
        basicRate.setWidth(FIELD_WIDTH);
        periodType.setWidth(FIELD_WIDTH);
        bank.setWidth(FIELD_WIDTH);
        accountNumber.setWidth(FIELD_WIDTH);
        attachments.setWidth(FIELD_WIDTH);
        attachments.setCaption("Attachments");
        getSaveButton().setWidth(ContractForm.FIELD_WIDTH);

        return new MPanel(
                new MFormLayout(
                        code,
                        employee,
                        new MHorizontalLayout(startDate, endDate).withCaption("Period"),
                        periodType,
                        new MHorizontalLayout(basicRate, hourly).withCaption("Basic"),
                        new MHorizontalLayout(bank, accountNumber).withCaption("Bank"),
                        attachments,
                        getToolbar()
                )

        ).withStyleName(Theme.PANEL_BORDERLESS).withFullSize();
    }
}
