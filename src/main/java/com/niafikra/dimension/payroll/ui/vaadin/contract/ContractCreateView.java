package com.niafikra.dimension.payroll.ui.vaadin.contract;

import com.niafikra.dimension.Permission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.money.Money;
import com.niafikra.dimension.payroll.domain.contract.Contract;
import com.niafikra.dimension.payroll.service.ContractService;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.HashSet;

import static com.niafikra.dimension.payroll.ui.vaadin.contract.ContractCreateView.VIEW_NAME;

@Secured(Permission.CREATE_CONTRACT)
@ViewComponent(value = MainDisplay.class, caption = "Create Contract")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class ContractCreateView extends ContractForm implements View, Notifier {

    public static final String VIEW_NAME = "create-contract";

    @Inject
    private ContractService contractService;
    @Inject
    private Navigator navigator;

    @PostConstruct
    private void build() {
        setSizeFull();

        addSavedHandler(contract -> doCreate(contract));
    }

    private void doCreate(Contract contract) {
        try {
            contract = contractService.create(contract);
            showSuccess("Contract created successful", contract.toString());
            navigator.navigateTo(ContractView.class, contract.getId());
        } catch (Exception e) {
            showError("Failed to create contract", e);
        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Contract contract = new Contract();
        contract.setBasicRate(Money.getZERO());
        contract.setAttachments(new HashSet<>());
        setEntity(contract);
    }
}
