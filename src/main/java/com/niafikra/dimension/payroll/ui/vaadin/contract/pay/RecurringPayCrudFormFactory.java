package com.niafikra.dimension.payroll.ui.vaadin.contract.pay;

import com.niafikra.dimension.core.ui.vaadin.util.MVerticalCrudFormFactory;
import com.niafikra.dimension.payroll.domain.contract.pay.RecurringPayConfig;
import com.vaadin.data.HasValue;
import com.vaadin.ui.TextField;
import org.apache.commons.lang3.StringUtils;
import org.vaadin.viritin.fields.IntegerField;
import org.vaadin.viritin.fields.MTextField;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

public class RecurringPayCrudFormFactory<T extends RecurringPayConfig> extends MVerticalCrudFormFactory<T> {

    public RecurringPayCrudFormFactory(Class<T> type) {
        super(type);

        setFieldProvider("startPeriodIndex", () -> new IntegerField("Start Period Index"));
        setFieldProvider("skipPeriods", () -> new IntegerField("Skip Periods"));
        setFieldProvider("excludePeriodIndices", () -> new MTextField("Exclude Periods"));
    }

    @Override
    protected void bindField(HasValue field, String property, Class propertyType) {
        if (property.equals("excludePeriodIndices")) {
            TextField excludePeriodIndicesField = (TextField) field;
            binder.forField(excludePeriodIndicesField)
//                    .withValidator(new RegexpValidator(
//                            "Only comma separated indices are allowed",
//                            "/^(\\d+,)*\\d+$/"))
                    .withConverter(
                            text -> {
                                if (text.isEmpty()) return Collections.emptySet();
                                else return Arrays.stream(text.split(","))
                                        .map(Integer::valueOf)
                                        .collect(Collectors.toSet());
                            },
                            integers -> StringUtils.join(integers, ","))
                    .bind(property);
        } else if (Integer.class.isAssignableFrom(propertyType) || int.class.isAssignableFrom(propertyType)) {
            binder.forField(field)
                    .bind(property);
        } else super.bindField(field, property, propertyType);
    }
}