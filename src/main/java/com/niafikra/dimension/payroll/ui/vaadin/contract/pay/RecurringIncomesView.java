package com.niafikra.dimension.payroll.ui.vaadin.contract.pay;

import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.payroll.domain.contract.pay.RecurringIncomeConfig;
import com.niafikra.dimension.payroll.service.IncomeService;
import com.niafikra.dimension.payroll.ui.vaadin.contract.ContractDisplay;
import com.niafikra.dimension.payroll.ui.vaadin.formular.MoneyFormulaField;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Collections;

import static com.niafikra.dimension.Permission.MANAGE_CONTRACT_RECURRING_INCOMES;
import static com.niafikra.dimension.payroll.ui.vaadin.contract.pay.RecurringIncomesView.VIEW_NAME;

@Secured(MANAGE_CONTRACT_RECURRING_INCOMES)
@ViewComponent(value = ContractDisplay.class, caption = "Recurring Incomes")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class RecurringIncomesView extends RecurringPayPanel<RecurringIncomeConfig> implements Notifier {
    public static final String VIEW_NAME = "recurring-incomes";

    @Inject
    private IncomeService incomeService;

    @Inject
    private MoneyFormulaField formulaField;

    public RecurringIncomesView() {
        super(RecurringIncomeConfig.class);
        setCrudFormFactory(new RecurringIncomeCrudFormFactory());
    }

    @PostConstruct
    private void build() {
        getGrid().setColumns("income", "actualStartDate", "actualEndDate");
        setFindAllOperation(() -> {
            if (getContract() == null) return Collections.emptyList();
            return getContract().getRecurringIncomes();
        });

        setAddOperation(income -> doCreate(income));
        setUpdateOperation(income -> doUpdate(income));
        setDeleteOperation(income -> doDelete(income));
    }

    private void doDelete(RecurringIncomeConfig income) {
        try {
            contract = contractService.cancelRecurringIncome(income);
            refreshGrid();
            showSuccess("Successfully cancelled recurring income", income.getIncome().toString());
        } catch (Exception e) {
            showError("Failed to cancel recurring income", e);
        }
    }

    private RecurringIncomeConfig doCreate(RecurringIncomeConfig income) {
        try {
            contract = contractService.registerRecurringIncome(contract, income);
            refreshGrid();
            showSuccess("Successfully added contractual recurring income", income.getIncome().toString());
            return income;
        } catch (Exception e) {
            showError("Failed to add recurring income", e);
            throw e;
        }
    }

    private RecurringIncomeConfig doUpdate(RecurringIncomeConfig income) {
        try {
            contractService.update(income);
            reload();
            showSuccess("Successfully updated contractual recurring income", income.getIncome().toString());
            return income;
        } catch (Exception e) {
            showError("Failed to update recurring income", e);
            throw e;
        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long contractId = Long.parseLong(event.getParameters());
        setContract(contractService.findContract(contractId));
    }


    protected class RecurringIncomeCrudFormFactory extends RecurringPayCrudFormFactory<RecurringIncomeConfig> {

        public RecurringIncomeCrudFormFactory() {
            super(RecurringIncomeConfig.class);
            setFieldProvider(
                    "formula",
                    () -> {
                        formulaField.clear();
                        return formulaField;
                    }
            );
            setFieldProvider("income", () -> new MComboBox("Income").withItems(incomeService.findAll()));
            setVisibleProperties("income", "formula", "startDate", "endDate", "startPeriodIndex", "skipPeriods", "excludePeriodIndices");
        }
    }

}
