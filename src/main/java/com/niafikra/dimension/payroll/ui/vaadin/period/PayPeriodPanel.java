package com.niafikra.dimension.payroll.ui.vaadin.period;

import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.TemplatePanel;
import com.niafikra.dimension.payroll.domain.period.PayPeriodType;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class PayPeriodPanel extends MVerticalLayout {

    @Inject
    private TemplatePanel templatePanel;

    @PostConstruct
    private void build() {
        setSizeFull();
        addComponentsAndExpand(templatePanel);
        templatePanel.getTemplateView().setTemplatePath(Templates.PAY_PERIOD_TYPE);
    }

    public PayPeriodPanel setPayPeriodType(PayPeriodType type) {
        templatePanel.getTemplateView().putBinding("payPeriodType", type).render();
        return this;
    }
}
