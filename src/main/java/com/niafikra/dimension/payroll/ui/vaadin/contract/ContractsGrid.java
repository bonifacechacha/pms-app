package com.niafikra.dimension.payroll.ui.vaadin.contract;

import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.BooleanOptionGroup;
import com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils;
import com.niafikra.dimension.core.ui.vaadin.util.date.LocalDateRangeSelector;
import com.niafikra.dimension.hr.ui.vaadin.employee.EmployeeComboBox;
import com.niafikra.dimension.payroll.domain.contract.Contract;
import com.niafikra.dimension.payroll.service.ContractService.ContractFilter;
import com.niafikra.dimension.payroll.ui.vaadin.period.PayPeriodTypeComboBox;
import com.vaadin.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.HeaderRow;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class ContractsGrid extends Grid<Contract> {

    @Inject
    private EmployeeComboBox employeeFilter;

    @Inject
    private PayPeriodTypeComboBox periodTypeFilter;

    public ContractsGrid(ContractsProvider provider) {
        super(provider.withConfigurableFilter());
    }

    @PostConstruct
    private void build() {
        addColumn(Contract::getCode)
                .setCaption("Code")
                .setId("code")
                .setSortable(true)
                .setHidable(true);

        addColumn(Contract::getEmployee)
                .setId("employee")
                .setCaption("Employee")
                .setSortable(true)
                .setHidable(true);

        addColumn(contract -> contract.isHourly() ? "Wage" : "Salary")
                .setId("hourly")
                .setCaption("Payment")
                .setSortable(true)
                .setHidable(true);

        addColumn(contract -> contract.getPeriodType())
                .setId("periodType")
                .setCaption("Period Type")
                .setSortable(true)
                .setHidable(true);

        addColumn(Contract::getStartDate)
                .setId("startDate")
                .setCaption("Start")
                .setSortable(true)
                .setHidable(true);

        addColumn(Contract::getEndDate)
                .setId("endDate")
                .setCaption("End")
                .setSortable(true)
                .setHidable(true);

        ContractFilter filter = ContractFilter.builder().build();
        getDataProvider().setFilter(filter);

        HeaderRow filtersRow = appendHeaderRow();
        VaadinUtils.createFilteringTextField(
                filtersRow,
                "code",
                "Filter codes",
                event -> {
                    filter.setCode(event.getValue());
                    getDataProvider().refreshAll();
                }
        );

        employeeFilter.withFullWidth().addStyleNames(Theme.COMBOBOX_TINY);
        employeeFilter.addValueChangeListener(event -> {
            filter.setEmployee(event.getValue());
            getDataProvider().refreshAll();
        });
        filtersRow.getCell("employee").setComponent(employeeFilter);

        BooleanOptionGroup salaryFilter = new BooleanOptionGroup("Filter type", "Wage", "Salary", "All");
        salaryFilter.addValueChangeListener(event -> {
            filter.setHourly(salaryFilter.getBooleanValue());
            getDataProvider().refreshAll();
        });
        filtersRow.getCell("hourly").setComponent(salaryFilter);

        periodTypeFilter.withFullWidth().addStyleNames(Theme.COMBOBOX_TINY);
        periodTypeFilter.addValueChangeListener(event -> {
            filter.setPeriodType(event.getValue());
            getDataProvider().refreshAll();
        });
        filtersRow.getCell("periodType").setComponent(periodTypeFilter);

        LocalDateRangeSelector startRangeFilter = new LocalDateRangeSelector();
        startRangeFilter.setWidth("200px");
        filtersRow.getCell("startDate").setComponent(startRangeFilter);
        startRangeFilter.addValueChangeListener(event -> {
            filter.setStartRange(startRangeFilter.getStart(), startRangeFilter.getEnd());
        });


        LocalDateRangeSelector endRangeFilter = new LocalDateRangeSelector();
        endRangeFilter.setWidth("200px");
        filtersRow.getCell("endDate").setComponent(endRangeFilter);
        endRangeFilter.addValueChangeListener(event -> {
            filter.setEndRange(endRangeFilter.getStart(), endRangeFilter.getEnd());
        });
    }

    @Override
    public ConfigurableFilterDataProvider<Contract, Void, ContractFilter> getDataProvider() {
        return (ConfigurableFilterDataProvider<Contract, Void, ContractFilter>) super.getDataProvider();
    }
}
