package com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.deduction;

import com.niafikra.dimension.payroll.domain.contract.pay.submitted.SubmittedDeduction;
import com.niafikra.dimension.payroll.service.DeductionService;
import com.niafikra.dimension.payroll.service.DeductionService.SubmittedDeductionFilter;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.spring.annotation.SpringComponent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.inject.Inject;
import java.util.List;

@PrototypeScope
@SpringComponent
public class SubmittedDeductionsProvider extends PageableDataProvider<SubmittedDeduction, SubmittedDeductionFilter> {

    @Inject
    private DeductionService deductionService;

    @Override
    protected Page<SubmittedDeduction> fetchFromBackEnd(Query<SubmittedDeduction, SubmittedDeductionFilter> query, Pageable pageable) {
        return deductionService.findSubmittedDeductions(query.getFilter(), pageable);
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        return List.of(
                new QuerySortOrder("date", SortDirection.DESCENDING)
        );
    }

    @Override
    protected int sizeInBackEnd(Query<SubmittedDeduction, SubmittedDeductionFilter> query) {
        return deductionService.countSubmittedDeductions(query.getFilter()).intValue();
    }
}
