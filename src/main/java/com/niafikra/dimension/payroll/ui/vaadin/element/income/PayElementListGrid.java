package com.niafikra.dimension.payroll.ui.vaadin.element.income;

import com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils;
import com.niafikra.dimension.payroll.domain.element.PayElement;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.HeaderRow;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;

public class PayElementListGrid<T extends PayElement> extends Grid<T> {


    public PayElementListGrid(Collection<T> elements) {
        super(DataProvider.ofCollection(elements));

        addColumn(PayElement::getName).setId("name").setSortable(true).setHidable(false);
        
        HeaderRow headerRow = appendHeaderRow();
        VaadinUtils.createFilteringTextField(headerRow, "name", "Filter by name", event -> {
            getDataProvider().setFilter(element -> StringUtils.isEmpty(event.getValue()) || element.getName().contains(event.getValue()));
        });
    }

    public ListDataProvider<T> getDataProvider() {
        return (ListDataProvider<T>) super.getDataProvider();
    }
}
