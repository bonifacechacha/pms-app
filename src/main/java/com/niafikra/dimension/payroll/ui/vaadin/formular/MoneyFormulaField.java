package com.niafikra.dimension.payroll.ui.vaadin.formular;

import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.MoneyField;
import com.niafikra.dimension.core.ui.vaadin.util.SubWindow;
import com.niafikra.dimension.money.Money;
import com.niafikra.dimension.payroll.domain.formula.ActualMoney;
import com.niafikra.dimension.payroll.domain.formula.Formula;
import com.niafikra.dimension.payroll.domain.formula.MoneyPercentageFormula;
import com.niafikra.dimension.payroll.domain.formula.table.MoneyTableEval;
import com.niafikra.dimension.payroll.ui.vaadin.formular.table.MoneyTableSelector;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.RadioButtonGroup;
import org.apache.commons.lang3.StringUtils;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.MSize;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.List;

@PrototypeScope
@SpringComponent
public class MoneyFormulaField extends CustomField<Formula<Money>> {

    private FormulaTree<Money> formulaTree = new FormulaTree<>();

    private MoneyField moneyField = new MoneyField();
    private MTextField percentField = new MTextField("Percent").withFullWidth();
    private RadioButtonGroup<String> typeSelector = new RadioButtonGroup<>();

    @Inject
    private MoneyFormulaSelector inputSelector;

    @Inject
    private MoneyTableSelector tableSelector;

    @Inject
    private FormulaPanel formulaPanel;

    private MButton customFormulaButton = new MButton("Create custom formula").withIcon(VaadinIcons.CALC).withFullWidth();
    private MVerticalLayout fieldHolder = new MVerticalLayout().withMargin(false).withFullSize();

    public MoneyFormulaField() {
    }

    public MoneyFormulaField(String caption) {
        this();
        setCaption(caption);
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        super.setReadOnly(readOnly);
        typeSelector.setReadOnly(readOnly);
        moneyField.setReadOnly(readOnly);
        percentField.setReadOnly(readOnly);
        inputSelector.setReadOnly(readOnly);
        tableSelector.setReadOnly(readOnly);
        customFormulaButton.setEnabled(!readOnly);
    }

    @Override
    protected Component initContent() {
        typeSelector.setItems(
                List.of(ActualMoney.NAME,
                        MoneyPercentageFormula.NAME,
                        MoneyTableEval.NAME,
                        "CUSTOM"
                )
        );
        moneyField.addValueChangeListener(event -> {
            setValue(new ActualMoney(event.getValue()));
        });

        inputSelector.withFullWidth();
        inputSelector.setCaption("From");
        inputSelector.forSystemAndSavedFormulas();
        inputSelector.setEmptySelectionAllowed(false);
        inputSelector.addValueChangeListener(event -> {
            tableSelector.setEnabled(event.getValue() != null);
            percentField.setEnabled(event.getValue() != null);
        });

        tableSelector.withFullWidth();
        tableSelector.setEmptySelectionAllowed(false);
        tableSelector.setCaption("Table");
        tableSelector.setEnabled(false);
        tableSelector.addValueChangeListener(event -> {
            if (event.getValue() == null) return;

            MoneyTableEval tableEval = new MoneyTableEval();
            tableEval.setTable(event.getValue());
            tableEval.setInput(inputSelector.initSelectedFormula());
            setValue(tableEval);
        });

        percentField.setEnabled(false);
        percentField.addValueChangeListener(event -> {
            if (event.getValue() == null || !StringUtils.isNumeric(event.getValue())) return;

            MoneyPercentageFormula percentageFormula = new MoneyPercentageFormula();
            percentageFormula.setPercent(new BigDecimal(event.getValue()));
            percentageFormula.setInput(inputSelector.initSelectedFormula());
            setValue(percentageFormula);
        });


        customFormulaButton.addStyleNames(Theme.BUTTON_SMALL);
        customFormulaButton.addClickListener(() -> {
            formulaPanel.init();
            new SubWindow(formulaPanel)
                    .show()
                    .withCaption("Edit formula")
                    .withSize(MSize.size("80%", "80%"))
                    .addCloseListener(e -> setValue(formulaPanel.getFormula()));
        });

        typeSelector.addStyleNames(Theme.OPTIONGROUP_HORIZONTAL, Theme.OPTIONGROUP_SMALL);
        typeSelector.addValueChangeListener(event -> {
            fieldHolder.removeAllComponents();
            String type = event.getValue();
            if (type == null) return;

            switch (type) {
                case ActualMoney.NAME:
                    fieldHolder.add(moneyField);
                    break;
                case MoneyPercentageFormula.NAME:
                    fieldHolder.add(new MHorizontalLayout(inputSelector, percentField).withFullWidth().withMargin(false));
                    break;
                case MoneyTableEval.NAME:
                    fieldHolder.add(new MHorizontalLayout(inputSelector, tableSelector).withFullWidth().withMargin(false));
                    break;
                case "CUSTOM":
                    fieldHolder.add(customFormulaButton);
                    break;
            }
        });

        formulaTree.setSelectionMode(Grid.SelectionMode.NONE);
        MVerticalLayout base = new MVerticalLayout(
                formulaTree,
                typeSelector,
                fieldHolder
        ).withMargin(false);

        return base;
    }

    @Override
    protected void doSetValue(Formula<Money> value) {
        formulaTree.setFormula(value);
    }

    public void clear() {
        moneyField.clear();
        percentField.clear();
        tableSelector.clear();
        inputSelector.clear();
    }

    @Override
    public Formula<Money> getValue() {
        return formulaTree.getFormula();
    }
}
