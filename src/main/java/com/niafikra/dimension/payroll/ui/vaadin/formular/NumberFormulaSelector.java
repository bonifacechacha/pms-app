package com.niafikra.dimension.payroll.ui.vaadin.formular;

import com.niafikra.dimension.payroll.domain.formula.AbstractFormula;
import com.niafikra.dimension.payroll.service.FormulaService;
import com.vaadin.ui.ComboBox;

import java.math.BigDecimal;


public class NumberFormulaSelector extends ComboBox<String> {

    public NumberFormulaSelector() {
        setItems(FormulaService.listNumber());
    }

    public AbstractFormula<BigDecimal> initSelectedFormula() {
        return FormulaService.initNumber(getValue());
    }
}
