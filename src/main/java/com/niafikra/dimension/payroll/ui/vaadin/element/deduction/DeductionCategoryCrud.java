package com.niafikra.dimension.payroll.ui.vaadin.element.deduction;

import com.niafikra.dimension.category.ui.vaadin.CategoryCrud;
import com.niafikra.dimension.payroll.domain.element.Deduction;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;


@PrototypeScope
@SpringComponent
public class DeductionCategoryCrud extends CategoryCrud {

    public DeductionCategoryCrud() {
        super(Deduction.CATEGORY_TYPE);
    }
}
