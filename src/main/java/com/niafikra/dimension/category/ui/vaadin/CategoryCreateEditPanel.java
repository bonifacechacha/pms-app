package com.niafikra.dimension.category.ui.vaadin;

import com.niafikra.dimension.category.domain.Category;
import com.niafikra.dimension.category.service.CategoryService;
import com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils;
import com.niafikra.dimension.core.ui.vaadin.util.create.TreeCreateEditPanel;
import com.vaadin.ui.Component;

import java.util.List;
import java.util.Optional;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/6/17 4:32 PM
 */
public class CategoryCreateEditPanel extends TreeCreateEditPanel<Category> {

    private String type;
    private CategoryService categoryService;
    private CategoryForm categoryForm;

    public CategoryCreateEditPanel(String type, CategoryForm categoryForm, CategoryService categoryService) {
        super();
        this.type = type;
        this.categoryService = categoryService;
        this.categoryForm = categoryForm;

        subWindow.withWidth("40%");
        subWindow.setHeightUndefined();
        categoryForm.addSavedHandler(category -> {
            reload();
            subWindow.close();
        });

        reload();
    }

    public void reload() {
        List<Category> roots = categoryService.getRoots(type);
        entriesTree.setItems(
                roots,
                category -> categoryService.getChildren(category));
        entriesTree.expand(roots);
    }

    @Override
    protected void delete(Category category) {
        try {
            categoryService.delete(category);
            reload();

            String msg = "Deleted category successful";
            getLogger().info(msg);
            NotificationUtils.showSuccess(msg);
        } catch (Exception e) {
            String msg = "Failed to delete category";
            getLogger().error(msg, e);
            NotificationUtils.showError(msg);
        }
    }

    @Override
    protected Component getCreateContent() {
        subWindow.setCaption("Create new category");

        Optional<Category> parentCategory = getSelected();
        Category category = parentCategory.isPresent() ? new Category(type, parentCategory.get()) : new Category(type);

        categoryForm.reloadParentCategoriesItems();
        categoryForm.setEntity(category);
        return categoryForm;
    }

    @Override
    protected Component getEditContent(Category category) {
        subWindow.setCaption("Edit category " + category);

        categoryForm.reloadParentCategoriesItems();
        categoryForm.setEntity(category);
        return categoryForm;
    }
}
