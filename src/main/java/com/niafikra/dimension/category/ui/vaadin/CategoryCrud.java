package com.niafikra.dimension.category.ui.vaadin;

import com.niafikra.dimension.category.domain.Category;
import com.niafikra.dimension.category.service.CategoryService;
import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.util.TreeCrud;
import com.vaadin.ui.TextArea;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

public class CategoryCrud extends TreeCrud<Category> implements Notifier {

    private String type;

    @Inject
    private CategoryService categoryService;

    public CategoryCrud(String type) {
        super(Category.class);
        this.type = type;
    }

    @PostConstruct
    private void build() {
        getCrudFormFactory().setVisibleProperties("name", "parent", "description");
        getCrudFormFactory().setFieldProvider("parent", () -> new MComboBox<Category>("Parent").withItems(categoryService.getAll(type)));
        getCrudFormFactory().setFieldProvider("description", () -> new TextArea("Description"));
        setFindAllOperation(() -> categoryService.getAll(type));
        setAddOperation(this::doSave);
        setUpdateOperation(this::doSave);
        setDeleteOperation(cat -> doDelete(cat));
    }

    protected void doDelete(Category category) {
        try {
            categoryService.delete(category);
            showSuccess("Deleted category successful", category.toString());
        } catch (Exception e) {
            showError("Failed to delete category", e);
        }
    }

    private Category doSave(Category category) {
        try {
            category.setType(type);
            category = categoryService.save(category);
            showSuccess("Saved category successful", category.toString());
            return category;
        } catch (Exception e) {
            showError("Failed to save category", e);
            return null;
        }
    }
}
