package com.niafikra.dimension.category.ui.vaadin;

import com.niafikra.dimension.category.domain.Category;
import com.niafikra.dimension.category.domain.HasCategory;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.VerticalLayout;

import java.util.Optional;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/8/17 7:54 PM
 */
public class CategorisedItemsManagementPanel<T extends HasCategory> extends VerticalLayout {
    private CategoryCreateEditPanel categoryCreateEditPanel;
    private HasCategoryList<T> categorisedListView;

    public CategorisedItemsManagementPanel(CategoryCreateEditPanel categoryCreateEditPanel, HasCategoryList<T> categorisedListView) {
        this.categoryCreateEditPanel = categoryCreateEditPanel;
        this.categorisedListView = categorisedListView;

        setSizeFull();
        setMargin(false);

        HorizontalSplitPanel splitPanel = new HorizontalSplitPanel();
        splitPanel.setSizeFull();
        addComponent(splitPanel);
        splitPanel.setSplitPosition(30);

        categoryCreateEditPanel.setSizeFull();
        splitPanel.setFirstComponent(categoryCreateEditPanel);

        categorisedListView.setSizeFull();
        splitPanel.setSecondComponent(categorisedListView);

        categoryCreateEditPanel.getEntriesTree().addSelectionListener(event -> {
            Optional<Category> selected = event.getFirstSelectedItem();
            categorisedListView.filterCategory(selected.orElse(null));

        });
    }

    public CategoryCreateEditPanel getCategoryCreateEditPanel() {
        return categoryCreateEditPanel;
    }

    public HasCategoryList<T> getCategorisedListView() {
        return categorisedListView;
    }
}
