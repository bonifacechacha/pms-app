create table dimension.cr_authority
(
  id        bigint auto_increment
    primary key,
  name      varchar(255) not null,
  version   int          null,
  parent_id bigint       null,
  constraint UK_boqxmoqkbin64u6j7hh4pgqkg
  unique (name),
  constraint FKstcu1dp0wm8m6tecdentndq4
  foreign key (parent_id) references dimension.cr_authority (id)
)
  engine = InnoDB;

create index FKstcu1dp0wm8m6tecdentndq4
  on dimension.cr_authority (parent_id);

create table dimension.authority_permissions
(
  authority_id bigint       not null,
  permissions  varchar(255) null,
  constraint FK4jv2tj2xdqprpjtkpjreip5s2
  foreign key (authority_id) references dimension.cr_authority (id)
)
  engine = InnoDB;

create index FK4jv2tj2xdqprpjtkpjreip5s2
  on dimension.authority_permissions (authority_id);

create table dimension.cr_user_authorities
(
  user_id        bigint not null,
  authorities_id bigint not null,
  primary key (user_id, authorities_id),
  constraint FKa5lgwhq2616xlyfycclxoo9tq
  foreign key (user_id) references dimension.cr_user (id),
  constraint FKjoasuy9f4ayl5lv7ofqwnlgsd
  foreign key (authorities_id) references dimension.cr_authority (id)
)
  engine = InnoDB;

create index FKjoasuy9f4ayl5lv7ofqwnlgsd
  on dimension.cr_user_authorities (authorities_id);

INSERT INTO `dimension`.`cr_user` (`email`,
                                   `external_identity`,
                                   `name`,
                                   `phone_number`,
                                   `time_created`,
                                   `username`,
                                   `version`)
VALUES ('admin@dimension.co.tz', 'admin', 'System Administrator', 'NULL', '2018-04-10 16:13:20', 'admin', 0);
INSERT INTO `dimension`.`cr_authority` (`name`, `version`)
VALUES ('System Administrator', 0);
INSERT INTO `dimension`.`authority_permissions` (`authority_id`, `permissions`)
VALUES (1, 'CONFIGURE_AUTHORITIES');
INSERT INTO `dimension`.`authority_permissions` (`authority_id`, `permissions`)
VALUES (1, 'CONFIGURE_USERS');

INSERT INTO cr_user_authorities (user_id, authorities_id)
SELECT id, 1
FROM cr_user
WHERE username = 'admin'