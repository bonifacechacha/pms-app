cd  ../target
pwd
echo "Enter IP address of remote server ..."
read ip
echo "Enter user of remote server ..."
read user
echo "packages will be deployed at .dimension/dist/"
ssh $user@$ip "mkdir -p .dimension/dist/"
rsync -v -e ssh pms-app-1.0.0-SNAPSHOT.war $user@$ip:.dimension/dist/dimension.war --progress