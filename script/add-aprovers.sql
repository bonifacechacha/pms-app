# adding another approver on request which there is a certain person already
use dimension;
INSERT INTO ap_tracker_cr_user(tracker_id, approvers_id)
SELECT tracker_id, NEW_APPROVER_ID
FROM ap_tracker_cr_user
WHERE approvers_id=CURRENT_APPROVER_ID;